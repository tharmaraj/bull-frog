<?php


use App\CartProductMaster;
use App\config;
use App\MerchantStoreMaster;
use App\VariantProductMaster;
use App\VoucherEntry;
use App\Voucher;
use App\PaymentProductMaster;
use App\User;
use App\NotificationModel;


function vaoucherValidate($voucherid, $actual_subtotal){

        $results = Voucher::where('activestatus','1')
        ->where('voucherid',$voucherid)
        ->first();

        
        if(!$results){
            return ['2','Voucher Not Found'];
            // return redirect()->back()->withErrors(['Voucher','Not Found']);
        }
        
        
        if($results->user != '*'){
            if($results->user != Auth::user()->userid){
                return ['3','User Not Same'];
                // return redirect()->back()->withErrors(['Voucher','User Not Same']);
            }
        }
        
        
        $datetime1 = new DateTime();
        $datetime2 = new DateTime($results->startdatetime);
        
        if ( $datetime1 < $datetime2 ) {
            // dd('expire Not Started Yet');
            return ['4','Not Started Yet'];
            // return redirect()->back()->withErrors(['Expired','Not Started Yet']);
        }
        else {
            
            
            $datetime3 = new DateTime($results->enddatetime);
            if ( $datetime1 > $datetime3 ) {
                // dd('Expired Bro Sorry');
                return ['5','End Date Expired Bro Sorry'];
                // return redirect()->back()->withErrors(['Expired','End Date Expired Bro Sorry']);
            }
            
        }
        
        if($actual_subtotal < $results->minimumpurchase){
            return ['6',"Not Enough Minimum RM $results->minimumpurchase "];
            
        }

        

        return ['1','Done'];


}

function paymentProduct($userid){


        $result_record = PaymentProductMaster::where('createdby',$userid)
        ->where('finalsubmit','0')
        ->delete();
        
        $response = getcartdetails($userid);
        // dd($response);
        $response = $response['data'];
        $orderidfix = rand();

        foreach($response as $key => $respons){
            // dd($respons);
            foreach($respons['ids'] as $keys => $res){
                // dd($res);
                $results = new PaymentProductMaster;
                $results->storeid = $respons['storeid'];
                $results->merchantid = $respons['variantid_Details'][$keys]['merchantid'];
                $results->productid = $respons['variantid_Details'][$keys]['productid'];
                $results->variantid = $respons['variantid_Details'][$keys]['variantid'];
                $results->variantid_ID = $respons['variantid_ID'][$keys];
                $results->userid = $respons['userid'];
                $results->orderid = $orderidfix;
                $results->count = $respons['variantid_Count'][$keys];
                $results->totalcart = $respons['variantid_Count'][$keys];
                $results->shippingvalue = $respons['variantid_Details'][$keys]['shippingprice'];
                $results->total = $respons['variantid_Details'][$keys]['sellingprice'];
                $results->subtotal = ($respons['variantid_Details'][$keys]['sellingprice'] * $respons['variantid_Count'][$keys] );
                $results->finalsubmit = '0';
                $results->ordershippingstatus = '1';
                $results->orderpaymentstatus = '2'; //pending
                $results->activestatus = $respons['variantid_Details'][$keys]['activestatus'];
                $results->createdby = $userid;
                $results->save();
            }

            
        }
}


function calculatePayment($response){

    // dd($response['data']);

    $result = array();

    $response_voucher = $response['voucher'];
    $response = $response['data'];
    
    
    $_total = [];
    $_totalshipping = [];
    $_subtotal = [];
    $voucher_amount = '';
    
    foreach($response as $respons){
        array_push($_totalshipping, $respons['shippingprice']);
        array_push($_total, $respons['total']);
        array_push($_subtotal, $respons['subtotal']);
        
        if(isset($response_voucher['voucher_amount'])){
            $voucher_amount = $response_voucher['voucher_amount'];
        }

        
        // dd($respons['subtotal']);
    }

    
    $sum_totalshipping = number_format(array_sum($_totalshipping),2);
    $sum_total = number_format(array_sum($_total),2);
    $sum_subtotal = number_format(array_sum($_subtotal),2);

    $result['sum_totalshipping'] = $sum_totalshipping;
    $result['sum_total'] = $sum_total;
    $result['sum_subtotal'] = $sum_subtotal;
    $result['voucher_amount'] = $voucher_amount;

    // dd($result);
    return $result;


}

function getcartdetails($userid){

    $users = User::find($userid);
    $configs = config::where('activestatus', 1)->first();

    $response = array();
    
    if($users->role == '1'){

        // $results_carts = CartProductMaster::
        // select(DB::raw('GROUP_CONCAT(count) as count'),DB::raw('GROUP_CONCAT(id) as ids'),'storeid',DB::raw('GROUP_CONCAT(variantid_ID) as variantid_ID'))
        // ->where('activestatus','1')
        // ->where('userid',$users->userid)
        // ->groupBy('storeid')
        // ->get();

        $results_carts = CartProductMaster::
        select(DB::raw('GROUP_CONCAT(cart_product_masters.count) as count'),DB::raw('GROUP_CONCAT(cart_product_masters.id) as ids'),'cart_product_masters.storeid',DB::raw('GROUP_CONCAT(cart_product_masters.variantid_ID) as variantid_ID'))

        ->join('product_store_masters','product_store_masters.productid','=','cart_product_masters.productid')
        ->where('product_store_masters.activestatus', '1')

        ->where('cart_product_masters.activestatus','1')
        ->where('cart_product_masters.userid',$users->userid)
        ->groupBy('cart_product_masters.storeid')
        ->get();
    }else{
        // $results_carts = CartProductMaster::
        // select(DB::raw('GROUP_CONCAT(count) as count'),DB::raw('GROUP_CONCAT(id) as ids'),'storeid',DB::raw('GROUP_CONCAT(variantid_ID) as variantid_ID'))
        // ->where('activestatus','1')
        // ->where('userid',$users->userid)
        // ->groupBy('storeid')
        // ->get();
        $results_carts = CartProductMaster::
        select(DB::raw('GROUP_CONCAT(cart_product_masters.count) as count'),DB::raw('GROUP_CONCAT(cart_product_masters.id) as ids'),'cart_product_masters.storeid',DB::raw('GROUP_CONCAT(cart_product_masters.variantid_ID) as variantid_ID'))
        
        ->join('product_store_masters','product_store_masters.productid','=','cart_product_masters.productid')
        ->where('product_store_masters.activestatus', '1')

        ->where('cart_product_masters.activestatus','1')
        ->where('cart_product_masters.userid',$users->userid)
        ->groupBy('cart_product_masters.storeid')
        ->get();

    }

    // dd($results_carts);

        $final_total = 0;
        $final_subtotal = 0;
        $final_actual_subtotal = 0;
        $aftercalculate_vaoucher_subtotal = 0;
        
        for($c = 0; $c < $results_carts->count(); $c++){


            $response['data'][$c]['storeid'] = $results_carts[$c]->storeid;
            $response['data'][$c]['userid'] = $users->userid;

            $response['data'][$c]['ids'] = array();
            $response['data'][$c]['variantid_ID'] = array();
            $response['data'][$c]['variantid_Count'] = array();
            $response['data'][$c]['variantid_Details'] = array();
            $response['data'][$c]['variantid_PricePlusCount'] = array();

            
            
            $ex_ids = explode(',',$results_carts[$c]->ids);
            $ex_variantids = explode(',',$results_carts[$c]->variantid_ID);
            $ex_counts = explode(',',$results_carts[$c]->count);
            
            foreach($ex_ids as $key => $ex_id){
                array_push($response['data'][$c]['ids'], $ex_id);
                array_push($response['data'][$c]['variantid_ID'], $ex_variantids[$key]);
                array_push($response['data'][$c]['variantid_Count'], $ex_counts[$key]);
            }



            $merchantstore = MerchantStoreMaster::where('storeid',$results_carts[$c]->storeid)->first();

            $count_variantid = count($ex_variantids);
            if($count_variantid > 1){
                $response['data'][$c]['shippingprice'] = $merchantstore->defaultshippingprice;
            }


            foreach($ex_variantids as $key => $ex_variantid){
                // dd($ex_variantid);
                $variants = VariantProductMaster::where('id',$ex_variantid)->get();
                foreach($variants as $variant){
                    
                    // dd($variant->sellingprice);
                    $variant->sellingprice = number_format($variant->sellingprice,2);
                    $variant->shippingprice = number_format($variant->shippingprice,2);
                    $variant->beforeprice = number_format($variant->beforeprice,2);

                    array_push($response['data'][$c]['variantid_Details'], $variant);
                    array_push($response['data'][$c]['variantid_PricePlusCount'], ( $ex_counts[$key] * $variant->sellingprice ));

                    if($count_variantid < 2){
                        $response['data'][$c]['shippingprice'] = $variant->shippingprice;
                    }

                }

                
                $response['data'][$c]['total'] = number_format(array_sum($response['data'][$c]['variantid_PricePlusCount']),2);
                $response['data'][$c]['subtotal'] = number_format(( array_sum($response['data'][$c]['variantid_PricePlusCount']) +  $response['data'][$c]['shippingprice'] ),2);
                $response['data'][$c]['actual_subtotal'] = number_format(( array_sum($response['data'][$c]['variantid_PricePlusCount']) +  $response['data'][$c]['shippingprice'] ),2);
            }
            
            $total_str = str_replace(',', '', $response['data'][$c]['total']);
            $subtotal_str = str_replace(',', '', $response['data'][$c]['subtotal']);
            $actual_subtotal_str = str_replace(',', '', $response['data'][$c]['actual_subtotal']);
            // dd($total_str);


            $final_total += $total_str;
            $final_subtotal += $subtotal_str;
            $final_actual_subtotal += $actual_subtotal_str;

        }

        $aftercalculate_vaoucher_subtotal = $final_subtotal;


        // dd($final_subtotal);

        //Voucher Start

        $voucher_entry = VoucherEntry::where('createdby', $userid)
        ->where('activestatus', '1')
        ->first();

        $response['voucher'] = array();
        $response['voucher']['voucher_details'] = array();
        array_push($response['voucher']['voucher_details'], $voucher_entry);

        //voucher calculate
        if($voucher_entry){

            $subtotal = $final_subtotal;

            $vouchers = Voucher::where('voucherid',$voucher_entry->voucherid)->first();
            // dd($vouchers);


            
            if($vouchers->voucherpricetype == '1'){
                // percentage %
                $response['voucher']['voucher_amount'] = ( $final_actual_subtotal - $subtotal*($vouchers->value/100) );
                $aftercalculate_vaoucher_subtotal = $final_subtotal*($vouchers->value/100);
                
            }else if($vouchers->voucherpricetype == '2'){
                // value RM
                $response['voucher']['voucher_amount'] = $vouchers->value;
                $aftercalculate_vaoucher_subtotal = ($final_subtotal - $vouchers->value);

            }


            // dd($vouchers);
        }

        $response['subtotal'] = number_format($aftercalculate_vaoucher_subtotal,2);
        $response['actual_subtotal'] = number_format($final_actual_subtotal,2);

        //Voucher End



        // dd($response);
        return $response;


}





function gettotalcart($userid){
    $users = User::find($userid);
    $prvresultscountsum = CartProductMaster::select('cart_product_masters.count')
    ->join('product_store_masters','product_store_masters.productid','=','cart_product_masters.productid')
    ->where('product_store_masters.activestatus', '1')
    ->where('cart_product_masters.userid', $users->userid)
    ->where('cart_product_masters.activestatus', '1')
    ->sum('cart_product_masters.count');


    return $prvresultscountsum;
}

function geturlfile($filename){
    $url = URL::to('/').'/files/'.$filename;
    return $url;
}

function uploadfile($file){

    if($file){
    $new_name = rand().'.'.$file->getClientOriginalExtension();
    $file->move(public_path("files"),$new_name);
    }else{
        $new_name = '';
    }

return $new_name;

} 
function updatefilefrombase64($name, $imagebase64){

    if(file_exists(Request::url().'/files/'.$name)){
        unlink('files/'.$name);
    }

    // $image_64 = $imagebase64; //your base64 encoded data
    // $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
    // $replace = substr($image_64, 0, strpos($image_64, ',')+1); 
    // // find substring fro replace here eg: data:image/png;base64,
    // $image = str_replace($replace, '', $image_64); 
    // $image = str_replace(' ', '+', $image); 
    $new_name = rand().'.jpg';
    Storage::disk('files')->put($new_name, base64_decode($imagebase64));

    // $image_64 = $imagebase64; //your base64 encoded data
    // $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
    // $replace = substr($image_64, 0, strpos($image_64, ',')+1); 
    // // find substring fro replace here eg: data:image/png;base64,
    // $image = str_replace($replace, '', $image_64); 
    // $image = str_replace(' ', '+', $image); 
    // $new_name = rand().'.'.$extension;
    // Storage::disk('files')->put($new_name, base64_decode($image));
    
    // dd($new_name);

    return $new_name;

} 

function updatefile($name, $file){

    if($file){
        if(file_exists(Request::url().'/files/'.$name)){
            unlink('files/'.$name);
        }
        $new_name = rand().'.'.$file->getClientOriginalExtension();
        $file->move(public_path("files"),$new_name);
    }else{
        $new_name = $name;
    }

    return $new_name;

} 

function deletefile($name){
    if($name){
        if(file_exists(Request::url().'/files/'.$name)){
        unlink('files/'.$name);
        }
    }
}

function roleLabel(){
    $_role = [
        "1" => [
            "key" => 1,
            "name" => "Admin"
        ],
        "2" => [
            "key" => 2,
            "name" => "Normal User"
        ],
        "3" => [
            "key" => 3,
            "name" => "Merchant"
        ],
    
    ];

    return $_role;
}


function getRole($role){
    $listRole = roleLabel();
    $listRole = $listRole[$role];
    $listRole = $listRole['name'];
    return $listRole;

}


function telegramapi($msg){
    //
    $website = "https://api.telegram.org/bot1690412958:AAHdALPR8AP6adlhUjkT75F78TZCtcy3vNs";
    $chatId = '-1001265561130';  //Receiver Chat Id 

    $params = [
    'chat_id' => '-1001265561130',
    'text' => "$msg",
    // 'parse_mode' => 'html', //<b>
    'parse_mode' => 'markdown',//*test*
    ];
    $ch = curl_init($website . '/sendMessage');
    //$ch = curl_init($website . '/sendMessage');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);
}

function dropDown($results,$name,$subcategory){
    // dd($results);
    $res = array();
    $res[] = "<select name='$name'>";
    foreach($results as $category){

    $selected = '';
    if($subcategory != ''){
        if($category->id == $subcategory){
            $selected = 'selected';
        }
    }    
    if($category->category){
        
        $res[] = "<option $selected value='$category->id'>$category->category - $category->name</option>";
    }else{
        $res[] = "<option $selected value='$category->id'>$category->id - $category->name</option>";

    }
    

    }
    $res[] = "</select>";
    $res = join('',$res);
    // dd(join('',$res));
    return $res;
}

function stateList($value){

    $res = array();
    $res[] = "<select name='state'>";

    $states = states();
    foreach($states as $state){
        $selected = '';
        $id = $state['id'];
        $name = $state['name'];
        if($value == $id){
            $selected = 'selected';
        }
        $res[] = "<option $selected value='$id'>$id - $name</option>";
    }
    $res[] = "</select>";
    $res = join('',$res);
    // dd(join('',$res));
    return $res;

}


function states(){
    $res = array();
    $res[] = ['id'=>'10','name'=>'Selangor'];
    return $res;
}

function cityList($value){

    $res = array();
    $res[] = "<select name='city'>";

    $states = cities();
    foreach($states as $state){
        $selected = '';
        $id = $state['id'];
        $name = $state['name'];
        if($value == $id){
            $selected = 'selected';
        }
        $res[] = "<option $selected value='$id'>$id - $name</option>";
    }
    $res[] = "</select>";
    $res = join('',$res);
    // dd(join('',$res));
    return $res;

}


function cities(){
    $res = array();
    $res[] = ['id'=>'1','city'=>'10','name'=>'Banting'];
    $res[] = ['id'=>'2','city'=>'10','name'=>'Cyberjaya'];
    $res[] = ['id'=>'3','city'=>'10','name'=>'Jenjarom'];
    $res[] = ['id'=>'4','city'=>'10','name'=>'Tanjung Sepat'];
    return $res;
}




function insertNotification($notification, $notificationtype, $message, $is_ssen_admin, $is_ssen_user, $notification_owner, $activestatus, $createdby){
    $results = new NotificationModel;
    $results->notification = $notification;
    $results->notificationtype = $notificationtype;
    $results->message = $message;
    $results->is_ssen_admin = $is_ssen_admin;
    $results->is_ssen_user = $is_ssen_user;
    $results->notification_owner = $notification_owner;
    $results->activestatus = $activestatus;
    $results->createdby = $createdby;
    $results->save();
}

function brainfunction($result)
{
    //
    $hotsalecount = CartProductMaster::select('count')->where('productid',$result->productid)->where('activestatus','1')->sum('count');
    $fastsalecount = CartProductMaster::select('count')->where('productid',$result->productid)->where('activestatus','2')->sum('count');

    // dd($hotsalecount);
    $hotsaleind = 0;
    $fastsaleind = 0;

    if($hotsalecount > 40){
        $hotsaleind = 1;
    }
    if($fastsalecount > 30){
        $fastsaleind = 1;
    }


    $brainarray = array();
    $brainarray['hotsaleind'] = $hotsaleind;
    $brainarray['fastsaleind'] = $fastsaleind;

    return $brainarray;
}