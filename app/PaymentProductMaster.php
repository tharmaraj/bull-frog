<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentProductMaster extends Model
{
    //
    public function ordershippingstatusval(){
        return $this->belongsTo(OrderShippingStatus::class,'ordershippingstatus');
    }
}
