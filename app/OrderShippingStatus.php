<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderShippingStatus extends Model
{
    //
    public function relationpaymentproduct(){
        return $this->hasMany(PaymentProductMaster::class);
    }
}
