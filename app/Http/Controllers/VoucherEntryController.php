<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VoucherEntry;
use App\Voucher;
use App\PaymentProductMaster;

use Auth;
use DateTime;

class VoucherEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = VoucherEntry::all();
        // dd($results);
        return view('voucherentry/view',compact('results'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if($request->action){
            Auth::loginUsingId($request->userid);
        }

        paymentProduct(Auth::id());

        $response = getcartdetails(Auth::id());
        
        $validateVoucher = vaoucherValidate($request->voucherid,$response['actual_subtotal']);

        if($validateVoucher[0] != '1'){
            if($request->action){
                $ret = [$validateVoucher[1]];
                return $ret;
            }else{
                return redirect()->back()->withErrors(['Voucher',$validateVoucher[1]]);
            }
        }
        

        $orders = PaymentProductMaster::where('createdby', Auth::id())
        ->where('finalsubmit', '0')
        ->first();


        $query = new VoucherEntry;
        $query->voucherid = $request->voucherid;
        $query->orderid = $orders->orderid;
        $query->userid = $orders->userid;
        $query->activestatus = '1';

        $query->createdby = Auth::id();

        $query->save();

        if($request->action){
            $ret = ['1'];
            return $ret;
        }

        
        return redirect('paymentproductcreate');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
