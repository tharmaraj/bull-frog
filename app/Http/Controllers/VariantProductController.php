<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VariantProductMaster;
use App\config;
use Auth;
use URL;
use Str;

class VariantProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->role == '1'){
        $results = VariantProductMaster::all();
        }else{
        $results = VariantProductMaster::where('createdby', Auth::id())->get();
        }

        return view('variant/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('variant/store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //
        $configs = config::where('activestatus', 1)->first();

        $results = new VariantProductMaster;

        $results->storeid = $request->storeid;
        $results->merchantid = $request->merchantid;
        $results->productid = $request->productid;
        $results->variantid = $request->variantid;
        $results->name = $request->name;
        $results->description = $request->description;
        $results->averageweight = $request->averageweight;
        $results->photo = uploadfile($request->file('photo'));
        $results->video = uploadfile($request->file('video'));
        $results->availablestock = $request->availablestock;
        $results->variantactivestatus = $request->variantactivestatus;
        
        $results->sellingprice = $request->sellingprice;
        $results->shippingprice = $request->shippingprice;
        $results->company = $results->sellingprice*($configs->companycommissionpercentage/100);
        $results->ipay = $results->sellingprice*($configs->ipay88commissionpercentage/100);

        $results->profit = ($results->sellingprice - ($results->sellingprice*($configs->companycommissionpercentage/100)));

        $results->topsellind = $request->topsellind;
        
        $results->whatisinside = $request->whatisinside;
        $results->howtocook = $request->howtocook;
        $results->readytocookind = $request->readytocookind;
        
        $results->beforeprice = $request->beforeprice;


        $results->activestatus = '1';
        

        $results->createdby = Auth::id();


        $results->save();

        if(Str::contains(URL::previous(), 'viewvariant')){
            return redirect("viewvariant/$request->variantid");
        }

        return redirect('variantproductview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = VariantProductMaster::find($id);

        return view('variant/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        $configs = config::where('activestatus', 1)->first();

        $results = VariantProductMaster::find($id);

        $results->name = $request->name;
        $results->description = $request->description;
        $results->averageweight = $request->averageweight;
        $results->photo = updatefile($results->photo, $request->file('photo'));
        $results->video = updatefile($results->video, $request->file('video'));
        $results->availablestock = $request->availablestock;
        $results->variantactivestatus = $request->variantactivestatus;
        
        $results->sellingprice = $request->sellingprice;
        $results->shippingprice = $request->shippingprice;
        $results->company = $results->sellingprice*($configs->companycommissionpercentage/100);
        $results->ipay = $results->sellingprice*($configs->ipay88commissionpercentage/100);

        $results->profit = ($results->sellingprice - ($results->sellingprice*($configs->companycommissionpercentage/100)));

        $results->topsellind = $request->topsellind;

        $results->whatisinside = $request->whatisinside;
        $results->howtocook = $request->howtocook;
        $results->readytocookind = $request->readytocookind;

        $results->beforeprice = $request->beforeprice;
        
        $results->activestatus = '1';
        

        $results->createdby = Auth::id();


        $results->save();

        return redirect('variantproductview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function approve($id)
    {

        
        $results = VariantProductMaster::find($id);
        $results->activestatus = '1';
        $results->save();

        return redirect('variantproductview');
    }
    public function reject($id)
    {

        $results = VariantProductMaster::find($id);
        $results->activestatus = '0';
        $results->save();


        return redirect('variantproductview');
        
    }

    public function destroy($id)
    {
        //
        $results = VariantProductMaster::find($id);

        deletefile($results->photo);

        $results->delete();

        return redirect('variantproductview');
    }
}
