<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductStoreMaster;
use App\MerchantMaster;
use App\MerchantStoreMaster;
use App\config;
use App\VariantProductMaster;
use App\CategoryMaster;
use App\SubCategoryMaster;
use App\VariantItemListMaster;
use App\ReviewProductModel;
use App\User;
use App\CartProductMaster;
use App\BargainModel;
use Auth;

class ProductStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->role == '1'){
            $results = ProductStoreMaster::all();
        }else{
            $results = ProductStoreMaster::where('createdby', Auth::id())->get();
        }
        return view('productstore/view', compact('results'));
    }
    public function morningmarketview()
    {
        $results = ProductStoreMaster::where('createdby', Auth::id())->where('morningmarketind',1)->get();
        return view('productstore/morningmarket', compact('results'));
    }
    public function viewvariant($variantid)
    {
        $results = ProductStoreMaster::where('variantid',$variantid)->first();
        $resultslist = VariantProductMaster::where('variantid',$variantid)->get();

        foreach($resultslist as $res){
            $results_itemlist = VariantItemListMaster::where('variant_ID',$res->id)->get();
            $res['variantitemlists'] = $results_itemlist; 
        }

        return view('productstore/viewvariant', compact('results','resultslist'));
    }
    public function publicproductview(Request $request)
    {

        $results_val = ProductStoreMaster::where('activestatus','1');
        if($request->filter_name){
            $results_val = $results_val->where('name','like', "%$request->filter_name%");
            $results_val = $results_val->orWhere('description','like', "%$request->filter_name%");
        }
        if($request->filter_category){
            $results_val = $results_val->where('category', $request->filter_category);
        }
        if($request->productid){
            $results_val = $results_val->where('productid', $request->productid);
        }
        if($request->group == '1'){
            $results_val = $results_val->where('topsellind', 1);
        }
        if($request->group == '2'){
            $results_val = $results_val->where('readytocookind', 1);
        }
        if($request->group == '3'){
            $results_val = $results_val->where('packageind', 1);
        }
        if($request->group == '4'){
            $results_val = $results_val->where('morningmarketind',1);
        }
        if($request->productid_in){
            $ress = explode(',', $request->productid_in);
            $results_val = $results_val->whereIn('productid', $ress);
        }

        $limit_list = 10;
        if($request->offsetLimit){
            $results_val = $results_val->offset($request->offsetLimit)->limit($limit_list);
        }else{
            $results_val = $results_val->offset(0)->limit($limit_list);
        }

        // $results_val = $results_val->inRandomOrder();
        $results_val = $results_val->get();
        // dd($results_val);
        $results = array();
        
        
        foreach($results_val as $result){
            
            $variant = VariantProductMaster::where('variantid',$result->variantid)->where('activestatus','1');

            $categories = CategoryMaster::where('id',$result->category)->first();
            $subcategories = SubCategoryMaster::where('id',$result->subcategory)->first();
            if($categories){
                $result['categorie_name'] = $categories->name;
            }else{
                $result['categorie_name'] = '';
            }
            if($subcategories){
                $result['subcategorie_name'] = $subcategories->name;
            }else{
                $result['subcategorie_name'] = '';
            }


            $variant = $variant->get();

            foreach($variant as $vari){
                $vari->sellingprice = number_format($vari->sellingprice,2);
                $vari->shippingprice = number_format($vari->shippingprice,2);
                $vari->beforeprice = number_format($vari->beforeprice,2);
            }

            foreach($variant as $var){
                $variant_list_items = VariantItemListMaster::where('variant_ID',$var->id)->get();
                $var['variant_list_items'] = $variant_list_items;

                $random_productid = ProductStoreMaster::where('activestatus','1')->inRandomOrder()->first();

                if($result->packageind == '1'){ //let customer add to cart straight because package already include all items
                    $random_productid->productid = '';
                }else{
                    $random_productid->productid = $random_productid->productid;
                }
                $var['random_productid'] = $random_productid->productid;

                $bargainresults = BargainModel::where('activestatus','1')
                ->where('productid',$var->productid)
                ->where('variantid_ID',$var->id)
                ->where('createdby',Auth::id())
                ->get();
                $var['bargain'] = $bargainresults;
            }


            $reviewproducts = ReviewProductModel::where('productid', $result->productid)->where('activestatus', 1)->get();
                
            foreach($reviewproducts as $key => $reviewproduct){
                $reviewproducts_reply = ReviewProductModel::where('replyto_reviewid',$reviewproduct->id)->where('activestatus', 1)->get();

                foreach($reviewproducts_reply as $review_reply){

                    $users_reply = User::find($review_reply->createdby);
                    $review_reply['users'] = $users_reply;

                }

                $users = User::find($reviewproduct->createdby);

                $reviewproduct['users'] = $users;
                $reviewproduct['replies'] = $reviewproducts_reply;

                if($reviewproduct->replyto_reviewid != null){
                    unset($reviewproducts[$key]);
                }
            }
            
            $result['reviews'] = $reviewproducts;
            $result['variant'] = $variant;
            

            
            $brainarray = brainfunction($result);
            $result['brain'] = $brainarray;

            array_push($results,$result);
        }
        
        // dd($request->action);
        if($request->action){
            foreach($results as $result){
                $merchant_details = MerchantMaster::where('merchantid', $result->merchantid)->first();
                $result['merchant_details'] = $merchant_details;

                $store_details = MerchantStoreMaster::where('storeid', $result->storeid)->first();
                $result['store_details'] = $store_details;

    
            }
    
            return compact('results');
        }
        if($request->group == '4'){
            return view('product_view_market', compact('results'));
        }

        return view('product_view', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
        $merchantstore = MerchantStoreMaster::where('createdby', Auth::id())->get();
        $categories = CategoryMaster::all();
        $subcategories = SubCategoryMaster::all();
        return view('productstore/store', compact('categories','subcategories','merchantstore'));
    }
    
    public function morningmarketcreate($morningmarketind)
    {
        //
        $merchantstore = MerchantStoreMaster::where('createdby', Auth::id())->get();
        $categories = CategoryMaster::all();
        $subcategories = SubCategoryMaster::all();
        return view('productstore/store', compact('categories','subcategories','merchantstore','morningmarketind'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $configs = config::where('activestatus', 1)->first();

        $merchantdetails = MerchantMaster::where('userid', Auth::user()->userid)->first();

        $results = new ProductStoreMaster;

        $results->storeid = $request->storeid;
        $results->merchantid = $merchantdetails->merchantid;
        $results->productid = rand();
        $results->name = $request->name;
        $results->category = $request->category;
        $results->subcategory = $request->subcategory;
        $results->description = $request->description;
        $results->deliveryperiod = $request->deliveryperiod;
        $results->unitmeasurement = $request->unitmeasurement;
        $results->photo1 = uploadfile($request->file('photo1'));
        $results->photo2 = uploadfile($request->file('photo2'));
        $results->photo3 = uploadfile($request->file('photo3'));
        $results->photo4 = uploadfile($request->file('photo4'));
        $results->photo5 = uploadfile($request->file('photo5'));
        $results->youtube = $request->youtube;
        $results->rating = $request->rating;
        $results->topsellind = $request->topsellind;
        $results->readytocookind = $request->readytocookind;
        $results->packageind = $request->packageind;
        $results->morningmarketind = $request->morningmarketind;
        $results->variantid = rand();

        // pricing masuk dalam variant
        // $results->sellingprice = $request->sellingprice;
        // $results->shippingprice = $request->shippingprice;
        // $results->company = $configs->companycommissionpercentage;
        // $results->ipay = $request->ipay88commissionpercentage;
        // $results->profit = $request->profit;
        
        
        $results->activestatus = '1';

        $results->createdby = Auth::id();

        $results->save();

        return redirect('productstoreview');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categories = CategoryMaster::all();
        $subcategories = SubCategoryMaster::all();
        $results = ProductStoreMaster::find($id);

        return view('productstore/edit',compact('results','categories','subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        //
        $configs = config::where('activestatus', 1)->first();

        $results = ProductStoreMaster::find($id);

        $results->name = $request->name;
        $results->category = $request->category;
        $results->subcategory = $request->subcategory;
        $results->description = $request->description;
        $results->deliveryperiod = $request->deliveryperiod;
        $results->unitmeasurement = $request->unitmeasurement;
        $results->photo1 = updatefile($results->photo1, $request->file('photo1'));
        $results->photo2 = updatefile($results->photo2, $request->file('photo2'));
        $results->photo3 = updatefile($results->photo3, $request->file('photo3'));
        $results->photo4 = updatefile($results->photo4, $request->file('photo4'));
        $results->photo5 = updatefile($results->photo5, $request->file('photo5'));
        $results->youtube = $request->youtube;
        $results->rating = $request->rating;
        $results->topsellind = $request->topsellind;
        $results->readytocookind = $request->readytocookind;
        $results->packageind = $request->packageind;
        $results->morningmarketind = $request->morningmarketind;


        // pricing masuk dalam variant
        // $results->sellingprice = $request->sellingprice;
        // $results->shippingprice = $request->shippingprice;
        // $results->company = $configs->companycommissionpercentage;
        // $results->ipay = $request->ipay88commissionpercentage;
        // $results->profit = $request->profit;

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return redirect('productstoreview');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function approve($id)
    {

        
        $results = ProductStoreMaster::find($id);
        $results->activestatus = '1';
        $results->save();

        return redirect('productstoreview');
    }
    public function reject($id)
    {

        $results = ProductStoreMaster::find($id);
        $results->activestatus = '0';
        $results->save();


        return redirect('productstoreview');
        
    }
    
    public function deactiveMarketProduct()
    {
        $dateHour = date("H:i:s");
        // dd($dateHour);
        if($dateHour > '11:30' && $dateHour < '15:38'){
        ProductStoreMaster::where('activestatus', '1')->where('morningmarketind', 1)->update(['activestatus' => '0']);
        }
    }

    public function destroy($id)
    {
        //
        $results = ProductStoreMaster::find($id);

        deletefile($results->photo1);
        deletefile($results->photo2);
        deletefile($results->photo3);
        deletefile($results->photo4);
        deletefile($results->photo5);

        $results->delete();

        return redirect('productstoreview');
    }
}
