<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EasyModel;
use App\EasyChannelModel;

class EasyAppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getchannel()
    {
        //
        $result = EasyChannelModel::where('activestatus','1')->get();
        return $result;
    }
    public function getrecords()
    {
        //
        $result = EasyModel::where('activestatus','1')->get();
        // $result = EasyModel::all();
        return $result;
    }
    public function getrecordstoday()
    {
        //
        $result = EasyModel::where('activestatus','1')->whereDay('created_at',now()->day)->get();
        // $result = EasyModel::all();
        return $result;
    }
    public function getrecorfilter(Request $request)
    {
        // dd(now()->day);
        //
        $result = EasyModel::where('activestatus','1')->whereDate('created_at',$request->date)->get();
        // $result = EasyModel::all();
        return $result;
    }
    public function getrecordsbelumbayar()
    {
        //
        $result = EasyModel::where('paymenttype','Belum Bayar')->get();
        // $result = EasyModel::all();
        return $result;
    }


    public function index()
    {
        //
        $result = EasyChannelModel::all();
        dd($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function recordCreate(Request $request)
    {
        //
        $result = new EasyModel;
        $result->channelid = $request->channelid;
        $result->description = $request->description;
        $result->note = $request->note;
        $result->photobase = $request->photobase;
        $result->amount = $request->amount;
        $result->paymenttype = $request->paymenttype;
        $result->activestatus = 1;
        $result->save();
        
    }
    public function storein(Request $request)
    {
        //
        $result = new EasyModel;
        $result->channelid = $request->channelid;
        $result->description = $request->description;
        $result->note = $request->note;
        $result->photobase = $request->photobase;
        $result->amount = $request->amount;
        $result->paymenttype = $request->paymenttype;
        $result->storeind = 1;
        $result->activestatus = 1;
        $result->save();
        
    }
    public function recordUpdate($id)
    {
        $result = EasyModel::find($id);
        $result->paymenttype = 'Bayar';
        $result->save();
    }
    public function recordDelete($id)
    {
        $result = EasyModel::find($id);
        $result->delete();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
