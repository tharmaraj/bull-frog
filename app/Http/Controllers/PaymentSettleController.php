<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentSettleMaster;
use App\PaymentProductMaster;
use App\CartProductMaster;
use App\VoucherEntry;

use App\Mail\OrderConfirmation;
use Illuminate\Support\Facades\Mail;

use Auth;

class PaymentSettleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->action){
            Auth::loginUsingId($request->userid_id_only);
        }
        $response = getcartdetails(Auth::id());
        
        PaymentProductMaster::where('orderid', $request->orderid)->update(['finalsubmit' => '1']);
        CartProductMaster::where('createdby', Auth::id())->update(['activestatus' => '2']);
        
        $results = new PaymentSettleMaster;
        $results->userid = $request->userid;
        $results->orderid = $request->orderid;
        $results->totalvalue = $request->totalvalue;
        $results->paymentmethod = '1'; // 1 = FPX
        $results->phone = $request->phone;
        $results->shippingaddressid = $request->shippingaddressid;
        $results->data = serialize($response);
        $results->changesmoney = $request->changesmoney;
        $results->paymentgatewayid = $request->paymentgatewayid;

        if($request->image1base64){
            $results->receipt_copy = updatefilefrombase64($results->receipt_copy, $request->image1base64);
        }else{
            $results->receipt_copy = updatefile($results->receipt_copy, $request->file('receipt_copy'));
        }

        $results->activestatus = '1';
        $results->createdby = Auth::id();
        $results->save();

        $voucher = VoucherEntry::where('createdby', $request->userid_id_only)
        ->where('activestatus', '1')
        ->first();

        if($voucher){
        $voucher->activestatus = '3'; // settle
        $voucher->save();
        }

        $telemsg = 'Oerderid *'.$request->orderid.'* from *'.Auth::user()->name.'* '.now();
        telegramapi($telemsg);

        try {
            Mail::to(Auth::user()->email)->send(new OrderConfirmation($request));
            // echo 'Mail send successfully';
        } catch (\Exception $e) {
            // echo 'Error - '.$e;
        }
        


        if($request->action){
            $ret = ['1'];
            return $ret;
        }

        return redirect('historymasterview');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateReceipt(Request $request)
    {

        $results = PaymentSettleMaster::where('orderid',$request->orderid)->first();

        if($request->image1base64){
            $results->receipt_copy = updatefilefrombase64($results->receipt_copy, $request->image1base64);
        }else{
            $results->receipt_copy = updatefile($results->receipt_copy, $request->file('receipt_copy'));
        }
        $results->updatedby = $request->userid;
        $results->updateddate = date("Y-m-d H:i:s");
        $results->save();

        if($request->action){
            $ret = ['1'];
            return $ret;
        }

    }
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function updateOrder(Request $request)
    {
        //
        dd($request);
    }
    public function callback(Request $request)
    {
        //
        $results = PaymentSettleMaster::where('paymentgatewayid',$request->id)->first();
        if($request->paid == 'true'){
            $results->payment_status = 1;
            PaymentProductMaster::where('orderid', $results->orderid)->update(['orderpaymentstatus' => '1']);
        }
        
        if($request->paid == 'false'){
            $results->payment_status = 3;
            PaymentProductMaster::where('orderid', $results->orderid)->update(['orderpaymentstatus' => '3']);
        }
        $results->save();

        // dd($request);
    }
    public function redirect(Request $request)
    {
        //
        // dd($request->billplz[id]);
        // $results = PaymentSettleMaster::where('paymentgatewayid',$request->id)->first();
        // $results->payment_status = 1;
        // if($request->paid == true){
        // }else{
        //     $results->payment_status = 3;
        // }
        // $results->save();
        // dd($request);
    }
    public function getpaymentstatus(Request $request)
    {
        //
        $results = PaymentSettleMaster::where('paymentgatewayid',$request->paymentgatewayid)->first();
        return $results;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
