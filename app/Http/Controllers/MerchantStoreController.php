<?php

namespace App\Http\Controllers;

use App\MerchantStoreMaster;
use Illuminate\Http\Request;

use App\MerchantMaster;

use Auth;

class MerchantStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->role == '1'){
        $results = MerchantStoreMaster::all();
        }else{
        $results = MerchantStoreMaster::where('createdby', Auth::id())->get();
        }
        return view('merchantstore/view', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('merchantstore/store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $results = new MerchantStoreMaster;

        $merchantidget = MerchantMaster::where('userid', Auth::user()->userid)->first();

        $results->storeid = rand();
        $results->merchantid = $merchantidget->merchantid;

        $results->storeimage = uploadfile($request->file('storeimage'));
        $results->storelogo = uploadfile($request->file('storelogo'));
        $results->fullstorename = $request->fullstorename;
        $results->nickstorename = $request->nickstorename;
        $results->storephone = $request->storephone;
        $results->address1 = $request->address1;
        $results->address2 = $request->address2;
        $results->poscood = $request->poscood;
        $results->city = $request->city;
        $results->state = $request->state;
        $results->openingtime = $request->openingtime;
        $results->closingtime = $request->closingtime;
        $results->holiday = $request->holiday;
        $results->instagram = $request->instagram;
        $results->facebook = $request->facebook;
        $results->youtube = $request->youtube;
        $results->website = $request->website;
        $results->location = $request->location;
        $results->defaultshippingprice = $request->defaultshippingprice;
        $results->cod = $request->cod;

        $results->activestatus = '0';

        $results->createdby = Auth::id();


        $results->save();

        return redirect('merchantstoreview');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = MerchantStoreMaster::find($id);

        return view('merchantstore/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //


        //
        $results = MerchantStoreMaster::find($id);

        $results->storeimage = updatefile($results->storeimage, $request->file('storeimage'));
        $results->storelogo = updatefile($results->storelogo, $request->file('storelogo'));
        $results->fullstorename = $request->fullstorename;
        $results->nickstorename = $request->nickstorename;
        $results->storephone = $request->storephone;
        $results->address1 = $request->address1;
        $results->address2 = $request->address2;
        $results->poscood = $request->poscood;
        $results->city = $request->city;
        $results->state = $request->state;
        $results->openingtime = $request->openingtime;
        $results->closingtime = $request->closingtime;
        $results->holiday = $request->holiday;
        $results->instagram = $request->instagram;
        $results->facebook = $request->facebook;
        $results->youtube = $request->youtube;
        $results->website = $request->website;
        $results->location = $request->location;
        $results->defaultshippingprice = $request->defaultshippingprice;
        $results->cod = $request->cod;



        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");


        $results->save();

        return redirect('merchantstoreview');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function approve($id)
    {

        
        $results = MerchantStoreMaster::find($id);

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->activestatus = '1';
        $results->save();

        return redirect('merchantstoreview');

    }
    public function reject($id)
    {

        
        $results = MerchantStoreMaster::find($id);

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->activestatus = '1';
        $results->save();

        return redirect('merchantstoreview');

    }

    public function destroy($id)
    {
        //
        $results = MerchantStoreMaster::find($id);

        deletefile($results->storeimage);
        deletefile($results->storelogo);

        $results->delete();

        return redirect('merchantstoreview');
    }
}
