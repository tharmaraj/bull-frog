<?php

namespace App\Http\Controllers;
use App\MerchantMaster;
use App\config;
use App\User;
use Illuminate\Http\Request;
use App\MerchantStoreMaster;
use Auth;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->role == '1'){
        $results = MerchantMaster::all();
        }else{
        $results = MerchantMaster::where('userid', Auth::user()->userid)->get();
        }

        return view('merchant/view',compact('results'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('merchant/store');

    }


    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        
        $configs = config::where('activestatus', 1)->first();

        $results = new MerchantMaster;

        $results->userid = Auth::user()->userid;
        $results->merchantid = rand();


        $results->activestatus = '0';

        $results->profileimage = uploadfile($request->file('profileimage'));

        $results->supportdoc1 = uploadfile($request->file('supportdoc1'));
        $results->supportdoc2 = uploadfile($request->file('supportdoc2'));
        // $results->supportdoc3
        // $results->supportdoc4
        // $results->supportdoc5

        $results->name = $request->name;
        $results->email = $request->email;
        $results->ic = $request->ic;
        $results->phone = $request->phone;
        $results->address1 = $request->address1;
        $results->address2 = $request->address2;
        $results->poscood = $request->poscood;
        $results->city = $request->city;
        $results->state = $request->state;
        
        $results->deliveryavailable = $request->deliveryavailable;
        
        $results->storeactivestatus = '0';
        $results->limitproduct = $configs->limitproductformerchant;
        $results->maxproductvalue = $configs->maxproductvaluemerchant;
        $results->minproductvalue = $configs->minproductvaluemerchant;


        $results->createdby = Auth::id();


        $results->save();

        return redirect('merchantview');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = MerchantMaster::find($id);

        return view('merchant/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $configs = config::where('activestatus', 1)->first();

        $results = MerchantMaster::find($id);

        $results->userid = Auth::user()->userid;

        $results->activestatus = '0';

        $results->profileimage = updatefile($results->profileimage, $request->profileimage);

        $results->supportdoc1 = updatefile($results->supportdoc1, $request->file('supportdoc1'));
        $results->supportdoc2 = updatefile($results->supportdoc2, $request->file('supportdoc2'));
        // $results->supportdoc3
        // $results->supportdoc4
        // $results->supportdoc5

        $results->name = $request->name;
        $results->email = $request->email;
        $results->ic = $request->ic;
        $results->phone = $request->phone;
        $results->address1 = $request->address1;
        $results->address2 = $request->address2;
        $results->poscood = $request->poscood;
        $results->city = $request->city;
        $results->state = $request->state;
        
        $results->deliveryavailable = $request->deliveryavailable;
        
        $results->limitproduct = $configs->limitproductformerchant;
        $results->maxproductvalue = $configs->maxproductvaluemerchant;
        $results->minproductvalue = $configs->minproductvaluemerchant;

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");


        $results->save();

        return redirect('merchantview');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {

        
        $results = MerchantMaster::find($id);

        $userget = User::where('userid', $results->userid)->first();
        $userget->merchant = '1';
        $userget->role = '3';
        $userget->save();

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");
        
        $results->storeactivestatus = '1';
        $results->save();
        
        $merchantstore = MerchantStoreMaster::where('merchantid', $results->merchantid)->update(array('activestatus' => '1'));

        return redirect('merchantview');

    }
    public function reject($id)
    {

        
        $results = MerchantMaster::find($id);

        $userget = User::where('userid', $results->userid)->first();
        $userget->merchant = '0';
        $userget->role = '2';
        $userget->save();


        $results->storeactivestatus = '0';
        $results->save();

        $merchantstore = MerchantStoreMaster::where('merchantid', $results->merchantid)->update(array('activestatus' => '0'));

        return redirect('merchantview');

    }


    public function destroy($id)
    {
        //
        $results = MerchantMaster::find($id);

        deletefile($results->profileimage);
        deletefile($results->supportdoc1);
        deletefile($results->supportdoc2);

        $userget = User::where('userid', $results->userid)->first();
        $userget->merchant = '0';
        $userget->save();

        $results->delete();

        return redirect('merchantview');

    }
}
