<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PaymentSettleMaster;
use App\PaymentProductMaster;
use Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($userid)
    {
        //
        $total_purchase = PaymentSettleMaster::where('createdby', $userid)->where('activestatus','1')->groupBy('createdby')->sum('totalvalue');

        $total_packing = PaymentProductMaster::where('finalsubmit', '1')
        ->where('ordershippingstatus',1)
        ->where('createdby',$userid)
        ->count();
        $total_out_for_delivery = PaymentProductMaster::where('finalsubmit', '1')
        ->where('ordershippingstatus',2)
        ->where('createdby',$userid)
        ->count();
        $total_delivered = PaymentProductMaster::where('finalsubmit', '1')
        ->where('ordershippingstatus',3)
        ->where('createdby',$userid)
        ->count();
        $total_pending = PaymentProductMaster::where('finalsubmit', '1')
        ->where('orderpaymentstatus',1)
        ->where('createdby',$userid)
        ->count();
        $total_payed = PaymentProductMaster::where('finalsubmit', '1')
        ->where('orderpaymentstatus',2)
        ->where('createdby',$userid)
        ->count();
        $total_failed = PaymentProductMaster::where('finalsubmit', '1')
        ->where('orderpaymentstatus',3)
        ->where('createdby',$userid)
        ->count();

        $result = array(
            "total_purchase" => $total_purchase,

            "total_packing" => $total_packing,
            "total_out_for_delivery" => $total_out_for_delivery,
            "total_delivered" => $total_delivered,

            "total_pending" => $total_pending,
            "total_payed" => $total_payed,
            "total_failed" => $total_failed,
        );

        dd($result);

        return view('dashboard/view', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
