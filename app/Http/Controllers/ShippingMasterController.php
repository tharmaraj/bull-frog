<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShippingMaster;
use Auth;

class ShippingMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = ShippingMaster::where('createdby',Auth::id())->get();
        // dd($results);
        return view('shippingaddress/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('shippingaddress/store');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if($request->action){
            Auth::loginUsingId($request->userid);
        }

        ShippingMaster::where('createdby', Auth::id())->update(['activestatus' => '0']);
        
        $results = new ShippingMaster;

        $results->userid = Auth::id();
        $results->address1 = $request->address1;
        $results->address2 = $request->address2;
        $results->postcode = $request->postcode;
        $results->city = $request->city;
        $results->state = $request->state;
        $results->geoaddress = $request->geoaddress;
        $results->geolat = $request->geolat;
        $results->geolong = $request->geolong;
        $results->activestatus = '1';

        $results->createdby = Auth::id();

        $results->save();

        if($request->action){
            return ['1','Done'];
        }

        return redirect('shippingmasterview');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = ShippingMaster::find($id);

        return view('shippingaddress/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if($request->action){
            Auth::loginUsingId($request->userid);
        }

        ShippingMaster::where('createdby', Auth::id())->update(['activestatus' => '0']);

        $results = ShippingMaster::find($id);

        $results->address1 = $request->address1;
        $results->address2 = $request->address2;
        $results->postcode = $request->postcode;
        $results->city = $request->city;
        $results->state = $request->state;
        $results->geoaddress = $request->geoaddress;
        $results->geolat = $request->geolat;
        $results->geolong = $request->geolong;
        $results->activestatus = '1';

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        if($request->action){
            return ['1','Done'];
        }

        return redirect('shippingmasterview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function approve($id)
    {
        
        $results = ShippingMaster::find($id);
        ShippingMaster::where('createdby', $results->createdby)->update(['activestatus' => '0']);
        $results->activestatus = '1';
        $results->save();

        return redirect('shippingmasterview');

    }
    public function reject($id)
    {

        
        $results = ShippingMaster::find($id);
        $results->activestatus = '0';
        $results->save();

        return redirect('shippingmasterview');

    }
    public function destroy($id)
    {
        //
        $results = ShippingMaster::find($id);
        $results->delete();

        return redirect('shippingmasterview');
    }
}
