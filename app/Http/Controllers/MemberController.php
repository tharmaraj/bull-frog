<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = User::all();

        return view('member/view', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function approve($id)
    {
       
        $results = User::find($id);
        $results->activestatus = '1';
        $results->updated_at = date("Y-m-d H:i:s");
        $results->save();
        
        return redirect('memberview');

    }
    public function reject($id)
    {

        $results = User::find($id);
        $results->activestatus = '0';
        $results->updated_at = date("Y-m-d H:i:s");
        $results->save();
        
        return redirect('memberview');

    }
    public function login($id)
    {
        Auth::loginUsingId($id);
        
        return redirect('memberview');

    }

    public function destroy($id)
    {
        //
    }
}
