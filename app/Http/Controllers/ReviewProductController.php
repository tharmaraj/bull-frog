<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReviewProductModel;

use Auth;

class ReviewProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->role == '1'){
            $results = ReviewProductModel::all();
        }else{
            $results = ReviewProductModel::where('createdby',Auth::id())->get();
        }
        // dd($results);
        return view('reviewproduct/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('reviewproduct/store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if($request->action){
            Auth::loginUsingId($request->userid);
        }

        $results = new ReviewProductModel;
        $results->productid = $request->productid;
        $results->variantid = $request->variantid;
        $results->variantid_ID = $request->variantid_ID;
        $results->orderid = $request->orderid;
        $results->star = $request->star;

        if($request->image1base64){
            $results->image1 = updatefilefrombase64($results->image1, $request->image1base64);
        }else{
            $results->image1 = updatefile($results->image1, $request->file('image1'));
        }


        // $results->image1 = uploadfile($request->file('image1'));
        // $results->image2 = uploadfile($request->file('image2'));
        // $results->image3 = uploadfile($request->file('image3'));
        $results->review = $request->review;
        $results->replyto_reviewid = $request->replyto_reviewid;
        $results->activestatus = '1';
        $results->createdby = Auth::id();

        $results->save();

        if($request->action){
            return ['1','Done'];
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = ReviewProductModel::find($id);

        return view('reviewproduct/edit',compact('results'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if($request->action){
            Auth::loginUsingId($request->userid);
        }
        
        $results = ReviewProductModel::find($id);
        $results->productid = $request->productid;
        $results->variantid = $request->variantid;
        $results->variantid_ID = $request->variantid_ID;
        $results->orderid = $request->orderid;
        $results->star = $request->star;
        $results->image1 = updatefile($results->image1, $request->file('image1'));
        $results->image2 = updatefile($results->image2, $request->file('image2'));
        $results->image3 = updatefile($results->image3, $request->file('image3'));
        $results->review = $request->review;
        $results->replyto_reviewid = $request->replyto_reviewid;
        $results->activestatus = '1';
        
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        if($request->action){
            return ['1','Done'];
        }

        // return redirect()->back();
        return redirect('reviewproductview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function approve($id)
    {

        
        $results = ReviewProductModel::find($id);
        $results->activestatus = '1';
        $results->save();

        return redirect('reviewproductview');

    }
    public function reject($id)
    {

        
        $results = ReviewProductModel::find($id);
        $results->activestatus = '0';
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");
        $results->save();


        return redirect('reviewproductview');

    }
    
    public function rejectapi(Request $request)
    {

        
        if($request->action){
            Auth::loginUsingId($request->userid);
        }

        $results = ReviewProductModel::find($request->reviewid);
        $results->activestatus = '0';
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");
        $results->save();

        if($request->action){
            return ['1','Done'];
        }


    }

    public function destroy($id)
    {
        //
        $results = ReviewProductModel::find($id);
        $results->delete();

        return redirect('reviewproductview');
    }
}
