<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResetPasswordModel;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Facades\Hash;

use App\Mail\ResetPasswordManual;
use Illuminate\Support\Facades\Mail;

class ResetPasswordManualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // return view('auth/resetpasswordform');
        return view('auth/resetpassword');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        // Get the value from the form
        $result_email = User::where('email', $request->email)->first();
        if($result_email){

            $results = ResetPasswordModel::where('activestatus','1')->where('userid',$result_email->id)->first();

            if(!$results){
            $results = new ResetPasswordModel;
            $results->resetpasskey = rand();
            $results->userid = $result_email->id;
            $results->email = $request->email;
            $results->emailsendind = 0;

            $results->activestatus = '1';

            $results->createdby = $result_email->id;

            $results->save();
            }

            $emailid = $request->email;

            if($request->action){
                return 1;
            }else{
                return view('auth/resetpasswordform',compact('emailid'));
            }


        }else{
            if($request->action){
                return 'Email Not Exist';
            }else{
                return redirect()->back()->withErrors(['Email Not Exist']);
            }
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $results = User::where('resetpasskey', $request->resetpasskey)->where('email',$request->emailid)->first();
        $emailid = $request->emailid;
        if($results){
            $results->password = Hash::make($request->newpasword);
            $results->updated_at = date("Y-m-d H:i:s");
            $results->save();
            if($request->action){
                return 1;
            }else{
                return redirect('login');
            }
        }else{
            if($request->action){
                return 'Email or Reset Pass Key Not Exist';
            }else{
                return view('auth/resetpasswordform',compact('emailid'))->withErrors(['Email or Reset Pass Key Not Exist']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function emailsend()
    {
        //

        $results = ResetPasswordModel::where('activestatus','1')->first();

        if($results){

        try {
            Mail::to($results->email)->send(new ResetPasswordManual($results));
            echo "Mail send to $results->email successfully";

            $user_update = User::find($results->userid);
            $user_update->resetpasskey = $results->resetpasskey;
            $user_update->save();


            $results->emailsendind = 1;
            $results->activestatus = '0';
            $results->save();

        } catch (\Exception $e) {
            echo 'Error - '.$e;
        }
        

        }
        
    }
}
