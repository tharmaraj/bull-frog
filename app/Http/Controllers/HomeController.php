<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MerchantStoreMaster;
use Auth;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $merchantstorecheck = MerchantStoreMaster::where('createdby', Auth::id())->get();

        return view('home', compact('merchantstorecheck'));
    }
}
