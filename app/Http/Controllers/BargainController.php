<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BargainModel;
use App\ProductStoreMaster;
use App\VariantProductMaster;
use App\User;

use Auth;

class BargainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // productid
        $results = BargainModel::all();
        foreach($results as $key=>$result){
            $variantDetails = VariantProductMaster::where('id', $result->variantid_ID)->where('activestatus', '1')->first();
            $productstore = ProductStoreMaster::where('productid', $result->productid)->where('activestatus', '1')->first();
            $users = User::find($result->createdby);
            $result['variant'] = $variantDetails;
            $result['users'] = $users;
            if($productstore->createdby != Auth::id()){
                unset($results[$key]);
            }
        }

        return view('bargain/view',compact('results'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $results = new BargainModel;
        $results->productid = $request->productid;
        $results->variantid = $request->variantid;
        $results->variantid_ID = $request->variantid_ID;
        $results->originalamount = $request->originalamount;
        if($request->customer_message){
            $results->customer_message = $request->customer_message;
        }
        if($request->merchant_message){
        $results->merchant_message = $request->merchant_message;
        }
        $results->bargainamount = $request->bargainamount;
        $results->customerbargainind = 0;
        $results->merchantbargainind = 3;
        $results->activestatus = '1';
        $results->createdby = Auth::id();

        $results->save();

        return redirect('home');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatemerchantmessage(Request $request, $id)
    {
        //
        $results = BargainModel::find($id);
        $results->merchant_message = $request->merchant_message;
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return redirect('bargainview');

    }
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject($id)
    {
        //
        $results = BargainModel::find($id);
        $results->customerbargainind = 0;
        $results->merchantbargainind = 2;
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return redirect('bargainview');
    }
    public function approve($id)
    {
        //
        $results = BargainModel::find($id);
        $results->customerbargainind = 0;
        $results->merchantbargainind = 1;
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return redirect('bargainview');
    }
    public function customerapprove($id)
    {
        //
        $results = BargainModel::find($id);
        $results->customerbargainind = 1;
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return redirect('home');
    }
    public function customerreject($id)
    {
        //
        $results = BargainModel::find($id);
        $results->customerbargainind = 2;
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return redirect('home');
    }
    public function destroy($id)
    {
        //
    }
}
