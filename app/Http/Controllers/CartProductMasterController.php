<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CartProductMaster;
use App\ProductStoreMaster;
use App\VariantProductMaster;
use App\MerchantStoreMaster;
use App\config;
use App\Voucher;
use App\VoucherEntry;
use Auth;
use DB;
use \stdClass;


class CartProductMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

            VoucherEntry::where('createdby', Auth::id())->update(['activestatus' => '2']);


            $prvresultscountsum = CartProductMaster::select('count')
            ->where('userid', Auth::user()->userid)
            ->where('activestatus', '1')
            ->sum('count');
            
            if($prvresultscountsum == 0){
                return redirect('products');
            }

            
            $response = getcartdetails(Auth::id());

            $paymentDetails = calculatePayment($response);
            
            $response = $response['data'];


    



        return view('cart/view', compact('response','paymentDetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $results_val = ProductStoreMaster::where('activestatus','1')->get();
        // dd($results_val);
        $results = array();
        
        
        foreach($results_val as $result){
            
            $variant = VariantProductMaster::where('variantid',$result->variantid)->get();
            $result['variant'] = $variant;
            array_push($results,$result);
        }
        
        // dd($results);

        return view('product_view', compact('results'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        if($request->action){
            Auth::loginUsingId($request->userid);
        }
        
        $variants = VariantProductMaster::find($request->variantid_ID); 
        $telemsg = '*'.Auth::user()->name.'* add *'.$variants->name.'* '.now();
        telegramapi($telemsg);

        
        $prvresultscountsum = CartProductMaster::select('count')->where('variantid_ID', $request->variantid_ID)->where('activestatus', '1')->where('userid', Auth::user()->userid)->sum('count');
        
        // dd($prvresultscountsum);
        
        if($prvresultscountsum != 0){
            $prvresultscountsum = CartProductMaster::where('variantid_ID', $request->variantid_ID)->where('activestatus', '1')->where('userid', Auth::user()->userid)->first();

            if($request->countProduct != null){

                if($request->countProduct != '1'){
                    $prvresultscountsum->count = $prvresultscountsum->count + $request->countProduct;
                }else{
                    $prvresultscountsum->count = $prvresultscountsum->count + 1;
                }
    
            }else{
                $prvresultscountsum->count = $prvresultscountsum->count + 1;
            }
            



            $prvresultscountsum->save();
        }else{

        $results = new CartProductMaster;
        $results->storeid = $request->storeid;
        $results->merchantid = $request->merchantid;
        $results->productid = $request->productid;
        $results->variantid = $request->variantid;
        $results->variantid_ID = $request->variantid_ID;
        $results->userid = Auth::user()->userid;

        if($request->countProduct){
            $results->count = $request->countProduct;
        }else{
            $results->count = 1;
        }
        
        $results->activestatus = '1';

        $results->createdby = Auth::id();
        $results->save();

        }

        if($request->action){
            return ['1','Done'];
        }

        return redirect('cartproductcreate');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = CartProductMaster::find($id);
        $results->storeid = $request->storeid;
        $results->merchantid = $request->merchantid;
        $results->productid = $request->productid;
        $results->variantid = $request->variantid;
        $results->variantid_ID = $request->variantid_ID;
        $results->userid = $request->userid;
        $results->count = $request->count;

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return redirect('cartproductview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function minus($id, $userid)
    {
        $results = CartProductMaster::where('variantid_ID',$id)
        ->where('activestatus','1')
        ->where('createdby',$userid)
        ->first();

        if($results->count != 1){
        $results->count = ($results->count - 1);
        $results->save();
        }

        return redirect('cartproductview');

    }
    public function destroy($id, $userid)
    {
        //

        $results = CartProductMaster::where('variantid_ID',$id)
        ->where('activestatus','1')
        ->where('createdby',$userid)
        ->first();
        $results->delete();

        return redirect('cartproductview');
    }
}
