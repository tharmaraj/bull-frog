<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CategoryMaster;
use App\PaymentSettleMaster;
use App\User;
use App\ShippingMaster;
use App\PaymentProductMaster;
use App\VariantProductMaster;
use App\Voucher;
use App\SliderMaster;
use App\ReviewProductModel;
use App\NotificationModel;
use Illuminate\Support\Facades\Hash;

use App\Mail\VerifyRegister;
use Illuminate\Support\Facades\Mail;


use Auth;
use DateTime;

class MasterApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




    public function handleRegister(Request $request)
    {
        if($request->action == 'user'){

        $result = new User;
        $result->userid = rand();
        $result->otvkey = rand(1000,9999);
        $result->role = '2';
        $result->phone = $request->phone;
        $result->activestatus = '1';
        $result->name = $request->name;
        $result->email = $request->email;
        $result->type = '2';
        $result->password = Hash::make($request->password);
        $result->save();


        // try {
        //     Mail::to('rajinfomina@gmail.com')->send(new VerifyRegister($result));
        //     // echo 'Mail send successfully';
        // } catch (\Exception $e) {
        //     // echo 'Error - '.$e;
        // }
        if($result->save()){
            return '1';
        }else{
            return '2';
        }

    }else{
            return '0';

        }

    }

    public function handleLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $activestatus = User::where('email',$request->email)->first();
        // dd($activestatus->activestatus);
        if(!$activestatus){
            return redirect()->back()->withErrors(['Login Failed Email Not Exist']);
        }
        if($activestatus->activestatus == '0'){
            return redirect()->back()->withErrors(['Activate Failed']);
        }
        
        if (Auth::attempt($credentials)) {

            if($request->action == 'user'){
                $users = User::where('email',$request->email)->first();
                return $users;
            }
            // Authentication passed...
            return redirect()->route('home');
        }

        
            
        // authentication failed...
        return redirect()->back()->withErrors(['Login','Failed']);

    }



    public function api(Request $request)
    {
        //
        if($request->action == 'category'){
            $result = CategoryMaster::where('activestatus', 1)->orderBy('index', 'asc')->get();
        }
        if($request->action == 'cart'){
            $result = getcartdetails($request->userid);
        }
        if($request->action == 'user_details'){
            $result = User::find($request->userid);
        }
        if($request->action == 'notifications_count'){

            $users_val = User::find($request->userid);

            if($users_val->role == '1'){
            $result['notification_for_header'] = NotificationModel::where('activestatus', 1)->where('is_ssen_admin','2')->count();
            $result['notification_for_snednotification'] = NotificationModel::where('activestatus', 1)->where('is_ssen_admin','1')->count();
            }else{
            $result['notification_for_header'] = NotificationModel::where('activestatus', 1)->where('is_ssen_user','2')->where('notification_owner',$request->userid)->count();
            $result['notification_for_snednotification'] = NotificationModel::where('activestatus', 1)->where('is_ssen_user','1')->where('notification_owner',$request->userid)->count();
            }
        }
        
        if($request->action == 'notifications'){

            $users_val = User::find($request->userid);

            if($users_val->role == '1'){
            $result = NotificationModel::where('activestatus', 1)->where('is_ssen_admin','2')->get();
            NotificationModel::where('activestatus', 1)->where('is_ssen_admin','2')->update(['activestatus' => '0','is_ssen_admin' => '3']);
            }else{
            $result = NotificationModel::where('activestatus', 1)->where('is_ssen_user','2')->where('notification_owner',$request->userid)->get();
            NotificationModel::where('activestatus', 1)->where('is_ssen_user','2')->where('notification_owner',$request->userid)->update(['activestatus' => '0','is_ssen_user' => '3']);
            }
        }
        if($request->action == 'notifications_all'){
            $users_val = User::find($request->userid);

            if($users_val->role == '1'){
            $result = NotificationModel::where('is_ssen_admin','3')->get();
            }else{
            $result = NotificationModel::where('is_ssen_user','3')->where('notification_owner',$request->userid)->get();
            }
        }

        if($request->action == 'notifications_notification_sent'){
            $users_val = User::find($request->userid);

            if($users_val->role == '1'){
            $result = NotificationModel::where('activestatus', 1)->where('is_ssen_admin','1')->get();
            NotificationModel::where('activestatus', 1)->where('is_ssen_admin','1')->update(['is_ssen_admin' => '2']);
            }else{
            $result = NotificationModel::where('activestatus', 1)->where('is_ssen_user','1')->where('notification_owner',$request->userid)->get();
            NotificationModel::where('activestatus', 1)->where('is_ssen_user','1')->where('notification_owner',$request->userid)->update(['is_ssen_user' => '2']);
            }

        }
        if($request->action == 'slider'){

            $result = SliderMaster::where('activestatus', 1)->where('adds',$request->modaltype)->get();

            foreach($result as $key => $res){

                $datetime_now = new DateTime();
                $datetime1 = new DateTime($res->startdate);
                $datetime2 = new DateTime($res->enddate);

                
                if ( $datetime_now < $datetime1 ) {
                    // dd('Not Start Yet');
                    unset($result[$key]);
                }else{
                    if ( $datetime_now > $datetime2 ) {
                        // dd('Finished Bro');
                        unset($result[$key]);
                    }
                }


            }
        }
        if($request->action == 'shipping_details'){
            $result = ShippingMaster::where('createdby',$request->userid)->where('activestatus', 1)->get();
        }
        if($request->action == 'shipping_details_all'){
            $result = ShippingMaster::where('createdby',$request->userid)->orderBy('activestatus', 'desc')->get();
        }
        if($request->action == 'voucher_list'){
            $result_user = User::find($request->userid);
            $result = Voucher::where('user','*')->orWhere('user', $result_user->userid)->where('activestatus', 1)->orderBy('user', 'desc')->get();
        }
        if($request->action == 'order_invoice'){
            
            $result = PaymentSettleMaster::where('orderid',$request->orderid)->get();
            
            foreach($result as $res){
                $result_users = User::find($res->createdby);
                $decodes = unserialize($res->data);
                $paymentDetails = calculatePayment($decodes);
                $res['decodes'] = $decodes;
                $res['users'] = $result_users;
                $res['paymentDetails'] = $paymentDetails;

                $resultShipping = ShippingMaster::where('createdby',$res->createdby)->where('activestatus', 1)->first();

                $res['shippingDetails'] = $resultShipping;
                unset($res['data']);

            }

        }
        if($request->action == 'history'){

            $result_users = User::find($request->userid);
            if($result_users->role == '1'){
                $result = PaymentProductMaster::where('finalsubmit','!=','0')->where('orderpaymentstatus','!=','3')->orderBy('id', 'desc')->get();
            }else{
                $result = PaymentProductMaster::where('createdby', $request->userid)->where('finalsubmit','!=','0')->where('orderpaymentstatus','!=','3')->orderBy('id', 'desc')->get();

            }


            $count_pending = PaymentProductMaster::where('createdby', $request->userid)->where('orderpaymentstatus','!=','3')->where('ordershippingstatus', '1')->count();
            $count_outofdelivery = PaymentProductMaster::where('createdby', $request->userid)->where('orderpaymentstatus','!=','3')->where('ordershippingstatus', '2')->count();
            $count_delivered = PaymentProductMaster::where('createdby', $request->userid)->where('orderpaymentstatus','!=','3')->where('ordershippingstatus', '3')->count();
            foreach($result as $res){
                $variant_details = VariantProductMaster::find($res->variantid_ID);
                $reviewproducts = ReviewProductModel::where('variantid_ID', $res->variantid_ID)->where('orderid', $res->orderid)->where('createdby', $request->userid)->get();
                
                foreach($reviewproducts as $key => $reviewproduct){
                    $reviewproducts_reply = ReviewProductModel::where('replyto_reviewid',$reviewproduct->id)->where('createdby', $request->userid)->get();
                    $reviewproduct['replies'] = $reviewproducts_reply;

                    if($reviewproduct->replyto_reviewid != null){
                        unset($reviewproducts[$key]);
                    }
                }


                
                $res['variant_details'] = $variant_details;
                $res['reviews'] = $reviewproducts;
            }
            // $result['count_pending'] = $count_pending;
            // $result['count_outofdelivery'] = $count_outofdelivery;
            // $result['count_delivered'] = $count_delivered;
        }

        return $result;
    }




}
