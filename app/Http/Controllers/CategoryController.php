<?php

namespace App\Http\Controllers;

use App\CategoryMaster;
use Illuminate\Http\Request;

use URL;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = CategoryMaster::all();
        return view('category/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $results = CategoryMaster::orderBy('id','desc')->first();
        return view('category/store',compact('results'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $results = new CategoryMaster;
        $results->name = $request->name;
        $results->image = uploadfile($request->file('image'));
        $results->index = $request->index;
        $results->activestatus = '1';

        $results->save();

        return view('category/store',compact('results'));

        // return redirect('categoryview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = CategoryMaster::find($id);

        return view('category/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = CategoryMaster::find($id);
        $results->name = $request->name;
        $results->image = updatefile($results->image, $request->file('image'));
        $results->index = $request->index;
        $results->activestatus = '1';

        $results->save();

        return redirect('categoryview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $results = CategoryMaster::find($id);
        deletefile($results->image);
        $results->delete();

        return redirect('categoryview');
    }
}
