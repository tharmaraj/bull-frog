<?php

namespace App\Http\Controllers;

use App\VariantItemListMaster;
use Illuminate\Http\Request;

use URL;
use Auth;
use Redirect;

class VariantItemListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = VariantItemListMaster::all();
        return view('variantitemlist/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('variantitemlist/store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $results = new VariantItemListMaster;
        $results->photo1 = uploadfile($request->file('photo1'));
        $results->variantid = $request->variantid;
        $results->variant_ID = $request->variant_ID;
        $results->name = $request->name;
        $results->count = $request->count;
        $results->activestatus = '1';

        $results->createdby = Auth::id();

        $results->save();

        return redirect()->back();
        // return redirect('variantitemlistview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = VariantItemListMaster::find($id);

        return view('variantitemlist/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = VariantItemListMaster::find($id);
        $results->photo1 =  updatefile($results->photo1, $request->file('photo1')); 
        $results->variantid = $request->variantid;
        $results->variant_ID = $request->variant_ID;
        $results->name = $request->name;
        $results->count = $request->count;
        $results->activestatus = '1';

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return Redirect::route('viewvariant',$results->variantid);
        // return redirect()->back();
        // return redirect('variantitemlistview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $results = VariantItemListMaster::find($id);
        deletefile($results->photo1);
        $results->delete();

        return redirect()->back();
        // return redirect('variantitemlistview');
    }
}
