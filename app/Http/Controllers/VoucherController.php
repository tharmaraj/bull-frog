<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voucher;

use Auth;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = Voucher::all();
        // dd($results);
        return view('voucher/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('voucher/store');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $results = new Voucher;

        $results->voucherid = $request->voucherid;
        $results->name = $request->name;
        $results->startdatetime = $request->startdatetime;
        $results->enddatetime = $request->enddatetime;
        $results->voucherpricetype = $request->voucherpricetype;
        $results->user = $request->user;
        $results->value = $request->value;
        $results->minimumpurchase = $request->minimumpurchase;
        $results->activestatus = '1';

        $results->createdby = Auth::id();

        $results->save();

        return redirect('voucherview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = Voucher::find($id);

        return view('voucher/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = Voucher::find($id);

        $results->name = $request->name;
        $results->startdatetime = $request->startdatetime;
        $results->enddatetime = $request->enddatetime;
        $results->voucherpricetype = $request->voucherpricetype;
        $results->user = $request->user;
        $results->value = $request->value;
        $results->minimumpurchase = $request->minimumpurchase;

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return redirect('voucherview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function approve($id)
    {

        
        $results = Voucher::find($id);
        $results->activestatus = '1';
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");
        $results->save();

        return redirect('voucherview');

    }
    public function reject($id)
    {

        
        $results = Voucher::find($id);
        $results->activestatus = '0';
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");
        $results->save();

        return redirect('voucherview');

    }
    public function destroy($id)
    {
        //
        $results = Voucher::find($id);
        $results->delete();

        return redirect('voucherview');
    }
}
