<?php

namespace App\Http\Controllers;

use App\config;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    public function deleteaccount(Request $request)
    {
        //
        $credentials = $request->only('email', 'password');

        $activestatus = User::where('email',$request->email)->first();

        if($activestatus){
            $password_chck = Hash::check($request->password, $activestatus->password);
            if($password_chck){
                $activestatus->activestatus = 0;
                $activestatus->save();
            }else{
                return redirect()->back()->withErrors(['Failed Password Not Correct']);
            }

        }else{
            return redirect()->back()->withErrors(['Failed Email Not Exist']);
        }

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function api()
    {
        //
        $val = config::where('activestatus', 1)->get();
        foreach($val as $vl){
            $vl->minimumpurchasevalue = number_format($vl->minimumpurchasevalue,2);
        }
        return $val;
    }
    public function getTotalCartCount(Request $request)
    {
        //
        $results = gettotalcart($request->userid);
        return $results;
    }
}
