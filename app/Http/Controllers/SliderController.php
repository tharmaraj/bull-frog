<?php

namespace App\Http\Controllers;

use App\SliderMaster;
use Illuminate\Http\Request;

use Auth;
use URL;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = SliderMaster::all();
        return view('slider/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('slider/store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $results = new SliderMaster;
        $results->image = uploadfile($request->file('image'));
        $results->name = $request->name;
        $results->url = $request->url;
        $results->productid = $request->productid;
        $results->startdate = $request->startdate;
        $results->enddate = $request->enddate;
        $results->adds = $request->adds;
        $results->activestatus = '1';

        $results->save();

        return redirect('sliderview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = SliderMaster::find($id);

        return view('slider/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = SliderMaster::find($id);
        $results->image =  updatefile($results->image, $request->file('image')); 
        $results->name = $request->name;
        $results->url = $request->url;
        $results->productid = $request->productid;
        $results->startdate = $request->startdate;
        $results->enddate = $request->enddate;
        $results->adds = $request->adds;
        $results->activestatus = '1';

        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");

        $results->save();

        return redirect('sliderview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function approve($id)
    {

        
        $results = SliderMaster::find($id);
        $results->activestatus = '1';
        $results->save();

        return redirect('sliderview');

    }
    public function reject($id)
    {

        
        $results = SliderMaster::find($id);
        $results->activestatus = '0';
        $results->updatedby = Auth::id();
        $results->updateddate = date("Y-m-d H:i:s");
        $results->save();

        return redirect('sliderview');

    }
    public function destroy($id)
    {
        //
        $results = SliderMaster::find($id);
        deletefile($results->image);
        $results->delete();

        return redirect('sliderview');
    }
}
