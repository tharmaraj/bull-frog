<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentProductMaster;
use App\ShippingMaster;
use App\User;
use Auth;

class PaymentProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $shippingaddress = ShippingMaster::where('userid',Auth::id())->where('activestatus','1')->first();
        if(Auth::user()->role == '1'){
            $results = PaymentProductMaster::where('finalsubmit', '0')->get();
        }else{
            $results = PaymentProductMaster::where('createdby', Auth::id())->where('finalsubmit', '0')->get();
        }


        $response = getcartdetails(Auth::id());

        $paymentDetails = calculatePayment($response);

        // dd($response);

        return view('paymentproduct/view', compact('paymentDetails','results','shippingaddress','response'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        paymentProduct(Auth::id());

        return redirect('paymentproductview');

    }

    public function createapi(Request $request)
    {
        //
        if($request->action){
            Auth::loginUsingId($request->userid);
        }
        $shippingaddress = ShippingMaster::where('userid',Auth::id())->where('activestatus','1')->first();
        $users = User::find(Auth::id());
        paymentProduct(Auth::id());
        $results = PaymentProductMaster::where('createdby', Auth::id())->where('finalsubmit', '0')->get();
        $response = getcartdetails(Auth::id());

        $paymentDetails = calculatePayment($response);

        return compact('results','response','paymentDetails','shippingaddress','users');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
