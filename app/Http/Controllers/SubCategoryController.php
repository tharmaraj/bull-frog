<?php

namespace App\Http\Controllers;

use App\SubCategoryMaster;
use App\CategoryMaster;
use Illuminate\Http\Request;

use URL;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = SubCategoryMaster::all();
        return view('subcategory/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = CategoryMaster::all();
        $results = SubCategoryMaster::orderBy('id','desc')->first();
        return view('subcategory/store',compact('results','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $categories = CategoryMaster::all();
        $results = new SubCategoryMaster;
        $results->name = $request->name;
        $results->category = $request->category;
        $results->image = uploadfile($request->file('image'));
        $results->index = $request->index;
        $results->activestatus = '1';

        $results->save();

        return view('subcategory/store',compact('results','categories'));

        // return redirect('subcategoryview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categories = CategoryMaster::all();
        $results = SubCategoryMaster::find($id);

        return view('subcategory/edit',compact('results','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = SubCategoryMaster::find($id);
        $results->name = $request->name;
        $results->category = $request->category;
        $results->image = updatefile($results->image, $request->file('image'));
        $results->index = $request->index;
        $results->activestatus = '1';

        $results->save();

        return redirect('subcategoryview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $results = SubCategoryMaster::find($id);
        deletefile($results->image);
        $results->delete();

        return redirect('subcategoryview');
    }
}
