<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentProductMaster;
use App\MerchantMaster;
use App\ReviewProductModel;
use App\VariantProductMaster;
use Auth;

class HistoryMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->role == '1'){
            $results = PaymentProductMaster::all();
        }else{
            $results = PaymentProductMaster::where('createdby', Auth::id())->get();
        }
        
        foreach($results as $result){
            if(Auth::user()->role == '1'){
                $reviewproducts = ReviewProductModel::where('variantid_ID', $result->variantid_ID)->where('orderid', $result->orderid)->get();
            }else{
                $reviewproducts = ReviewProductModel::where('variantid_ID', $result->variantid_ID)->where('orderid', $result->orderid)->where('createdby', Auth::id())->get();
            }

            $result['reviews'] = $reviewproducts;
        }
        
        return view('history/view',compact('results'));
    }
    public function indexmerchant()
    {
        $merchant_details = MerchantMaster::where('userid',Auth::user()->userid)->first();

        $results = PaymentProductMaster::where('finalsubmit', '1')
        ->where('merchantid',$merchant_details->merchantid)
        ->where('ordershippingstatus','!=','3')
        ->get();

        return view('history/viewmerchant',compact('results'));
    }
    public function indexmerchantdelivered()
    {
        $merchant_details = MerchantMaster::where('userid',Auth::user()->userid)->first();

        $results = PaymentProductMaster::where('finalsubmit', '1')
        ->where('merchantid',$merchant_details->merchantid)
        ->where('ordershippingstatus','3')
        ->get();

        return view('history/viewmerchant',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editmerchantshippingstatus($id, $userid, $appind)
    {
        //
        $arr = explode('~',$id);
        $results = PaymentProductMaster::find($arr[0]);
        $results->ordershippingstatus = $arr[1];
        $results->save();

        $shipval = $arr[1];
        $productstatus = '';
        if($shipval == '1'){ $productstatus = 'is being Packed'; }
        if($shipval == '2'){ $productstatus = 'is Out Of Delivery'; }
        if($shipval == '3'){ $productstatus = 'is Delivered'; }

        $variantdetails = VariantProductMaster::find($results->variantid_ID);

        insertNotification('1', 'Shipping Status', "Your Order $variantdetails->name $productstatus", '1', '1',$results->createdby, '1', $userid);

        if($appind == 1){
            return $results;
        }
        return redirect('historymerchantmasterview');


    }
    public function editmerchantpaymentstatus($id, $userid)
    {
        //
        $arr = explode('~',$id);
        $results = PaymentProductMaster::find($arr[0]);
        $results->orderpaymentstatus = $arr[1];
        $results->save();

        $shipval = $arr[1];
        $productstatus = '';
        if($shipval == '1'){ $productstatus = 'is Pending'; }
        if($shipval == '2'){ $productstatus = 'is Payed'; }
        if($shipval == '3'){ $productstatus = 'is Failed'; }

        $variantdetails = VariantProductMaster::find($results->variantid_ID);

        insertNotification('1', 'Shipping Status', "Your Payment for $variantdetails->name $productstatus", '1', '1', $results->createdby, '1', $userid);

        if($appind == 1){
            return $results;
        }

        return redirect('historymerchantmasterview');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
