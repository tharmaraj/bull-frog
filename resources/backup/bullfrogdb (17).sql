-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 24, 2021 at 04:23 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bullfrogdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_product_masters`
--

DROP TABLE IF EXISTS `cart_product_masters`;
CREATE TABLE IF NOT EXISTS `cart_product_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storeid` int(11) NOT NULL,
  `merchantid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `variantid` int(11) NOT NULL,
  `variantid_ID` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_product_masters`
--

INSERT INTO `cart_product_masters` (`id`, `storeid`, `merchantid`, `productid`, `variantid`, `variantid_ID`, `userid`, `count`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, 11489457, 1058152664, 622358871, 1807205330, 5, 1916653428, 2, '2', '3', NULL, NULL, '2021-03-30 01:53:32', '2021-04-19 07:07:09'),
(2, 11489457, 1058152664, 420264236, 1381582827, 4, 1916653428, 2, '2', '3', NULL, NULL, '2021-03-30 01:53:45', '2021-04-19 07:07:09'),
(3, 534211849, 742239204, 800097549, 1075046348, 2, 843480070, 1, '2', '6', NULL, NULL, '2021-03-31 09:26:27', '2021-04-01 04:42:41'),
(4, 11489457, 1058152664, 420264236, 1381582827, 4, 843480070, 2, '2', '6', NULL, NULL, '2021-03-31 09:26:30', '2021-04-01 04:42:41'),
(5, 534211849, 742239204, 800097549, 1075046348, 1, 843480070, 1, '2', '6', NULL, NULL, '2021-04-01 04:32:28', '2021-04-01 04:42:41'),
(6, 534211849, 742239204, 1795879308, 1119887063, 3, 843480070, 1, '1', '6', NULL, NULL, '2021-04-01 06:23:27', '2021-04-01 06:23:27'),
(7, 11489457, 1058152664, 420264236, 1381582827, 4, 843480070, 1, '1', '6', NULL, NULL, '2021-04-01 06:23:28', '2021-04-01 06:23:28'),
(8, 11489457, 1058152664, 622358871, 1807205330, 5, 843480070, 1, '1', '6', NULL, NULL, '2021-04-01 07:15:13', '2021-04-01 07:15:13'),
(9, 534211849, 742239204, 800097549, 1075046348, 1, 843480070, 1, '1', '6', NULL, NULL, '2021-04-01 07:19:43', '2021-04-01 07:19:43'),
(10, 534211849, 742239204, 800097549, 1075046348, 1, 1916653428, 5, '2', '3', NULL, NULL, '2021-04-07 01:14:57', '2021-04-19 07:07:09'),
(11, 534211849, 742239204, 800097549, 1075046348, 2, 1916653428, 3, '2', '3', NULL, NULL, '2021-04-07 01:16:27', '2021-04-19 07:07:09'),
(12, 534211849, 742239204, 800097549, 1075046348, 1, 1916653428, 1, '2', '3', NULL, NULL, '2021-04-19 06:49:50', '2021-04-19 07:07:09'),
(13, 534211849, 742239204, 800097549, 1075046348, 2, 1916653428, 1, '2', '3', NULL, NULL, '2021-04-19 06:50:26', '2021-04-19 07:07:09'),
(14, 534211849, 742239204, 800097549, 1075046348, 2, 1916653428, 1, '2', '3', NULL, NULL, '2021-04-19 07:06:55', '2021-04-19 07:07:09'),
(15, 534211849, 742239204, 800097549, 1075046348, 1, 1916653428, 6, '1', '3', NULL, NULL, '2021-04-19 08:54:03', '2021-04-19 10:03:01'),
(16, 534211849, 742239204, 800097549, 1075046348, 2, 1916653428, 2, '1', '3', NULL, NULL, '2021-04-19 08:54:06', '2021-04-19 10:04:35');

-- --------------------------------------------------------

--
-- Table structure for table `category_masters`
--

DROP TABLE IF EXISTS `category_masters`;
CREATE TABLE IF NOT EXISTS `category_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `index` int(11) NOT NULL,
  `activestatus` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_masters`
--

INSERT INTO `category_masters` (`id`, `name`, `image`, `index`, `activestatus`, `created_at`, `updated_at`) VALUES
(1, 'Chicken', '983626932.png', 2, 1, NULL, '2021-04-22 07:22:23'),
(2, 'Fish', 'http://bullfrog.softwareprojectplus.com/images/fish.png', 3, 1, NULL, NULL),
(3, 'Goat', 'http://bullfrog.softwareprojectplus.com/images/goat.png', 1, 1, NULL, NULL),
(10, 'gayu', 'http://127.0.0.1:8000/files/2076734842.png', 1, 1, '2021-04-21 03:48:48', '2021-04-21 03:48:55');

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

DROP TABLE IF EXISTS `configs`;
CREATE TABLE IF NOT EXISTS `configs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cod` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fpx` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `companyname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `string1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `string2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `string3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `string4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `string5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `string6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seeall` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryColor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondaryColor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `infoColor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warningColor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangerColor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `succcessColor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingCommission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `companyCommission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ipayCommission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `backgroundimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `limitproductformerchant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '10',
  `maxproductvaluemerchant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '200',
  `minproductvaluemerchant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '10',
  `companycommissionpercentage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '11',
  `ipay88commissionpercentage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `defaultshippingprice` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '4',
  `defaultProfileImage` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noDataImage` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `cod`, `fpx`, `companyname`, `string1`, `string2`, `string3`, `string4`, `string5`, `string6`, `seeall`, `primaryColor`, `secondaryColor`, `infoColor`, `warningColor`, `dangerColor`, `succcessColor`, `shippingCommission`, `companyCommission`, `ipayCommission`, `backgroundimage`, `limitproductformerchant`, `maxproductvaluemerchant`, `minproductvaluemerchant`, `companycommissionpercentage`, `ipay88commissionpercentage`, `defaultshippingprice`, `defaultProfileImage`, `noDataImage`, `activestatus`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Froggy', 'Top Sell', 'Easy Pack', NULL, NULL, NULL, NULL, 'See All', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://bullfrog.softwareprojectplus.com/images/chickenmeat.jpg', '10', '200', '10', '11', '0', '2', NULL, 'http://bullfrog.softwareprojectplus.com/images/nodata.png', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_masters`
--

DROP TABLE IF EXISTS `history_masters`;
CREATE TABLE IF NOT EXISTS `history_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `merchant_masters`
--

DROP TABLE IF EXISTS `merchant_masters`;
CREATE TABLE IF NOT EXISTS `merchant_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `merchantid` int(11) NOT NULL,
  `activestatus` int(11) NOT NULL,
  `profileimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supportdoc1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supportdoc2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supportdoc3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supportdoc4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supportdoc5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poscood` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliveryavailable` int(11) NOT NULL,
  `storeactivestatus` int(11) NOT NULL,
  `limitproduct` int(11) NOT NULL,
  `maxproductvalue` int(11) NOT NULL,
  `minproductvalue` int(11) NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merchant_masters`
--

INSERT INTO `merchant_masters` (`id`, `userid`, `merchantid`, `activestatus`, `profileimage`, `supportdoc1`, `supportdoc2`, `supportdoc3`, `supportdoc4`, `supportdoc5`, `name`, `email`, `ic`, `phone`, `address1`, `address2`, `poscood`, `city`, `state`, `deliveryavailable`, `storeactivestatus`, `limitproduct`, `maxproductvalue`, `minproductvalue`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, 579594918, 742239204, 0, '1503185513.png', '', '', NULL, NULL, NULL, 'Tharmaraj', 'tharmaraj123@yahoo.com', '941103101231', '01231020302', 'Lot 177 Batu 28 1/4', 'Morib Sepang', '42700', 'banting', '10', 1, 1, 10, 200, 10, '2', '2', '2021-04-20 12:01:45', '2021-03-10 00:42:04', '2021-04-20 04:01:45'),
(2, 1327250225, 1058152664, 0, '', '', '', NULL, NULL, NULL, 'Merchant 122', 'tharmaraj123@yahoo.com', '123', '+60133052358', 'lot 17 jalan morib', '123', '123', '123', '123', 123, 1, 10, 200, 10, '4', '1', '2021-03-12 03:51:55', '2021-03-11 19:47:52', '2021-03-11 19:51:55'),
(3, 833496386, 474089534, 0, '1497587901.png', '', '', NULL, NULL, NULL, 'Banting MFF', 'banting@gmil.com', '123123', '123123', 'adads', 'weqqew', '22112', '12', '22', 1, 1, 10, 200, 10, '5', '1', '2021-03-25 09:58:04', '2021-03-25 01:53:33', '2021-03-25 01:58:04');

-- --------------------------------------------------------

--
-- Table structure for table `merchant_store_masters`
--

DROP TABLE IF EXISTS `merchant_store_masters`;
CREATE TABLE IF NOT EXISTS `merchant_store_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storeid` int(11) NOT NULL,
  `merchantid` int(11) NOT NULL,
  `storeimage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `storelogo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullstorename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickstorename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `storephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poscood` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `openingtime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `closingtime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `holiday` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `defaultshippingprice` double NOT NULL,
  `cod` int(11) NOT NULL DEFAULT '0',
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merchant_store_masters`
--

INSERT INTO `merchant_store_masters` (`id`, `storeid`, `merchantid`, `storeimage`, `storelogo`, `fullstorename`, `nickstorename`, `storephone`, `address1`, `address2`, `poscood`, `city`, `state`, `openingtime`, `closingtime`, `holiday`, `instagram`, `facebook`, `youtube`, `website`, `location`, `defaultshippingprice`, `cod`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, 534211849, 742239204, '1194878366.jpg', '', 'Morib Frozen', 'tharmaraj123@yahoo.com', '9322192920', 'Lot 171 Tongkah', 'Jugra Morib', '42700', '10', '10', '08:00', '20:00', 'Nope', 'instagram', 'facebook', 'youtube', 'www.webiste.com', 'morib', 9, 1, '1', '2', '2', '2021-04-20 12:00:14', '2021-03-10 00:43:23', '2021-04-20 04:00:14'),
(2, 11489457, 1058152664, '1781884250.jpg', '', 'Store Baru', 'softwstore@yahoo.com@gmail.com', '0133052350', 'lot 17 jalan morib', '312', '321', '312', '123', '23:52', '11:52', '132', '312', '132', '213', '132', '312', 7, 0, '1', '4', '4', '2021-03-15 06:48:33', '2021-03-11 19:52:28', '2021-03-14 22:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_02_19_045522_create_configs_table', 1),
(5, '2021_03_01_031157_create_merchant_masters_table', 1),
(6, '2021_03_01_031930_create_category_masters_table', 1),
(7, '2021_03_01_031938_create_sub_category_masters_table', 1),
(8, '2021_03_01_032220_create_merchant_store_masters_table', 1),
(9, '2021_03_01_033539_create_product_store_masters_table', 1),
(10, '2021_03_01_035923_create_product_unit_measurement_masters_table', 1),
(11, '2021_03_01_040627_create_variant_product_masters_table', 1),
(12, '2021_03_01_041831_create_cart_product_masters_table', 1),
(13, '2021_03_01_042606_create_payment_product_masters_table', 1),
(14, '2021_03_01_062037_create_payment_settle_masters_table', 1),
(15, '2021_03_16_085525_create_shipping_masters_table', 2),
(16, '2021_03_16_145831_create_order_payment_statuses_table', 3),
(17, '2021_03_16_145918_create_order_shipping_statuses_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `order_payment_statuses`
--

DROP TABLE IF EXISTS `order_payment_statuses`;
CREATE TABLE IF NOT EXISTS `order_payment_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `index` int(11) NOT NULL,
  `activestatus` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_shipping_statuses`
--

DROP TABLE IF EXISTS `order_shipping_statuses`;
CREATE TABLE IF NOT EXISTS `order_shipping_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `index` int(11) NOT NULL,
  `activestatus` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_shipping_statuses`
--

INSERT INTO `order_shipping_statuses` (`id`, `name`, `image`, `index`, `activestatus`, `created_at`, `updated_at`) VALUES
(1, 'test', '', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_product_masters`
--

DROP TABLE IF EXISTS `payment_product_masters`;
CREATE TABLE IF NOT EXISTS `payment_product_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storeid` int(11) NOT NULL,
  `merchantid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `variantid` int(11) NOT NULL,
  `variantid_ID` int(100) NOT NULL,
  `userid` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `shippingvalue` double NOT NULL,
  `totalcart` double NOT NULL,
  `total` double NOT NULL,
  `subtotal` double NOT NULL,
  `finalsubmit` int(11) NOT NULL,
  `ordershippingstatus` int(11) NOT NULL,
  `orderpaymentstatus` int(100) NOT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=318 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_product_masters`
--

INSERT INTO `payment_product_masters` (`id`, `storeid`, `merchantid`, `productid`, `variantid`, `variantid_ID`, `userid`, `orderid`, `count`, `shippingvalue`, `totalcart`, `total`, `subtotal`, `finalsubmit`, `ordershippingstatus`, `orderpaymentstatus`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(314, 534211849, 742239204, 800097549, 1075046348, 2, 1916653428, 2003580666, 3, 4, 3, 36, 108, 1, 1, 1, '1', '3', NULL, NULL, '2021-04-19 06:49:24', '2021-04-19 06:49:29'),
(313, 534211849, 742239204, 800097549, 1075046348, 1, 1916653428, 2003580666, 5, 4, 5, 21, 105, 1, 1, 1, '1', '3', NULL, NULL, '2021-04-19 06:49:24', '2021-04-19 06:49:29'),
(58, 534211849, 742239204, 800097549, 1075046348, 2, 843480070, 956052622, 1, 4, 1, 36, 36, 1, 3, 1, '1', '6', NULL, NULL, '2021-04-01 04:21:47', '2021-04-19 04:08:04'),
(57, 11489457, 1058152664, 420264236, 1381582827, 4, 843480070, 956052622, 2, 4, 2, 23, 46, 1, 1, 1, '1', '6', NULL, NULL, '2021-04-01 04:21:47', '2021-04-01 04:29:39'),
(59, 534211849, 742239204, 800097549, 1075046348, 1, 843480070, 183173033, 1, 4, 1, 21, 21, 1, 1, 1, '1', '6', NULL, NULL, '2021-04-01 04:42:37', '2021-04-01 04:42:41'),
(190, 534211849, 742239204, 800097549, 1075046348, 1, 843480070, 1623027235, 1, 4, 1, 21, 21, 0, 1, 1, '1', '6', NULL, NULL, '2021-04-01 07:19:47', '2021-04-01 07:19:47'),
(189, 534211849, 742239204, 1795879308, 1119887063, 3, 843480070, 1623027235, 1, 4, 1, 59, 59, 0, 1, 1, '1', '6', NULL, NULL, '2021-04-01 07:19:47', '2021-04-01 07:19:47'),
(188, 11489457, 1058152664, 622358871, 1807205330, 5, 843480070, 1623027235, 1, 2, 1, 27, 27, 0, 1, 1, '1', '6', NULL, NULL, '2021-04-01 07:19:47', '2021-04-01 07:19:47'),
(187, 11489457, 1058152664, 420264236, 1381582827, 4, 843480070, 1623027235, 1, 4, 1, 23, 23, 0, 1, 1, '1', '6', NULL, NULL, '2021-04-01 07:19:47', '2021-04-01 07:19:47'),
(312, 11489457, 1058152664, 420264236, 1381582827, 4, 1916653428, 2003580666, 2, 4, 2, 23, 46, 1, 1, 1, '1', '3', NULL, NULL, '2021-04-19 06:49:24', '2021-04-19 06:49:29'),
(311, 11489457, 1058152664, 622358871, 1807205330, 5, 1916653428, 2003580666, 2, 2, 2, 27, 54, 1, 1, 1, '0', '3', NULL, NULL, '2021-04-19 06:49:24', '2021-04-19 06:49:29'),
(315, 534211849, 742239204, 800097549, 1075046348, 1, 1916653428, 464674032, 1, 4, 1, 21, 21, 1, 1, 1, '1', '3', NULL, NULL, '2021-04-19 06:49:55', '2021-04-19 06:49:59'),
(316, 534211849, 742239204, 800097549, 1075046348, 2, 1916653428, 729531525, 1, 4, 1, 36, 36, 1, 1, 1, '1', '3', NULL, NULL, '2021-04-19 06:50:31', '2021-04-19 06:51:43'),
(317, 534211849, 742239204, 800097549, 1075046348, 2, 1916653428, 50748065, 1, 4, 1, 36, 36, 1, 1, 1, '1', '3', NULL, NULL, '2021-04-19 07:07:05', '2021-04-19 07:07:09');

-- --------------------------------------------------------

--
-- Table structure for table `payment_settle_masters`
--

DROP TABLE IF EXISTS `payment_settle_masters`;
CREATE TABLE IF NOT EXISTS `payment_settle_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `orderid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucherid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalvalue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentmethod` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shippingaddressid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci,
  `changesmoney` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_settle_masters`
--

INSERT INTO `payment_settle_masters` (`id`, `userid`, `orderid`, `voucherid`, `totalvalue`, `paymentmethod`, `phone`, `shippingaddressid`, `data`, `changesmoney`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, 843480070, '956052622', NULL, '90', '1', '0133052350', '5', 'a:3:{s:4:\"data\";a:2:{i:0;a:11:{s:7:\"storeid\";i:11489457;s:6:\"userid\";s:9:\"843480070\";s:3:\"ids\";a:1:{i:0;s:1:\"4\";}s:12:\"variantid_ID\";a:1:{i:0;s:1:\"4\";}s:15:\"variantid_Count\";a:1:{i:0;s:1:\"2\";}s:17:\"variantid_Details\";a:1:{i:0;O:24:\"App\\VariantProductMaster\":27:{s:13:\"\0*\0connection\";s:5:\"mysql\";s:8:\"\0*\0table\";s:23:\"variant_product_masters\";s:13:\"\0*\0primaryKey\";s:2:\"id\";s:10:\"\0*\0keyType\";s:3:\"int\";s:12:\"incrementing\";b:1;s:7:\"\0*\0with\";a:0:{}s:12:\"\0*\0withCount\";a:0:{}s:10:\"\0*\0perPage\";i:15;s:6:\"exists\";b:1;s:18:\"wasRecentlyCreated\";b:0;s:13:\"\0*\0attributes\";a:21:{s:2:\"id\";i:4;s:7:\"storeid\";i:11489457;s:10:\"merchantid\";i:1058152664;s:9:\"productid\";i:420264236;s:9:\"variantid\";i:1381582827;s:4:\"name\";s:7:\"Ikan YU\";s:5:\"photo\";s:15:\"1452881825.webp\";s:14:\"availablestock\";i:21;s:19:\"variantactivestatus\";i:1;s:12:\"sellingprice\";d:23;s:13:\"shippingprice\";d:4;s:7:\"company\";d:2.53;s:4:\"ipay\";d:0;s:6:\"profit\";d:20.47;s:10:\"topsellind\";i:1;s:12:\"activestatus\";s:1:\"1\";s:9:\"createdby\";s:1:\"4\";s:9:\"updatedby\";N;s:11:\"updateddate\";N;s:10:\"created_at\";s:19:\"2021-03-12 03:53:52\";s:10:\"updated_at\";s:19:\"2021-03-25 09:38:47\";}s:11:\"\0*\0original\";a:21:{s:2:\"id\";i:4;s:7:\"storeid\";i:11489457;s:10:\"merchantid\";i:1058152664;s:9:\"productid\";i:420264236;s:9:\"variantid\";i:1381582827;s:4:\"name\";s:7:\"Ikan YU\";s:5:\"photo\";s:15:\"1452881825.webp\";s:14:\"availablestock\";i:21;s:19:\"variantactivestatus\";i:1;s:12:\"sellingprice\";d:23;s:13:\"shippingprice\";d:4;s:7:\"company\";d:2.53;s:4:\"ipay\";d:0;s:6:\"profit\";d:20.47;s:10:\"topsellind\";i:1;s:12:\"activestatus\";s:1:\"1\";s:9:\"createdby\";s:1:\"4\";s:9:\"updatedby\";N;s:11:\"updateddate\";N;s:10:\"created_at\";s:19:\"2021-03-12 03:53:52\";s:10:\"updated_at\";s:19:\"2021-03-25 09:38:47\";}s:10:\"\0*\0changes\";a:0:{}s:8:\"\0*\0casts\";a:0:{}s:17:\"\0*\0classCastCache\";a:0:{}s:8:\"\0*\0dates\";a:0:{}s:13:\"\0*\0dateFormat\";N;s:10:\"\0*\0appends\";a:0:{}s:19:\"\0*\0dispatchesEvents\";a:0:{}s:14:\"\0*\0observables\";a:0:{}s:12:\"\0*\0relations\";a:0:{}s:10:\"\0*\0touches\";a:0:{}s:10:\"timestamps\";b:1;s:9:\"\0*\0hidden\";a:0:{}s:10:\"\0*\0visible\";a:0:{}s:11:\"\0*\0fillable\";a:0:{}s:10:\"\0*\0guarded\";a:1:{i:0;s:1:\"*\";}}}s:24:\"variantid_PricePlusCount\";a:1:{i:0;d:46;}s:13:\"shippingprice\";d:4;s:5:\"total\";d:46;s:8:\"subtotal\";d:50;s:15:\"actual_subtotal\";d:50;}i:1;a:11:{s:7:\"storeid\";i:534211849;s:6:\"userid\";s:9:\"843480070\";s:3:\"ids\";a:1:{i:0;s:1:\"3\";}s:12:\"variantid_ID\";a:1:{i:0;s:1:\"2\";}s:15:\"variantid_Count\";a:1:{i:0;s:1:\"1\";}s:17:\"variantid_Details\";a:1:{i:0;O:24:\"App\\VariantProductMaster\":27:{s:13:\"\0*\0connection\";s:5:\"mysql\";s:8:\"\0*\0table\";s:23:\"variant_product_masters\";s:13:\"\0*\0primaryKey\";s:2:\"id\";s:10:\"\0*\0keyType\";s:3:\"int\";s:12:\"incrementing\";b:1;s:7:\"\0*\0with\";a:0:{}s:12:\"\0*\0withCount\";a:0:{}s:10:\"\0*\0perPage\";i:15;s:6:\"exists\";b:1;s:18:\"wasRecentlyCreated\";b:0;s:13:\"\0*\0attributes\";a:21:{s:2:\"id\";i:2;s:7:\"storeid\";i:534211849;s:10:\"merchantid\";i:742239204;s:9:\"productid\";i:800097549;s:9:\"variantid\";i:1075046348;s:4:\"name\";s:12:\"Ayam Kampung\";s:5:\"photo\";s:12:\"40708092.png\";s:14:\"availablestock\";i:30;s:19:\"variantactivestatus\";i:1;s:12:\"sellingprice\";d:36;s:13:\"shippingprice\";d:4;s:7:\"company\";d:3.96;s:4:\"ipay\";d:0;s:6:\"profit\";d:32.04;s:10:\"topsellind\";i:1;s:12:\"activestatus\";s:1:\"1\";s:9:\"createdby\";s:1:\"2\";s:9:\"updatedby\";N;s:11:\"updateddate\";N;s:10:\"created_at\";s:19:\"2021-03-10 08:50:12\";s:10:\"updated_at\";s:19:\"2021-03-25 09:38:51\";}s:11:\"\0*\0original\";a:21:{s:2:\"id\";i:2;s:7:\"storeid\";i:534211849;s:10:\"merchantid\";i:742239204;s:9:\"productid\";i:800097549;s:9:\"variantid\";i:1075046348;s:4:\"name\";s:12:\"Ayam Kampung\";s:5:\"photo\";s:12:\"40708092.png\";s:14:\"availablestock\";i:30;s:19:\"variantactivestatus\";i:1;s:12:\"sellingprice\";d:36;s:13:\"shippingprice\";d:4;s:7:\"company\";d:3.96;s:4:\"ipay\";d:0;s:6:\"profit\";d:32.04;s:10:\"topsellind\";i:1;s:12:\"activestatus\";s:1:\"1\";s:9:\"createdby\";s:1:\"2\";s:9:\"updatedby\";N;s:11:\"updateddate\";N;s:10:\"created_at\";s:19:\"2021-03-10 08:50:12\";s:10:\"updated_at\";s:19:\"2021-03-25 09:38:51\";}s:10:\"\0*\0changes\";a:0:{}s:8:\"\0*\0casts\";a:0:{}s:17:\"\0*\0classCastCache\";a:0:{}s:8:\"\0*\0dates\";a:0:{}s:13:\"\0*\0dateFormat\";N;s:10:\"\0*\0appends\";a:0:{}s:19:\"\0*\0dispatchesEvents\";a:0:{}s:14:\"\0*\0observables\";a:0:{}s:12:\"\0*\0relations\";a:0:{}s:10:\"\0*\0touches\";a:0:{}s:10:\"timestamps\";b:1;s:9:\"\0*\0hidden\";a:0:{}s:10:\"\0*\0visible\";a:0:{}s:11:\"\0*\0fillable\";a:0:{}s:10:\"\0*\0guarded\";a:1:{i:0;s:1:\"*\";}}}s:24:\"variantid_PricePlusCount\";a:1:{i:0;d:36;}s:13:\"shippingprice\";d:4;s:5:\"total\";d:36;s:8:\"subtotal\";d:40;s:15:\"actual_subtotal\";d:40;}}s:7:\"voucher\";a:2:{s:15:\"voucher_details\";a:1:{i:0;O:16:\"App\\VoucherEntry\":27:{s:13:\"\0*\0connection\";s:5:\"mysql\";s:8:\"\0*\0table\";s:15:\"voucher_entries\";s:13:\"\0*\0primaryKey\";s:2:\"id\";s:10:\"\0*\0keyType\";s:3:\"int\";s:12:\"incrementing\";b:1;s:7:\"\0*\0with\";a:0:{}s:12:\"\0*\0withCount\";a:0:{}s:10:\"\0*\0perPage\";i:15;s:6:\"exists\";b:1;s:18:\"wasRecentlyCreated\";b:0;s:13:\"\0*\0attributes\";a:10:{s:2:\"id\";i:21;s:9:\"voucherid\";s:10:\"1138405672\";s:7:\"orderid\";s:10:\"1245137756\";s:6:\"userid\";s:9:\"843480070\";s:12:\"activestatus\";s:1:\"1\";s:9:\"createdby\";s:1:\"6\";s:9:\"updatedby\";N;s:11:\"updateddate\";N;s:10:\"created_at\";s:19:\"2021-04-01 12:21:46\";s:10:\"updated_at\";s:19:\"2021-04-01 12:21:46\";}s:11:\"\0*\0original\";a:10:{s:2:\"id\";i:21;s:9:\"voucherid\";s:10:\"1138405672\";s:7:\"orderid\";s:10:\"1245137756\";s:6:\"userid\";s:9:\"843480070\";s:12:\"activestatus\";s:1:\"1\";s:9:\"createdby\";s:1:\"6\";s:9:\"updatedby\";N;s:11:\"updateddate\";N;s:10:\"created_at\";s:19:\"2021-04-01 12:21:46\";s:10:\"updated_at\";s:19:\"2021-04-01 12:21:46\";}s:10:\"\0*\0changes\";a:0:{}s:8:\"\0*\0casts\";a:0:{}s:17:\"\0*\0classCastCache\";a:0:{}s:8:\"\0*\0dates\";a:0:{}s:13:\"\0*\0dateFormat\";N;s:10:\"\0*\0appends\";a:0:{}s:19:\"\0*\0dispatchesEvents\";a:0:{}s:14:\"\0*\0observables\";a:0:{}s:12:\"\0*\0relations\";a:0:{}s:10:\"\0*\0touches\";a:0:{}s:10:\"timestamps\";b:1;s:9:\"\0*\0hidden\";a:0:{}s:10:\"\0*\0visible\";a:0:{}s:11:\"\0*\0fillable\";a:0:{}s:10:\"\0*\0guarded\";a:1:{i:0;s:1:\"*\";}}}s:14:\"voucher_amount\";d:10;}s:8:\"subtotal\";d:80;}', NULL, '1', '6', NULL, NULL, '2021-04-01 04:29:39', '2021-04-01 04:29:39'),
(2, 843480070, '183173033', NULL, '25', '1', '0133052350', '5', 'a:2:{s:7:\"voucher\";a:1:{s:15:\"voucher_details\";a:1:{i:0;N;}}s:8:\"subtotal\";i:0;}', NULL, '1', '6', NULL, NULL, '2021-04-01 04:42:41', '2021-04-01 04:42:41'),
(3, 1916653428, '2003580666', NULL, '329', '1', '12345666', '6', 'a:3:{s:7:\"voucher\";a:1:{s:15:\"voucher_details\";a:1:{i:0;N;}}s:8:\"subtotal\";i:0;s:15:\"actual_subtotal\";i:0;}', NULL, '1', '3', NULL, NULL, '2021-04-19 06:49:29', '2021-04-19 06:49:29'),
(4, 1916653428, '464674032', NULL, '25', '1', '12345666', '6', 'a:3:{s:7:\"voucher\";a:1:{s:15:\"voucher_details\";a:1:{i:0;N;}}s:8:\"subtotal\";i:0;s:15:\"actual_subtotal\";i:0;}', NULL, '1', '3', NULL, NULL, '2021-04-19 06:49:59', '2021-04-19 06:49:59'),
(5, 1916653428, '729531525', NULL, '40', '1', '12345666', '6', 'a:3:{s:7:\"voucher\";a:1:{s:15:\"voucher_details\";a:1:{i:0;N;}}s:8:\"subtotal\";i:0;s:15:\"actual_subtotal\";i:0;}', NULL, '1', '3', NULL, NULL, '2021-04-19 06:50:35', '2021-04-19 06:50:35'),
(6, 1916653428, '729531525', NULL, '40', '1', '12345666', '6', 'a:3:{s:7:\"voucher\";a:1:{s:15:\"voucher_details\";a:1:{i:0;N;}}s:8:\"subtotal\";i:0;s:15:\"actual_subtotal\";i:0;}', NULL, '1', '3', NULL, NULL, '2021-04-19 06:51:43', '2021-04-19 06:51:43'),
(7, 1916653428, '50748065', NULL, '40', '1', '12345666', '6', 'a:3:{s:7:\"voucher\";a:1:{s:15:\"voucher_details\";a:1:{i:0;N;}}s:8:\"subtotal\";i:0;s:15:\"actual_subtotal\";i:0;}', NULL, '1', '3', NULL, NULL, '2021-04-19 07:07:09', '2021-04-19 07:07:09');

-- --------------------------------------------------------

--
-- Table structure for table `product_store_masters`
--

DROP TABLE IF EXISTS `product_store_masters`;
CREATE TABLE IF NOT EXISTS `product_store_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storeid` int(11) NOT NULL,
  `merchantid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `subcategory` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliveryperiod` double NOT NULL,
  `unitmeasurement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo5` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `topsellind` int(11) DEFAULT NULL,
  `readytocookind` int(11) DEFAULT NULL,
  `packageind` int(11) DEFAULT NULL,
  `variantid` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_store_masters`
--

INSERT INTO `product_store_masters` (`id`, `storeid`, `merchantid`, `productid`, `name`, `category`, `subcategory`, `description`, `deliveryperiod`, `unitmeasurement`, `photo1`, `photo2`, `photo3`, `photo4`, `photo5`, `youtube`, `rating`, `topsellind`, `readytocookind`, `packageind`, `variantid`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, 534211849, 742239204, 800097549, 'AyamAyamAyamAyamAyamAyamAyamAyamAyam', 1, 2, 'Fresh Only', 2, '1', '1470782760.png', '', '', '', '', 'www.youtube.com', 2, NULL, NULL, NULL, '1075046348', '1', '2', '2', '2021-04-12 16:47:09', '2021-03-10 00:45:02', '2021-04-12 08:47:09'),
(2, 534211849, 742239204, 1795879308, 'Kambing', 3, 1, 'Fresh', 2, '1', '368181733.jpg', '', '', '', '', 'youtube', NULL, NULL, NULL, NULL, '1119887063', '1', '2', '2', '2021-03-15 03:47:43', '2021-03-10 01:32:14', '2021-03-14 19:47:43'),
(3, 11489457, 1058152664, 420264236, 'Fish', 2, 1, '1', 1, '1', '1257057588.jpg', '', '', '', '', '1', NULL, NULL, NULL, NULL, '1381582827', '1', '4', NULL, NULL, '2021-03-11 19:53:27', '2021-03-11 19:54:27'),
(4, 11489457, 1058152664, 622358871, 'Goats', 3, 4, 'Nice to good like very much as good', 3, '1', '827093733.png', '', '', '', '', 'test', NULL, NULL, NULL, NULL, '1807205330', '1', '4', NULL, NULL, '2021-03-30 11:16:39', '2021-03-30 22:15:08');

-- --------------------------------------------------------

--
-- Table structure for table `product_unit_measurement_masters`
--

DROP TABLE IF EXISTS `product_unit_measurement_masters`;
CREATE TABLE IF NOT EXISTS `product_unit_measurement_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activestatus` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_unit_measurement_masters`
--

INSERT INTO `product_unit_measurement_masters` (`id`, `name`, `activestatus`, `created_at`, `updated_at`) VALUES
(1, 'KG', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `review_product_models`
--

DROP TABLE IF EXISTS `review_product_models`;
CREATE TABLE IF NOT EXISTS `review_product_models` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `productid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `variantid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `variantid_ID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `star` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `replyto_reviewid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `review_product_models`
--

INSERT INTO `review_product_models` (`id`, `productid`, `variantid`, `variantid_ID`, `star`, `image1`, `image2`, `image3`, `review`, `replyto_reviewid`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, '123', '123', '123', '2', '1790147027.png', '', '', 'das', '12', '1', '1', '1', '2021-04-24 12:19:04', '2021-04-24 04:15:47', '2021-04-24 04:19:04');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_masters`
--

DROP TABLE IF EXISTS `shipping_masters`;
CREATE TABLE IF NOT EXISTS `shipping_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_masters`
--

INSERT INTO `shipping_masters` (`id`, `userid`, `address1`, `address2`, `postcode`, `city`, `state`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(3, '3', 'lot 17 jalan morib', 'das', '42311', '9', '1', '0', '3', '3', '2021-04-07 16:07:13', '2021-03-16 01:37:23', '2021-04-07 23:08:34'),
(4, '5', 'lot 17 jalan morib', '1', '4121', '12', '11', '1', '5', NULL, NULL, '2021-03-25 01:53:01', '2021-03-25 01:53:01'),
(5, '6', 'lot 17 jalan morib', 'uy', '12322', '12', '22', '1', '6', NULL, NULL, '2021-04-01 03:01:46', '2021-04-01 03:01:46'),
(6, '3', '43e', 'dsdd', '5555', '12', '9', '1', '3', '3', '2021-04-07 16:29:03', '2021-04-07 23:17:48', '2021-04-07 23:29:03');

-- --------------------------------------------------------

--
-- Table structure for table `slider_masters`
--

DROP TABLE IF EXISTS `slider_masters`;
CREATE TABLE IF NOT EXISTS `slider_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productid` longtext COLLATE utf8mb4_unicode_ci,
  `startdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adds` int(11) DEFAULT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_masters`
--

INSERT INTO `slider_masters` (`id`, `image`, `name`, `url`, `productid`, `startdate`, `enddate`, `adds`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, '1079835863.png', 'gayu', 'https://softwareprojectplus.com/lailai/', NULL, '2021-04-18', '2021-04-22', 0, '1', NULL, '1', '2021-04-23 22:44:45', '2021-04-22 06:25:29', '2021-04-23 14:44:45');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category_masters`
--

DROP TABLE IF EXISTS `sub_category_masters`;
CREATE TABLE IF NOT EXISTS `sub_category_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `index` int(11) NOT NULL,
  `activestatus` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_category_masters`
--

INSERT INTO `sub_category_masters` (`id`, `category`, `name`, `image`, `index`, `activestatus`, `created_at`, `updated_at`) VALUES
(1, 1, 'Kampung', 'http://127.0.0.1:8000/files/1341864161.jpg', 1, 1, NULL, '2021-04-20 03:18:02'),
(2, 1, 'Daging', 'http://localhost/images/ayamdaging.png', 2, 1, NULL, NULL),
(3, 1, 'Telur', 'http://127.0.0.1:8000/files/', 3, 1, NULL, '2021-04-20 03:30:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `merchant` int(11) NOT NULL DEFAULT '0',
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otvkey` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobilekey` longtext COLLATE utf8mb4_unicode_ci,
  `mobileregisterind` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userid`, `photo`, `name`, `phone`, `email`, `email_verified_at`, `password`, `role`, `merchant`, `activestatus`, `type`, `otvkey`, `mobilekey`, `mobileregisterind`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '1647131362', '', 'Tharmaraj', '0133052350', 'tharmaraj123@yahoo.com', NULL, '$2y$10$Q1kC.9P0YQ.sL9shF/6OtO8aP2BTyPSzVkH74yZcMHqOe5X6lQAXm', '1', 0, '1', '1', NULL, NULL, NULL, NULL, NULL, '2021-03-24 23:45:15'),
(2, '579594918', '', 'Tharmaraj', '0133052350', 'softwareprojectplus@gmail.com', NULL, '$2y$10$hMOnv0qh0AOdTqvrFcWLLuXkKam7WOuO.JSpLVfDhoedEZDq4it5S', '3', 1, '1', '1', NULL, NULL, NULL, NULL, NULL, '2021-03-24 23:45:58'),
(3, '1916653428', '487551233.jpg', 'Tharmaraj', '12345666', 'rajinfomina@gmail.com', NULL, '$2y$10$NK0CRn8srvaSI5dAT1E9XOE8WhjQ2oh3jqKyaeQwkDcUjlQr5GrbO', '2', 0, '1', '1', NULL, NULL, NULL, NULL, NULL, '2021-04-05 21:47:19'),
(4, '1327250225', '', 'merchant1', '0133052350', 'merchant@yahoo.com', NULL, '$2y$10$IJALtmUuX6RroeQfU2mft.gUaq8SyuQHvz8V215Zge2XUjVyFT6S6', '3', 1, '1', '1', NULL, NULL, NULL, NULL, '2021-03-11 19:47:40', '2021-03-25 23:15:51'),
(5, '833496386', '', 'MFF', '0133052350', 'mff@gmail.com', NULL, '$2y$10$WRoHLmi/1cm7JMb/bYE8y.9.I0pG14Nmih6KPw2YcKPVxklaTe5g.', '3', 1, '1', '1', NULL, NULL, NULL, NULL, '2021-03-25 01:52:10', '2021-03-25 01:58:04'),
(6, '843480070', '', 'user', '0133052350', 'user@yahoo.com', NULL, '$2y$10$aNTZc40fP75XpPjUPLBrkuBAZgYQGyUSV0Z4qJoCUo1LTDijTaXZi', '2', 0, '1', '1', NULL, NULL, NULL, NULL, '2021-03-31 09:26:20', '2021-04-01 03:09:32'),
(7, '1897130226', '', 'raaja', '', 'rajinfomina1@gmail.com', NULL, '$2y$10$6pGDAvHBas5.ePd0KmjcLOazzzUDw/yZDUgf2uvOiDzpoSF5dbqGW', '2', 0, '1', '2', NULL, NULL, NULL, NULL, '2021-04-02 15:47:07', '2021-04-02 15:47:07'),
(8, '1759173778', '', 'tanesh', '', 'tanesh@yahoo.com', NULL, '$2y$10$MANa2VwS4PgVFD342blq1.Hj.sR38Htc974g.tV4UwegLqcNHNeDK', '2', 0, '1', '2', NULL, NULL, NULL, NULL, '2021-04-02 16:03:56', '2021-04-02 16:03:56'),
(9, '1094068589', NULL, 'qwe', '', 'tharmaraj123123@yahoo.com', NULL, '$2y$10$Sg4wNrfxosDtD3CVryGaMuXLmhfO75axxXmhLrvOO97uS3yAOarhy', '2', 0, '1', '1', NULL, NULL, NULL, NULL, '2021-04-19 01:58:56', '2021-04-19 01:58:56');

-- --------------------------------------------------------

--
-- Table structure for table `variant_item_list_masters`
--

DROP TABLE IF EXISTS `variant_item_list_masters`;
CREATE TABLE IF NOT EXISTS `variant_item_list_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `variantid` int(11) NOT NULL,
  `variant_ID` int(11) NOT NULL,
  `photo1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variant_item_list_masters`
--

INSERT INTO `variant_item_list_masters` (`id`, `variantid`, `variant_ID`, `photo1`, `name`, `count`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, 0, 12, 'http://127.0.0.1:8000/files/231140935.png', 'qwe', '1', '1', NULL, NULL, NULL, '2021-04-21 03:32:39', '2021-04-21 03:35:10'),
(2, 0, 123, 'http://127.0.0.1:8000/files/1296306313.jpg', 'weq', '1', '1', NULL, NULL, NULL, '2021-04-21 03:35:03', '2021-04-21 03:35:03'),
(7, 1075046348, 1, '561343539.jpg', '1', '1', '1', '1', '1', '2021-04-22 14:40:50', '2021-04-22 06:39:44', '2021-04-22 06:40:50');

-- --------------------------------------------------------

--
-- Table structure for table `variant_product_masters`
--

DROP TABLE IF EXISTS `variant_product_masters`;
CREATE TABLE IF NOT EXISTS `variant_product_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storeid` int(11) NOT NULL,
  `merchantid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `variantid` int(11) NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `averageweight` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `availablestock` int(11) NOT NULL,
  `variantactivestatus` int(11) NOT NULL,
  `sellingprice` double NOT NULL,
  `shippingprice` double NOT NULL,
  `company` double NOT NULL,
  `ipay` double NOT NULL,
  `profit` double NOT NULL,
  `beforeprice` double DEFAULT NULL,
  `topsellind` int(11) NOT NULL,
  `whatisinside` longtext COLLATE utf8mb4_unicode_ci,
  `howtocook` longtext COLLATE utf8mb4_unicode_ci,
  `readytocookind` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variant_product_masters`
--

INSERT INTO `variant_product_masters` (`id`, `storeid`, `merchantid`, `productid`, `variantid`, `name`, `description`, `averageweight`, `photo`, `availablestock`, `variantactivestatus`, `sellingprice`, `shippingprice`, `company`, `ipay`, `profit`, `beforeprice`, `topsellind`, `whatisinside`, `howtocook`, `readytocookind`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, 534211849, 742239204, 800097549, 1075046348, 'Ayam Daging Ayam Daging Ayam Daginga AAAAA', 'Best Dooooooooooooooooooooo', '1.8 - 2.0', '539637944.png', 20, 1, 21, 4, 2.31, 0, 18.69, 0, 1, NULL, NULL, NULL, '1', '2', NULL, NULL, '2021-03-10 00:48:19', '2021-04-02 11:32:22'),
(2, 534211849, 742239204, 800097549, 1075046348, 'Ayam Kampung', 'Jet Flying Sky', '2.0 - 1.5', '40708092.png', 30, 1, 36, 4, 3.96, 0, 32.04, 0, 1, NULL, NULL, NULL, '1', '2', NULL, NULL, '2021-03-10 00:50:12', '2021-04-02 11:32:23'),
(3, 534211849, 742239204, 1795879308, 1119887063, 'Kambing Kampung', 'Wsspace teddy bear cool fly', '9.9', '980012272.jpg', 21, 1, 59, 4, 6.49, 0, 52.51, 0, 1, NULL, NULL, NULL, '1', '2', NULL, NULL, '2021-03-10 01:38:44', '2021-04-02 11:32:24'),
(4, 11489457, 1058152664, 420264236, 1381582827, 'Ikan YU', '', '', '1452881825.webp', 21, 1, 23, 4, 2.53, 0, 20.47, 0, 1, NULL, NULL, NULL, '1', '4', NULL, NULL, '2021-03-11 19:53:52', '2021-04-07 00:41:03'),
(5, 11489457, 1058152664, 622358871, 1807205330, 'Variant One Goat', '', '', '462001570.png', 20, 1, 27, 2, 2.97, 0, 24.03, 0, 0, NULL, NULL, NULL, '0', '4', NULL, NULL, '2021-03-30 11:27:06', '2021-04-07 00:41:04');

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

DROP TABLE IF EXISTS `vouchers`;
CREATE TABLE IF NOT EXISTS `vouchers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `voucherid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `startdatetime` datetime NOT NULL,
  `enddatetime` datetime NOT NULL,
  `voucherpricetype` int(11) NOT NULL,
  `value` double NOT NULL,
  `user` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minimumpurchase` double NOT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `voucherid`, `name`, `startdatetime`, `enddatetime`, `voucherpricetype`, `value`, `user`, `minimumpurchase`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, '1138405672', 'mff stall', '2021-03-30 17:14:00', '2021-04-08 17:14:00', 2, 10, '*', 121, '1', '1', '1', '2021-03-31 18:30:06', '2021-03-10 01:15:02', '2021-03-31 10:30:06'),
(2, '1351802384', 'Person Only', '2021-03-22 00:00:00', '2021-03-31 19:00:00', 2, 15, '1916653428', 30, '1', '1', '1', '2021-03-31 18:29:39', '2021-03-23 16:00:00', '2021-03-31 10:29:39'),
(3, '1442294833', 'makan makan', '2021-04-14 11:20:00', '2021-04-15 11:31:00', 1, 50, '*', 60, '1', '1', '1', '2021-04-14 11:32:40', '2021-04-14 03:31:35', '2021-04-14 03:32:40');

-- --------------------------------------------------------

--
-- Table structure for table `voucher_entries`
--

DROP TABLE IF EXISTS `voucher_entries`;
CREATE TABLE IF NOT EXISTS `voucher_entries` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `voucherid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activestatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updatedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updateddate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voucher_entries`
--

INSERT INTO `voucher_entries` (`id`, `voucherid`, `orderid`, `userid`, `activestatus`, `createdby`, `updatedby`, `updateddate`, `created_at`, `updated_at`) VALUES
(1, '1351802384', '1723614736', '1916653428', '2', '3', NULL, NULL, '2021-03-31 08:33:41', '2021-04-19 09:54:02'),
(2, '1351802384', '1723614736', '1916653428', '2', '3', NULL, NULL, '2021-03-31 09:53:18', '2021-04-19 09:54:02'),
(3, '1351802384', '1723614736', '1916653428', '2', '3', NULL, NULL, '2021-03-31 09:56:05', '2021-04-19 09:54:02'),
(4, '1351802384', '1723614736', '1916653428', '2', '3', NULL, NULL, '2021-03-31 10:00:20', '2021-04-19 09:54:02'),
(5, '1351802384', '1385625323', '1916653428', '2', '3', NULL, NULL, '2021-03-31 10:09:36', '2021-04-19 09:54:02'),
(6, '1138405672', '681545065', '843480070', '2', '6', NULL, NULL, '2021-04-01 03:01:32', '2021-04-01 07:19:44'),
(7, '1138405672', '1583720369', '843480070', '2', '6', NULL, NULL, '2021-04-01 03:01:49', '2021-04-01 07:19:44'),
(8, '1138405672', '1919881532', '843480070', '2', '6', NULL, NULL, '2021-04-01 03:04:46', '2021-04-01 07:19:44'),
(9, '1138405672', '1511574054', '843480070', '2', '6', NULL, NULL, '2021-04-01 03:06:26', '2021-04-01 07:19:44'),
(10, '1138405672', '1034562141', '843480070', '2', '6', NULL, NULL, '2021-04-01 03:14:24', '2021-04-01 07:19:44'),
(11, '1138405672', '812410778', '843480070', '2', '6', NULL, NULL, '2021-04-01 03:20:07', '2021-04-01 07:19:44'),
(12, '1138405672', '1797118166', '843480070', '2', '6', NULL, NULL, '2021-04-01 03:40:27', '2021-04-01 07:19:44'),
(13, '1138405672', '1489151967', '843480070', '2', '6', NULL, NULL, '2021-04-01 03:44:18', '2021-04-01 07:19:44'),
(14, '1138405672', '638468606', '843480070', '2', '6', NULL, NULL, '2021-04-01 03:45:13', '2021-04-01 07:19:44'),
(15, '1138405672', '1981421629', '843480070', '2', '6', NULL, NULL, '2021-04-01 04:04:45', '2021-04-01 07:19:44'),
(16, '1138405672', '578163251', '843480070', '2', '6', NULL, NULL, '2021-04-01 04:05:47', '2021-04-01 07:19:44'),
(17, '1138405672', '1228738819', '843480070', '2', '6', NULL, NULL, '2021-04-01 04:07:36', '2021-04-01 07:19:44'),
(18, '1138405672', '1088428019', '843480070', '2', '6', NULL, NULL, '2021-04-01 04:08:59', '2021-04-01 07:19:44'),
(19, '1138405672', '1488574399', '843480070', '2', '6', NULL, NULL, '2021-04-01 04:11:06', '2021-04-01 07:19:44'),
(20, '1138405672', '191282410', '843480070', '2', '6', NULL, NULL, '2021-04-01 04:18:14', '2021-04-01 07:19:44'),
(21, '1138405672', '1245137756', '843480070', '2', '6', NULL, NULL, '2021-04-01 04:21:46', '2021-04-01 07:19:44'),
(22, '1138405672', '1601762416', '843480070', '2', '6', NULL, NULL, '2021-04-01 06:28:39', '2021-04-01 07:19:44'),
(23, '1138405672', '1340281159', '843480070', '2', '6', NULL, NULL, '2021-04-01 06:29:01', '2021-04-01 07:19:44'),
(24, '1138405672', '990591241', '843480070', '2', '6', NULL, NULL, '2021-04-01 06:29:27', '2021-04-01 07:19:44'),
(25, '1138405672', '48370752', '843480070', '2', '6', NULL, NULL, '2021-04-01 06:31:19', '2021-04-01 07:19:44'),
(26, '11384056721', '432237140', '843480070', '2', '6', NULL, NULL, '2021-04-01 06:31:30', '2021-04-01 07:19:44'),
(27, '1138405672', '2064495490', '843480070', '2', '6', NULL, NULL, '2021-04-01 06:37:09', '2021-04-01 07:19:44'),
(28, '1138405672', '1097487467', '843480070', '2', '6', NULL, NULL, '2021-04-01 06:45:56', '2021-04-01 07:19:44'),
(29, '1138405672', '1065411637', '843480070', '2', '6', NULL, NULL, '2021-04-01 06:47:09', '2021-04-01 07:19:44'),
(30, '1138405672', '1936162546', '843480070', '2', '6', NULL, NULL, '2021-04-01 06:52:26', '2021-04-01 07:19:44'),
(31, '1138405672', '1526848922', '843480070', '2', '6', NULL, NULL, '2021-04-01 07:15:27', '2021-04-01 07:19:44'),
(32, '1138405672', '1791946749', '843480070', '1', '6', NULL, NULL, '2021-04-01 07:19:47', '2021-04-01 07:19:47'),
(33, '1442294833', '1008806571', '1916653428', '2', '3', NULL, NULL, '2021-04-14 03:31:43', '2021-04-19 09:54:02'),
(34, '1442294833', '20387768', '1916653428', '2', '3', NULL, NULL, '2021-04-14 03:32:30', '2021-04-19 09:54:02'),
(35, '1442294833', '1957389307', '1916653428', '2', '3', NULL, NULL, '2021-04-14 03:33:59', '2021-04-19 09:54:02'),
(36, '1442294833', '1300171947', '1916653428', '2', '3', NULL, NULL, '2021-04-14 07:27:49', '2021-04-19 09:54:02');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
