@extends('layouts.app')

@section('content')
<div class="container">
Bargain
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>From </td>
        <td>
        {{$result->users->name}}
        <br>
        {{$result->users->phone}}
        <br>
        {{$result->users->email}}
        <br>
        </td>
        @if($result->merchantbargainind != 2)
        <td><a href="{{ route('bargainapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('bargainreject', $result->id ) }}">Reject</a></td>
        @endif
        <td><img width=50 src="<?=geturlfile($result->variant->photo)?>"></td>
        <td>Selling Price : {{$result->originalamount}}</td>
        <td>Bargain Price : {{$result->bargainamount}}</td>
        <td>{{$result->variant->name}}</td>
        <td>{{$result->customer_message}}</td>


        

        <td>{{$result->customerbargainind}}</td>
        <td>{{$result->merchantbargainind}}</td>
        <!-- <td>{{$result}}</td> -->
        <td>
        <form action="{{ route('bargainupdatemerchantmessage', $result->id) }}" method="POST">
        @csrf
        <input type="text" name="merchant_message" placeholder="merchant_message" value="{{$result->merchant_message}}">
        <input type="submit" value="Send" >
        </form>
        
        </td>
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection