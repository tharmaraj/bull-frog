@extends('layouts.app')

@section('content')
<div class="container">
Edit
<form method="post" action="{{ route('profileupdate',$results->id) }}" enctype="multipart/form-data">
@csrf
<input type="file" name="photo" value="" >

<input type="text" name="name" value="{{$results->name}}">
<input type="number" name="phone" value="{{$results->phone}}" placeholder="Phone Number">
<input type="password" name="password" value="" placeholder="New Password">
<input type="submit" value="Update">
</form>
</div>

@endsection