@extends('layouts.app')

@section('content')
<div class="container">
View Profile
<table class="table table-border">
    @foreach($results as $result)
        <td><img width=50 src="<?=geturlfile($result->photo)?>"></td>
        <td>{{$result->userid}}</td>
        <td>{{$result->name}}</td>
        <td>{{$result->email}}</td>
        <td>{{$result->merchant}}</td>
        <td>{{$result}}</td>
        <td><a href="{{ route('profileedit', $result->id ) }}">Update Here</a></td>
    @endforeach
</table>
</div>

@endsection