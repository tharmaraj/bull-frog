@extends('layouts.app')

@section('content')
<div class="container">
Variant Item List <a href="{{route('variantitemlistcreate')}}">Create New</a>
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td><img src='{{$result->photo1}}' width='50'></td>
        <td>{{$result}}</td>
        <td><a href="{{ route('variantitemlistedit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('variantitemlistdelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection