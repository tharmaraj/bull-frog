@extends('layouts.app')

@section('content')
<div class="container">
Payment
<br>


<table class="table table-border">
        <tr><td>Total</td><td>{{$paymentDetails['sum_total']}}</td></tr>
        @if($paymentDetails['voucher_amount'])
        <tr><td>Voucher</td><td>- {{$paymentDetails['voucher_amount']}}</td></tr>
        @endif
        <tr><td>Total Shipping</td><td>{{$paymentDetails['sum_totalshipping']}}</td></tr>
        <tr><td>Sub Total</td><td>{{$response['subtotal']}}</td></tr>
    </table>



@if(isset($shippingaddress->id))
<form method="post" action="{{ route('paymentsettlestore') }}" enctype="multipart/form-data">
@csrf
<input type="text" name="userid" value="{{ $results[0]['userid'] }}" placeholder="userid">
<input type="text" name="orderid" value="{{ $results[0]['orderid'] }}" placeholder="orderid">

@if($response['voucher']['voucher_details'][0] != null)

<input type="text" name="voucherid" value="{{ $response['voucher']['voucher_details'][0]['voucherid'] }}" placeholder="voucherid">
@endif
<input type="text" name="totalvalue" value="{{ $paymentDetails['sum_subtotal'] }}" placeholder="totalvalue">
<input type="text" name="phone" value="{{Auth::user()->phone}}" placeholder="Phone" readonly>
<input type="text" name="shippingaddressid" value="{{ $shippingaddress->id }}" placeholder="Shipping Address ID">

<input type="file" name="receipt_copy" value="" placeholder="Receipt Copy">

@if(Auth::user()->phone)
<input type="submit" onclick="return confirm('Are you sure?')" value="Final Submit">
@else
<div class="alert alert-danger">
Please Update Your Profile <a href="{{route('profileview')}}" class="text-danger">Update Profile</a>
</div>
@endif

</form>
@else
{{__('No Shipping Address')}} 

<div class="alert alert-danger">
Please create address for Next Step <a href="{{route('shippingmastercreate')}}" class="text-danger">Create New</a>
</div>

@endif


<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result}}</td>
        <td><a href="{{ route('paymentproductdelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}} 
    @endforelse
</table>


</div>

@endsection