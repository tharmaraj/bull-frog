@extends('layouts.app')

@section('content')
<div class="container">
View
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result}}</td>
        <td><a href="{{ route('merchantstoreedit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('merchantstoredelete', $result->id ) }}">Delete</a></td>
        @if(Auth::user()->role == '1')
        <td><a href="{{ route('merchantstoreapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('merchantstorereject', $result->id ) }}">Reject</a></td>
        @endif
    </tr>
    @empty
        {{__('No Record')}} <a href="{{route('merchantstorecreate')}}">Apply Now</a>
    @endforelse
</table>


</div>

@endsection