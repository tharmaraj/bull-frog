@extends('layouts.app')

@section('content')
<div class="container">
Create
<form method="post" action="{{ route('merchantstoreupdate',$results->id) }}" enctype="multipart/form-data">
@csrf

<input type="file" name="storeimage" value="" >
<a href="../files/{{$results->storeimage}}">{{$results->storeimage}}</a>
<input type="file" name="storelogo" value="" >
<a href="../files/{{$results->storelogo}}">{{$results->storelogo}}</a>

<input type="text" name="fullstorename" value="{{$results->fullstorename}}" placeholder="Full Store Name">
<input type="text" name="nickstorename" value="{{$results->nickstorename}}" placeholder="Nicname of Store">
<input type="text" name="storephone" value="{{$results->storephone}}" placeholder="Store Phone Number">
<input type="text" name="address1" value="{{$results->address1}}" placeholder="Address 1">
<input type="text" name="address2" value="{{$results->address2}}" placeholder="Address 2">
<input type="text" name="poscood" value="{{$results->poscood}}" placeholder="poscod">
<input type="text" name="city" value="{{$results->city}}" placeholder="city">
<!-- <input type="text" name="state" value="{{$results->state}}" placeholder="state"> -->

{!! stateList($results->state) !!}

<input type="time" name="openingtime" value="{{$results->openingtime}}" placeholder="openingtime">
<input type="time" name="closingtime" value="{{$results->closingtime}}" placeholder="closingtime">
<input type="text" name="holiday" value="{{$results->holiday}}" placeholder="holiday">

<input type="text" name="instagram" value="{{$results->instagram}}" placeholder="instagram">
<input type="text" name="facebook" value="{{$results->facebook}}" placeholder="facebook">
<input type="text" name="youtube" value="{{$results->youtube}}" placeholder="youtube">
<input type="text" name="website" value="{{$results->website}}" placeholder="website">
<input type="text" name="location" value="{{$results->location}}" placeholder="location">

<input type="number" name="cod" value="{{$results->cod}}" placeholder="cod">


<input type="text" name="defaultshippingprice" value="{{$results->defaultshippingprice}}" placeholder="defaultshippingprice">



<input type="submit" value="Update">
</form>
</div>

@endsection