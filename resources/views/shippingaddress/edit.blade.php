@extends('layouts.app')

@section('content')
<div class="container">
Edit
<form method="post" action="{{ route('shippingmasterupdate',$results->id) }}" enctype="multipart/form-data">
@csrf

<input type="text" name="address1" value="{{$results->address1}}" placeholder="add">
<input type="text" name="address2" value="{{$results->address2}}" placeholder="add">
<input type="text" name="postcode" value="{{$results->postcode}}" placeholder="postcode">
<!-- <input type="text" name="city" value="{{$results->city}}" placeholder="city"> -->
<!-- <input type="text" name="state" value="{{$results->state}}" placeholder="state"> -->
{!! cityList($results->city) !!}
{!! stateList($results->state) !!}


<input type="submit" value="Update">
</form>
</div>

@endsection