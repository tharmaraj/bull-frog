@extends('layouts.app')

@section('content')
<div class="container">
Shipping Address <a href="{{route('shippingmastercreate')}}">Create New</a>
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result}}</td>
        <td><a href="{{ route('shippingmasteredit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('shippingmasterapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('shippingmasterreject', $result->id ) }}">Reject</a></td>
        <td><a href="{{ route('shippingmasterdelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}} 
    @endforelse
</table>


</div>

@endsection