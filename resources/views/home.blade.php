@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!

                    @if(Auth::User()->role == '1')
                    Admin
                    <br><a href="{{route('profileview')}}">Profile</a>
                    <br><a href="{{route('merchantview')}}">Merchant List</a>
                    <br><a href="{{route('merchantstoreview')}}">Store List</a>
                    <br><a href="{{route('productstoreview')}}">Product List</a>
                    <br><a href="{{route('variantproductview')}}">Variant List</a>
                    <br><a href="{{route('historymasterview')}}">History</a>
                    <br><a href="{{route('voucherview')}}">Voucher</a>
                    <br><a href="{{route('voucherentryview')}}">Voucher Entry</a>
                    <br><a href="{{route('memberview')}}">Users</a>
                    <br><a href="{{route('categoryview')}}">Category</a>
                    <br><a href="{{route('subcategoryview')}}">Sub Category</a>
                    <br><a href="{{route('sliderview')}}">Slider Master</a>
                    <br><a href="{{route('reviewproductview')}}">Reviews</a>
                    <br><a href="#">Dashboard</a>
                    @endif
                    @if(Auth::User()->role == '2')
                    Normal User
                    <br><a href="{{route('dashboardview',Auth::id())}}">Dashboard</a>
                    <br><a href="{{route('profileview')}}">Profile</a>
                    <br><a href="{{route('shippingmasterview')}}">Shipping Address</a>
                    <br><a href="{{route('merchantview')}}">Become Merchant</a>
                    <br><a href="{{route('publicproductview')}}">Products</a>
                    <br>
                    <form action="{{ route('productsapiweb') }}" method="POST">
                    @csrf
                    <input type="hidden" name="group" value="4">
                    <input type="submit" value="Morning Market" >
                    </form>
                    
                    <br><a href="{{route('cartproductview')}}">Cart</a>
                    <br><a href="{{route('historymasterview')}}">History</a>
                    <br><a href="#">Dashboard</a>
                    @endif
                    @if(Auth::User()->role == '3')
                    Merchant
                    <br><a href="{{route('profileview')}}">Profile</a>
                    <br><a href="{{route('bargainview')}}">Bargain</a>
                    <br><a href="{{route('merchantview')}}">Become Merchant</a>
                    @if(Auth::user()->merchant == 1)
                    <br><a href="{{route('merchantstoreview')}}">Merchant Store</a>
                        @if($merchantstorecheck->count() > 0)
                        <br><a href="{{route('productstoreview')}}">My Product</a>
                        <br><a href="{{route('variantproductview')}}">Variant</a>
                        <br><a href="{{route('variantitemlistview')}}">Item In Variant</a>
                        <br><a href="{{route('morningmarketview')}}">Morning Market</a>
                        @endif
                    @endif
                    <br><a href="{{route('historymerchantmasterview')}}">History</a>
                    <br><a href="{{route('historymerchantmasterviewdelivered')}}">History Delivered</a>
                    <br><a href="{{route('cartproductview')}}">Cart</a>
                    <br><a href="#">Dashboard</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
