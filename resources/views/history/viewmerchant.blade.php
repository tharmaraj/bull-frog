@extends('layouts.app')

@section('content')
<div class="container">
View
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result->ordershippingstatus}}</td>
        <td>{{$result->orderpaymentstatus}}</td>
        <td><a href="{{ route('historymerchantshippingupdate', [$result->id.'~1',Auth::id(),0] ) }}">Packing</a></td>
        <td><a href="{{ route('historymerchantshippingupdate', [$result->id.'~2',Auth::id(),0] ) }}">Out for Delivery</a></td>
        <td><a href="{{ route('historymerchantshippingupdate', [$result->id.'~3',Auth::id(),0] ) }}">Delivered</a></td>
        <td><a href="{{ route('historymerchantpaymentupdate', [$result->id.'~1',Auth::id(),0] ) }}">Pending</a></td>
        <td><a href="{{ route('historymerchantpaymentupdate', [$result->id.'~2',Auth::id(),0] ) }}">Payed</a></td>
        <td><a href="{{ route('historymerchantpaymentupdate', [$result->id.'~3',Auth::id(),0] ) }}">Failed</a></td>
        <td>{{$result}}</td>

        @if(Auth::user()->role == '1')
        @endif
    </tr>
    @empty
        {{__('No Record')}} <a href="{{route('merchantstorecreate')}}">Apply Now</a>
    @endforelse
</table>


</div>

@endsection