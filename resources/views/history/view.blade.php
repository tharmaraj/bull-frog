@extends('layouts.app')

@section('content')
<div class="container">
View
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result->ordershippingstatus}}</td>
        <td>{{$result->orderpaymentstatus}}</td>
        <td>{{$result}}</td>
        @if(Auth::user()->role == '1')
        @endif
    </tr>
    @if($result->ordershippingstatus == 1)
    <tr>
        <td colspan="3">
            <form method="post" action="{{ route('reviewproductstore') }}" enctype="multipart/form-data">
            @csrf

            <input type="text" readonly name="productid" value="{{$result->productid}}" placeholder="productid">
            <input type="text" readonly name="variantid" value="{{$result->variantid}}" placeholder="variantid">
            <input type="text" readonly name="variantid_ID" value="{{$result->variantid_ID}}" placeholder="variantid_ID">
            <input type="text" readonly name="orderid" value="{{$result->orderid}}" placeholder="orderid">
            <input type="text" name="star" value="" placeholder="star">
            <input type="file" name="image1" value="" >
            <input type="file" name="image2" value="" >
            <input type="file" name="image3" value="" >
            <textarea name="review" placeholder="review"></textarea>
            <input type="text" name="replyto_reviewid" value="" placeholder="replyto_reviewid">

            <input type="submit" value="Create">
            </form>
        </td>
    </tr>
    @endif
    @empty
        {{__('No Record')}} <a href="{{route('merchantstorecreate')}}">Apply Now</a>
    @endforelse
</table>


</div>

@endsection