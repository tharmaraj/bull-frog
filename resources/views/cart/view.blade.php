@extends('layouts.app')

@section('content')
<div class="container">
Cart View <a href="{{route('cartproductcreate')}}">Continue Shopping</a>
<br>
<a class="btn btn-lg btn-warning" href="{{route('paymentproductcreate')}}">Checkout</a>

    <table class="table table-border">
        <tr><td>Total</td><td>{{$paymentDetails['sum_total']}}</td></tr>
        <tr><td>Total Shipping</td><td>{{$paymentDetails['sum_totalshipping']}}</td></tr>
        <tr><td>Sub Total</td><td>{{$paymentDetails['sum_subtotal']}}</td></tr>
    </table>
    

    @forelse($response as $key => $result)
    {{$result['storeid']}} Shipping RM {{$result['shippingprice']}} Total RM {{$result['total']}} SubTotal RM {{$result['subtotal']}}
    <table class="table table-border">
        @foreach($result['variantid_Details'] as $key_variant => $variant)
        <tr>
            <td>{{$variant->name}}</td>
            <td>{{$variant->storeid}}</td>
            <td>{{$variant->productid}}</td>
            <td>{{ $result['variantid_Count'][$key_variant] }}</td>
            <td>RM {{$variant->sellingprice}}</td>
            <td><a href="{{ route('cartproductminus', [$variant->id,Auth::id()] ) }}">Minus</a></td>
            <td><a href="{{ route('cartproductdelete', [$variant->id,Auth::id()] ) }}">Delete</a></td>
        </tr>
        @endforeach
    </table>
    @empty
        {{__('No Record')}} 
    @endforelse



    @if ($errors->any())

        <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
        </div>
    @endif

    Voucher
    <form method="post" action="{{ route('voucherentrystore') }}" enctype="multipart/form-data">
    @csrf

    <input type="text" name="voucherid" value="" placeholder="voucherid">


    <input type="submit" value="Create">
    </form>


</div>


@endsection