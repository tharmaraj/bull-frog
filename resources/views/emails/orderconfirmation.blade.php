@component('mail::message')
# Hi

Thank you for Your Order.

Orderid : <b>{{$content->orderid}}</b><br>
Total : <b>RM {{$content->totalvalue}}</b>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
