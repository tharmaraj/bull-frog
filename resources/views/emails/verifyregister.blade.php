@component('mail::message')
# Dear {{$content->name}}

Thanks for signing up. The verfifcation code is : {{$content->otvkey}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
