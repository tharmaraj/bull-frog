@component('mail::message')
# Reset Password

A request has been received to change password for your {{$content->email}}.<br>
Use this Reset Password Code to reset password : <b>{{$content->resetpasskey}}</b>.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
