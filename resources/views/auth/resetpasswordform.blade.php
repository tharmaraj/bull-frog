@extends('layouts.app')

@section('content')

  
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>
                <div class="card-header bg-success text-white">{{ __('Please Check your email for Reset Key') }}</div>
                <div class="card-header text-white"><a href="{{route('resetpasswordview')}}">{{ __('Cleck Here if you didn`t receive the email') }}</a></div>


                @if ($errors->any())
                    <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                    </div>
                @endif

                <div class="card-body">
                    <form method="POST" action="{{ route('resetnewpassword') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="emailid" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="emailid" type="text" class="form-control" name="emailid" value="{{$emailid}}" readonly>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="resetpasskey" class="col-md-4 col-form-label text-md-right">{{ __('Reset Password Code') }}</label>

                            <div class="col-md-6">
                                <input id="resetpasskey" type="number" class="form-control" name="resetpasskey" value="" required autofocus>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="newpasword" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                            <div class="col-md-6">
                                <input id="newpasword" type="password" class="form-control" name="newpasword" value="" required>

                            </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
