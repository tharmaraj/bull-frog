@extends('layouts.app')

<script>
    window.onload = function() {
        CKEDITOR.replace( 'editor1' );
    };
</script>

@section('content')
<div class="container">
Create
<form method="post" action="{{ route('variantproductstore') }}" enctype="multipart/form-data">
@csrf

<input type="text" name="storeid" value="" placeholder="storeid">
<input type="text" name="merchantid" value="" placeholder="merchantid">
<input type="text" name="productid" value="" placeholder="productid">
<input type="text" name="variantid" value="" placeholder="variantid">
<input type="text" name="topsellind" value="1" placeholder="topsellind">

<input type="file" name="photo" value="" >
<input type="file" name="video" value="" >

<input type="text" name="name" value="" placeholder="name">
<input type="text" name="description" value="" placeholder="description">
<input type="text" name="averageweight" value="" placeholder="averageweight">
<input type="number" name="availablestock" value="10" placeholder="availablestock">
<input type="text" name="variantactivestatus" value="1" placeholder="variantactivestatus">
<input type="number" name="sellingprice" step=".01" value="" placeholder="sellingprice">
<input type="number" name="shippingprice" step=".01" value="" placeholder="shippingprice">
<input type="number" name="beforeprice" step=".01" value="" placeholder="beforeprice"> display only

<textarea name="whatisinside" placeholder="whatisinside"></textarea>add comma(,)
<textarea name="howtocook" id="editor1" placeholder="howtocook"></textarea>add comma(,)
<input type="text" name="readytocookind" value="" placeholder="readytocookind">

<input type="submit" value="Create">
</form>

</div>

@endsection