@extends('layouts.app')

@section('content')
<div class="container">
View <a href="{{route('variantproductcreate')}}">Create Variant</a>
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td><a href="{{ route('variantproductedit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('variantproductdelete', $result->id ) }}">Delete</a></td>
        <td><a href="{{ route('variantproductapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('variantproductreject', $result->id ) }}">Reject</a></td>
        <td>{{$result}}</td>
    </tr>
    @empty
        {{__('No Record')}} 
    @endforelse
</table>


</div>

@endsection