@extends('layouts.app')

<script>
    window.onload = function() {
        CKEDITOR.replace( 'editor1' );
    };
</script>

@section('content')
<div class="container">
Edit
<form method="post" action="{{ route('variantproductupdate',$results->id) }}" enctype="multipart/form-data">
@csrf

<input type="text" name="storeid" value="{{$results->storeid}}" placeholder="storeid">
<input type="text" name="merchantid" value="{{$results->merchantid}}" placeholder="merchantid">
<input type="text" name="productid" value="{{$results->productid}}" placeholder="productid">
<input type="text" name="variantid" value="{{$results->variantid}}" placeholder="variantid">
<input type="text" name="topsellind" value="{{$results->topsellind}}" placeholder="topsellind">

<input type="file" name="photo" value="" >
<a href="../files/{{$results->photo}}">{{$results->photo}}</a>
<input type="file" name="video" value="" >
<a href="../files/{{$results->video}}">{{$results->video}}</a>


<input type="text" name="name" value="{{$results->name}}" placeholder="name">
<input type="text" name="description" value="{{$results->description}}" placeholder="description">
<input type="text" name="averageweight" value="{{$results->averageweight}}" placeholder="averageweight">
<input type="number" name="availablestock" value="{{$results->availablestock}}" placeholder="availablestock">
<input type="text" name="variantactivestatus" value="{{$results->variantactivestatus}}" placeholder="variantactivestatus">
<input type="number" name="sellingprice" step=".01" value="{{$results->sellingprice}}" placeholder="sellingprice">
<input type="number" name="shippingprice" step=".01" value="{{$results->shippingprice}}" placeholder="shippingprice">
<input type="number" name="beforeprice" step=".01" value="{{$results->beforeprice}}" placeholder="beforeprice"> display only


<textarea name="whatisinside" placeholder="whatisinside">{{$results->whatisinside}}</textarea> add comma(,)
<textarea name="howtocook" id="editor1" placeholder="howtocook">{{$results->howtocook}}</textarea> add comma(,)
<input type="text" name="readytocookind" value="{{$results->readytocookind}}" placeholder="readytocookind">

<input type="submit" value="Update">
</form>
</div>

@endsection