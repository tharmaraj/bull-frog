@extends('layouts.app')

@section('content')
<div class="container">
Create
<form method="post" action="{{ route('productstoreupdate',$results->id) }}" enctype="multipart/form-data">
@csrf
<input type="file" name="photo1" value="" >
<a href="../files/{{$results->photo1}}">{{$results->photo1}}</a>
<input type="file" name="photo2" value="" >
<a href="../files/{{$results->photo2}}">{{$results->photo2}}</a>
<input type="file" name="photo3" value="" >
<a href="../files/{{$results->photo3}}">{{$results->photo3}}</a>
<input type="file" name="photo4" value="" >
<a href="../files/{{$results->photo4}}">{{$results->photo4}}</a>
<input type="file" name="photo5" value="" >
<a href="../files/{{$results->photo5}}">{{$results->photo5}}</a>Video Here

<input type="text" name="name" value="{{$results->name}}" placeholder="name">
{!! dropDown($categories,'category',$results->category) !!}
{!! dropDown($subcategories,'subcategory',$results->subcategory) !!}
<!-- <input type="number" name="category" value="{{$results->category}}" placeholder="category">
<input type="number" name="subcategory" value="{{$results->subcategory}}" placeholder="subcategory"> -->
<input type="text" name="description" value="{{$results->description}}" placeholder="description">
<input type="number" name="deliveryperiod" value="{{$results->deliveryperiod}}" placeholder="deliveryperiod"> 24 Hours
<input type="text" name="unitmeasurement" value="{{$results->unitmeasurement}}" placeholder="unitmeasurement">
<input type="text" name="youtube" value="{{$results->youtube}}" placeholder="youtube">
<input type="text" name="defaultshippingprice" value="{{$results->defaultshippingprice}}" placeholder="defaultshippingprice">
<input type="number" name="rating" value="{{$results->rating}}" placeholder="rating">

<input type="number" name="topsellind" value="{{$results->topsellind}}" placeholder="topsellind">
<input type="number" name="readytocookind" value="{{$results->readytocookind}}" placeholder="readytocookind">
<input type="number" name="packageind" value="{{$results->packageind}}" placeholder="packageind">
<input type="number" name="morningmarketind" value="{{$results->morningmarketind}}" placeholder="morningmarketind">



<input type="submit" value="Update">
</form>
</div>

@endsection