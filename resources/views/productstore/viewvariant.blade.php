@extends('layouts.app')

<script>
    window.onload = function() {
        CKEDITOR.replace( 'editor1' );
    };
</script>

@section('content')
<div class="container">

Create
<form method="post" action="{{ route('variantproductstore') }}" enctype="multipart/form-data">
@csrf

<input type="text" name="storeid" value="{{$results->storeid}}" placeholder="storeid" readonly>
<input type="text" name="merchantid" value="{{$results->merchantid}}" placeholder="merchantid" readonly>
<input type="text" name="productid" value="{{$results->productid}}" placeholder="productid" readonly>
<input type="text" name="variantid" value="{{$results->variantid}}" placeholder="variantid" readonly>
<input type="text" name="topsellind" value="1" placeholder="topsellind">

<input type="file" name="photo" value="" >
<input type="file" name="video" value="" >

<input type="text" name="name" value="" placeholder="name">
<input type="text" name="description" value="" placeholder="description">
<input type="text" name="averageweight" value="" placeholder="averageweight">
<input type="number" name="availablestock" value="10" placeholder="availablestock">
<input type="text" name="variantactivestatus" value="1" placeholder="variantactivestatus">
<input type="number" name="sellingprice" step=".01" value="" placeholder="sellingprice">
<input type="number" name="shippingprice" step=".01" value="" placeholder="shippingprice">

<input type="number" name="beforeprice" step=".01" value="" placeholder="beforeprice"> display only

<textarea name="whatisinside" placeholder="whatisinside"></textarea>add comma(,)
<textarea name="howtocook" id="editor1" placeholder="howtocook"></textarea>add comma(,)
<input type="text" name="readytocookind" value="" placeholder="readytocookind">

<input type="submit" value="Add Variant">
</form>


<table class="table table-border">
    @forelse($resultslist as $result)
    <tr>
        <td><img src='{{geturlfile($result->photo)}}' width='50'><br>{{$result}}</td>
        <td><a href="{{ route('variantproductedit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('variantproductdelete', $result->id ) }}">Delete</a></td>
    </tr>
    <tr>
        <th>Item List</th>
    </tr>

    @forelse($result->variantitemlists as $resultvariantlist)
    <tr class="bg-secondary text-white">
        <td><img width=50 src="<?=geturlfile($resultvariantlist->photo1)?>"><br>{{$resultvariantlist}}</td>
        <td><a href="{{ route('variantitemlistedit', $resultvariantlist->id ) }}">Edit</a></td>
        <td><a href="{{ route('variantitemlistdelete', $resultvariantlist->id ) }}">Delete</a></td>
    </tr>
    @empty
    <tr class="bg-secondary text-white">
        <td>{{__('No Record')}}</td>
    </tr>
    @endforelse

    <tr class="bg-secondary text-white">
        <td>
        <form method="post" action="{{ route('variantitemliststore') }}" enctype="multipart/form-data">
        @csrf

        <input type="number" readonly name="variantid" value="{{$result->variantid}}" placeholder="variantid" readonly>
        <input type="number" readonly name="variant_ID" value="{{$result->id}}" placeholder="storeid" readonly>
        <input type="file" name="photo1" value="" >
        <input type="text" name="name" value="" placeholder="name">
        <input type="number" name="count" value="" placeholder="count" >
        <input type="submit" value="Add Variant">
        </form>
</td>

    </tr>
    @empty
        {{__('No Record')}} 
    @endforelse
</table>


</div>

@endsection