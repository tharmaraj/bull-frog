@extends('layouts.app')

@section('content')
<div class="container">
Create
<form method="post" action="{{ route('productstorestore') }}" enctype="multipart/form-data">
@csrf

<!-- <input type="text" name="storeid" value="" placeholder="storeid"> -->

@foreach($merchantstore as $ms)
    <input type="radio" required name="storeid" id="{{$ms->storeid}}" value="{{$ms->storeid}}"><label for="{{$ms->storeid}}">{{$ms->fullstorename}}</label><br>
@endforeach

<input type="file" name="photo1" value="" >
<input type="file" name="photo2" value="" >
<input type="file" name="photo3" value="" >
<input type="file" name="photo4" value="" >
<input type="file" name="photo5" value="" >Video Here

<input type="text" name="name" value="" placeholder="name">
{!! dropDown($categories,'category','') !!}
{!! dropDown($subcategories,'subcategory','') !!}
<!-- <input type="number" name="category" value="" placeholder="category">
<input type="number" name="subcategory" value="" placeholder="subcategory"> -->
<input type="text" name="description" value="" placeholder="description">
<input type="number" name="deliveryperiod" value="1" placeholder="deliveryperiod"> 24 Hours
<input type="text" name="unitmeasurement" value="" placeholder="unitmeasurement 1- 100 Gram">
<input type="text" name="youtube" value="-" placeholder="youtube">
<input type="number" name="rating" value="3" placeholder="rating">

<input type="number" name="topsellind" value="1" placeholder="topsellind">
<input type="number" name="readytocookind" value="0" placeholder="readytocookind">
<input type="number" name="packageind" value="0" placeholder="packageind">

@if(isset($morningmarketind))
<input type="number" name="morningmarketind" value="1" placeholder="morningmarketind">
@else
<input type="number" name="morningmarketind" value="0" placeholder="morningmarketind">
@endif

<input type="submit" value="Create and Add Variant">
</form>

</div>

@endsection