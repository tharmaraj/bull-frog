@extends('layouts.app')

@section('content')
<div class="container">
View <a href="{{route('productstorecreate')}}">Create Product</a>
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td><td><img width=50 src="<?=geturlfile($result->photo1)?>"></td></td>
        <td><a href="{{ route('viewvariant', $result->variantid ) }}">Variant</a></td>
        <td><a href="{{ route('productstoreedit', $result->id ) }}">Edit</a></td>
        <td>{{$result}}</td>
        <td><a href="{{ route('productstoredelete', $result->id ) }}">Delete</a></td>
        @if(Auth::user()->role == '1')
        <td><a href="{{ route('productstoreapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('productstorereject', $result->id ) }}">Reject</a></td>
        @endif
    </tr>
    @empty
        {{__('No Record')}} 
    @endforelse
</table>


</div>

@endsection