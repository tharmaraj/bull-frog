@extends('layouts.app')

@section('content')
<div class="container">
Products List Morning Market, You can add to cart

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td><img width=50 src="<?=geturlfile($result->photo1)?>"></td>
        <td>{{$result->name}}</td>
        <!-- <td>{{$result}}</td> -->
        
        <td>
        <table class="table table-border">
        @forelse($result->variant as $resvariant)
        <tr>
            <td><img width=50 src="<?=geturlfile($resvariant->photo)?>"></td>
            <td>{{$resvariant->name}}</td>
            <td>RM {{$resvariant->sellingprice}}</td>
            <td>RM {{$resvariant->shippingprice}}</td>
            <td>

            <form method="post" action="{{ route('cartproductstore') }}" enctype="multipart/form-data">
            <input type="number" name="storeid" value="{{$resvariant->storeid}}" placeholder="storeid" readonly>
            <input type="number" name="merchantid" value="{{$resvariant->merchantid}}" placeholder="merchantid" readonly>
            <input type="number" name="productid" value="{{$resvariant->productid}}" placeholder="productid" readonly>
            <input type="number" name="variantid" value="{{$resvariant->variantid}}" placeholder="variantid" readonly>
            <input type="number" name="variantid_ID" value="{{$resvariant->id}}" placeholder="variantid_ID" readonly>
            <input type="submit" value="Add">

            </form>

            <table>
                <tr>
                    <td>Customer Message</td>
                    <td>Merchant Message</td>
                    <td>Originalamount</td>
                    <td>Bargainamount</td>
                    <td>Customerbargainind</td>
                    <td>Merchantbargainind</td>
                </tr>
                <?php 
                $last_customerbargainind = 0 ;
                $last_merchantbargainind = 0 ;
                ?>
                @foreach($resvariant->bargain as $bargainres)
                <tr>
                    <td>{{$bargainres->customer_message}}</td>
                    <td>{{$bargainres->merchant_message}}</td>
                    <td>{{$bargainres->originalamount}}</td>
                    <td>{{$bargainres->bargainamount}}</td>
                    <td>{{$bargainres->customerbargainind}}</td>
                    @if($bargainres->merchantbargainind == 1)
                        @if($bargainres->customerbargainind != 1)
                    <td><a href="{{ route('bargaincustomerapprove', $bargainres->id ) }}">Approve</a></td>
                        @endif
                    <td><a href="{{ route('bargaincustomerreject', $bargainres->id ) }}">Reject</a></td>
                    @endif
                    <td>{{$bargainres->merchantbargainind}}</td>
                </tr>
                <?php
                $last_customerbargainind = $bargainres->customerbargainind;
                $last_merchantbargainind = $bargainres->merchantbargainind;
                ?>
                @endforeach
            </table>

            
            Bargain
            @if($last_merchantbargainind == 2 || $last_merchantbargainind == 0)
            <form method="post" action="{{ route('bargainstore') }}" enctype="multipart/form-data">
            <input type="number" name="productid" value="{{$resvariant->productid}}" placeholder="productid" readonly>
            <input type="number" name="variantid" value="{{$resvariant->variantid}}" placeholder="variantid" readonly>
            <input type="number" name="variantid_ID" value="{{$resvariant->id}}" placeholder="variantid_ID" readonly>
            <input type="number" name="originalamount" value="{{$resvariant->sellingprice}}" placeholder="originalamount" readonly>
            <input type="text" name="customer_message" placeholder="customer_message" >
            <input type="number" name="bargainamount" step=".01" min="10" max="{{$resvariant->sellingprice}}" placeholder="bargainamount" required>
            <input type="submit" value="Bargain">
            </form>
            @endif


            </td>
            <!-- <td>{{$resvariant}}</td> -->
        </tr>
        @empty
        {{__('No Variant Record')}} 
        @endforelse
        </table>
        </td>

    </tr>
    @empty
        {{__('No Record')}} 
    @endforelse
</table>


</div>

@endsection