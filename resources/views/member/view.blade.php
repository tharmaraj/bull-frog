@extends('layouts.app')

@section('content')
<div class="container">
All Members
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result->id}}</td>
        <td>{{$result->userid}}</td>
        <td>{{$result->email}}</td>
        <td>{{getRole($result->role)}}</td>
        <td>{{$result->activestatus}}</td>
        @if(Auth::user()->role == '1')
        <td><a href="{{ route('memberapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('memberreject', $result->id ) }}">Reject</a></td>
        <td><a href="{{ route('memberlogin', $result->id ) }}">Login</a></td>
        @endif
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection