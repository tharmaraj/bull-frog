@extends('layouts.app')

@section('content')
<div class="container">
Edit
<form method="post" action="{{ route('merchantupdate',$results->id) }}" enctype="multipart/form-data">
@csrf

<input type="text" name="name" value="{{$results->name}}" placeholder="name">
<input type="text" name="email" value="{{$results->email}}" placeholder="email">
<input type="text" name="ic" value="{{$results->ic}}" placeholder="ic">
<input type="text" name="phone" value="{{$results->phone}}" placeholder="phone">
<input type="text" name="address1" value="{{$results->address1}}" placeholder="add">
<input type="text" name="address2" value="{{$results->address2}}" placeholder="add">
<input type="text" name="poscood" value="{{$results->poscood}}" placeholder="poscod">
<input type="text" name="city" value="{{$results->city}}" placeholder="city">
<!-- <input type="text" name="state" value="{{$results->state}}" placeholder="state"> -->
{!! stateList($results->state) !!}
<input type="text" name="deliveryavailable" value="{{$results->deliveryavailable}}" placeholder="delivery available">


<input type="file" name="profileimage" value="" >
<a href="../files/{{$results->profileimage}}">{{$results->profileimage}}</a>
<input type="file" name="supportdoc1" value="" >
<a href="../files/{{$results->supportdoc1}}">{{$results->supportdoc1}}</a>
<input type="file" name="supportdoc2" value="" >
<a href="../files/{{$results->supportdoc2}}">{{$results->supportdoc2}}</a>


<input type="submit" value="Update">
</form>
</div>

@endsection