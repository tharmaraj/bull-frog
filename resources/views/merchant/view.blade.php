@extends('layouts.app')

@section('content')
<div class="container">
View
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result->userid}}</td>
        <td>{{$result->name}}</td>
        <td>{{$result->storeactivestatus}}</td>
        <td><a href="{{ route('merchantedit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('merchantdelete', $result->id ) }}">Delete</a></td>
        @if(Auth::user()->role == '1')
        <td><a href="{{ route('merchantapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('merchantreject', $result->id ) }}">Reject</a></td>
        @endif
    </tr>
    @empty
        {{__('No Record')}} <a href="{{route('merchantcreate')}}">Apply Now</a>
    @endforelse
</table>


</div>

@endsection