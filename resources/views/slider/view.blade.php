@extends('layouts.app')

@section('content')
<div class="container">
Slider
<br>
<a href="{{route('slidercreate')}}">Create New</a>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td><img width=50 src="<?=geturlfile($result->image)?>"></td>
        <td>{{$result}}</td>
        <td><a href="{{ route('slideredit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('sliderapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('sliderreject', $result->id ) }}">Reject</a></td>
        <td><a href="{{ route('sliderdelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection