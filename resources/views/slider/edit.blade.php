@extends('layouts.app')

@section('content')
<div class="container">
Edit
<form method="post" action="{{ route('sliderupdate',$results->id) }}" enctype="multipart/form-data">
@csrf

<input type="file" name="image" value="">
<input type="text" name="name" value="{{$results->name}}" placeholder="name">
<input type="text" name="url" value="{{$results->url}}" placeholder="url">
<input type="date" name="startdate" value="{{$results->startdate}}" placeholder="startdate">
<input type="date" name="enddate" value="{{$results->enddate}}" placeholder="enddate">
<input type="number" name="adds" value="{{$results->adds}}" placeholder="adds"> 2 = Announcement & 1 = Adds & 0 - Slider
<textarea name="productid" placeholder="productid">{{$results->productid}}</textarea>


<input type="submit" value="Update">
</form>
</div>

@endsection