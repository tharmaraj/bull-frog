@extends('layouts.app')

@section('content')
<div class="container">
Sub Category <a href="{{route('subcategorycreate')}}">Create New</a>
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td><img width=50 src="<?=geturlfile($result->image)?>"></td>
        <td>{{$result}}</td>
        <td><a href="{{ route('subcategoryedit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('subcategorydelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection