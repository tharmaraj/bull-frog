@extends('layouts.app')

@section('content')
<div class="container">
Edit
<form method="post" action="{{ route('subcategoryupdate',$results->id) }}" enctype="multipart/form-data">
@csrf


{!! dropDown($categories,'category',$results->category) !!}

<input type="text" name="name" value="{{$results->name}}" placeholder="name">
<input type="file" name="image" value="" >
<input type="number" name="index" value="{{$results->index}}" placeholder="index">


<input type="submit" value="Update">
</form>
</div>

@endsection