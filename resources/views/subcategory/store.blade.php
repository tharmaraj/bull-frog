@extends('layouts.app')

@section('content')
<div class="container">
Create
<form method="post" action="{{ route('subcategorystore') }}" enctype="multipart/form-data">
@csrf

@if($results)
{!! dropDown($categories,'category',$results->category) !!}
@else
{!! dropDown($categories,'category','') !!}
@endif

<input type="text" name="name" value="" placeholder="name">
<input type="file" name="image" value="" >
<input type="number" name="index" value="{{$results == null ? '1' : $results->index + 1}}" placeholder="index">


<input type="submit" value="Create">
</form>
</div>

@endsection