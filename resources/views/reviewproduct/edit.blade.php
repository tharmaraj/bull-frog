@extends('layouts.app')

@section('content')
<div class="container">
Edit
<form method="post" action="{{ route('reviewproductupdate',$results->id) }}" enctype="multipart/form-data">
@csrf

<input type="text" name="productid" value="{{$results->productid}}" placeholder="productid">
<input type="text" name="variantid" value="{{$results->variantid}}" placeholder="variantid">
<input type="text" name="variantid_ID" value="{{$results->variantid_ID}}" placeholder="variantid_ID">
<input type="text" name="orderid" value="{{$results->orderid}}" placeholder="orderid">
<input type="text" name="star" value="{{$results->star}}" placeholder="star">
<input type="file" name="image1" value="" >
<input type="file" name="image2" value="" >
<input type="file" name="image3" value="" >
<textarea name="review" placeholder="review">{{$results->review}}</textarea>
<input type="text" name="replyto_reviewid" value="{{$results->replyto_reviewid}}" placeholder="replyto_reviewid">

<input type="submit" value="Update">
</form>
</div>

@endsection