@extends('layouts.app')

@section('content')
<div class="container">
Review <a href="{{route('reviewproductcreate')}}">Create New</a>
<br>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td><img width=50 src="<?=geturlfile($result->image1)?>"></td>
        <td>{{$result}}</td>
        <td><a href="{{ route('reviewproductedit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('reviewproductapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('reviewproductreject', $result->id ) }}">Reject</a></td>
        <td><a href="{{ route('reviewproductdelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection