@extends('layouts.app')

@section('content')
<div class="container">
Create
<form method="post" action="{{ route('categorystore') }}" enctype="multipart/form-data">
@csrf

<input type="text" name="name" value="" placeholder="name">
<input type="file" name="image" value="" >
<input type="number" name="index" value="{{$results == null ? '1' : $results->index + 1}}" placeholder="index">


<input type="submit" value="Create">
</form>
</div>

@endsection