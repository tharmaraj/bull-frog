@extends('layouts.app')

@section('content')
<div class="container">
Edit
<form method="post" action="{{ route('voucherupdate',$results->id) }}" enctype="multipart/form-data">
@csrf

<input type="text" name="name" value="{{$results->name}}" placeholder="name">
<input type="datetime-local" name="startdatetime" value="{{ date('Y-m-d\TH:i', strtotime($results->startdatetime)) }}" placeholder="startdatetime">
<input type="datetime-local" name="enddatetime" value="{{ date('Y-m-d\TH:i', strtotime($results->enddatetime)) }}" placeholder="enddatetime">
<input type="number" name="voucherpricetype" value="{{$results->voucherpricetype}}" placeholder="voucherpricetype">
1 = % | 2 = RM __ . __
<input type="text" name="user" value="{{$results->user}}" placeholder="user">
// * for all
<input type="number" name="value" value="{{$results->value}}" placeholder="value" step="0.01">
<input type="number" name="minimumpurchase" value="{{$results->minimumpurchase}}" placeholder="minimumpurchase" step="0.01">



<input type="submit" value="Update">
</form>
</div>

@endsection