@extends('layouts.app')

@section('content')
<div class="container">
Voucher
<br>
<a href="{{route('vouchercreate')}}">Create New</a>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result}}</td>
        <td><a href="{{ route('voucheredit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('voucherapprove', $result->id ) }}">Approve</a></td>
        <td><a href="{{ route('voucherreject', $result->id ) }}">Reject</a></td>
        <td><a href="{{ route('voucherdelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection