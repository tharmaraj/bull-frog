@extends('layouts.app')

@section('content')
<div class="container">
Create
<form method="post" action="{{ route('voucherstore') }}" enctype="multipart/form-data">
@csrf

<input type="text" name="voucherid" value="{{rand()}}" placeholder="voucherid">
<input type="text" name="name" value="" placeholder="name">
<input type="datetime-local" name="startdatetime" value="" placeholder="startdatetime">
<input type="datetime-local" name="enddatetime" value="" placeholder="enddatetime">
<input type="number" name="voucherpricetype" value="" placeholder="voucherpricetype">
// 1 = % | 2 = RM __ . __
<input type="text" name="user" value="" placeholder="user">
// * for all
<input type="number" name="value" value="" placeholder="value" step="0.01">
<input type="number" name="minimumpurchase" value="" placeholder="minimumpurchase" step="0.01">

<input type="submit" value="Create">
</form>
</div>

@endsection