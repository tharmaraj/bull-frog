@extends('layouts.app')

@section('content')
<div class="container">
Products List, You can add to cart

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td><img width=50 src="<?=geturlfile($result->photo1)?>"></td>
        <td>{{$result->name}}</td>
        <!-- <td>{{$result}}</td> -->
        
        <td>
        <table class="table table-border">
        @forelse($result->variant as $resvariant)
        <tr>
            <td><img width=50 src="<?=geturlfile($resvariant->photo)?>"></td>
            <td>{{$resvariant->name}}</td>
            <td>RM {{$resvariant->sellingprice}}</td>
            <td>RM {{$resvariant->shippingprice}}</td>
            <td>

            <form method="post" action="{{ route('cartproductstore') }}" enctype="multipart/form-data">
            <input type="number" name="storeid" value="{{$resvariant->storeid}}" placeholder="storeid" readonly>
            <input type="number" name="merchantid" value="{{$resvariant->merchantid}}" placeholder="merchantid" readonly>
            <input type="number" name="productid" value="{{$resvariant->productid}}" placeholder="productid" readonly>
            <input type="number" name="variantid" value="{{$resvariant->variantid}}" placeholder="variantid" readonly>
            <input type="number" name="variantid_ID" value="{{$resvariant->id}}" placeholder="variantid_ID" readonly>
            <input type="submit" value="Add">

            </form>
            </td>
            <!-- <td>{{$resvariant}}</td> -->
        </tr>
        @empty
        {{__('No Variant Record')}} 
        @endforelse
        </table>
        </td>

    </tr>
    @empty
        {{__('No Record')}} 
    @endforelse
</table>


</div>

@endsection