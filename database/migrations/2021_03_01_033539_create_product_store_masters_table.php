<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductStoreMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_store_masters', function (Blueprint $table) {
            $table->id();

            //storeid
            $table->integer('storeid');
            //merchantid
            $table->integer('merchantid');
            //productid
            $table->integer('productid');

            //basic
            $table->string('name');
            $table->integer('category');
            $table->integer('subcategory');
            $table->longText('description');
            $table->double('deliveryperiod'); //based on hour
            
            //weight
            $table->string('unitmeasurement'); //this is only for display
            
            
            //media
            $table->string('photo1');
            $table->string('photo2');
            $table->string('photo3');
            $table->string('photo4');
            $table->string('photo5');
            $table->string('youtube');
            $table->integer('rating')->nullable();
            
            //variant options
            $table->longText('variantid');

            
            // //pricing masuk dalam variant
            // $table->double('sellingprice');

            // $table->double('shippingprice');
            // $table->double('company');
            // $table->double('ipay');
            
            // $table->double('profit');

            $table->integer('topsellind')->nullable();
            $table->integer('readytocookind')->nullable();
            $table->integer('packageind')->nullable();

            $table->integer('morningmarketind')->nullable();

            $table->integer('bargainid')->nullable();
            $table->integer('bargainindicator')->nullable();


            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_store_masters');
    }
}
