<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->id();
            $table->string('voucherid');
            $table->string('name');
            $table->dateTime('startdatetime');
            $table->dateTime('enddatetime');
            $table->integer('voucherpricetype'); //percentage or cash
            $table->double('value'); //percentage or cash
            $table->string('user'); //* for all
            $table->double('minimumpurchase'); //* for all

            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
