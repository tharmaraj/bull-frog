<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantProductMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variant_product_masters', function (Blueprint $table) {
            $table->id();

            //storeid
            $table->integer('storeid');
            //merchantid
            $table->integer('merchantid');
            //productid
            $table->integer('productid');
            //variantid
            $table->integer('variantid');
            
            $table->longText('name');
            $table->longText('description');
            $table->longText('averageweight');
            $table->longText('photo');
            $table->longText('video')->nullable();
            $table->integer('availablestock');
            $table->integer('variantactivestatus');

            //pricing
            $table->double('sellingprice');
            $table->double('shippingprice');
            $table->double('company');
            $table->double('ipay');
            $table->double('profit');
            $table->double('beforeprice')->nullable(); //display only

            $table->integer('topsellind');

            $table->longText('whatisinside')->nullable();
            $table->longText('howtocook')->nullable();
            $table->string('readytocookind')->nullable();
            
            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variant_product_masters');
    }
}
