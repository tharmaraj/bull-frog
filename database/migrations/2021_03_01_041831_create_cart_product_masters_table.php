<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartProductMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_product_masters', function (Blueprint $table) {
            $table->id();

            //storeid
            $table->integer('storeid');
            //merchantid
            $table->integer('merchantid');
            //productid
            $table->integer('productid');
            //variantid
            $table->integer('variantid');
            //variantid ID only
            $table->integer('variantid_ID');
            //userid
            $table->integer('userid');
            
            $table->integer('count');

            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();

            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_product_masters');
    }
}
