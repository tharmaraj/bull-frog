<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentProductMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_product_masters', function (Blueprint $table) {
            $table->id();

            
            //storeid
            $table->integer('storeid');
            //merchantid
            $table->integer('merchantid');
            //productid
            $table->integer('productid');
            //variantid
            $table->integer('variantid');
            //variantid ID only
            $table->integer('variantid_ID');
            //userid
            $table->integer('userid');
            
            $table->integer('orderid');

            $table->integer('count');

            //every product's
            $table->double('shippingvalue'); //will groub if one product
            
            //pricing
            $table->double('totalcart');
            $table->double('totalshipping');
            $table->double('subtotal');
            
            //confirmation
            $table->integer('finalsubmit');
            
            $table->unsignedBigInteger('ordershippingstatus');
            $table->integer('orderpaymentstatus');

            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();


            $table->timestamps();

            $table->foreign('ordershippingstatus')->references('id')->on('order_shipping_statuses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_product_masters');
    }
}
