<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantStoreMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_store_masters', function (Blueprint $table) {
            $table->id();

            //storeid
            $table->integer('storeid');
            
            //merchantid
            $table->integer('merchantid');

            //files
            $table->string('storeimage');
            $table->string('storelogo');
            
            //profile
            $table->string('fullstorename');
            $table->string('nickstorename');
            $table->string('storephone');
            $table->string('address1');
            $table->string('address2');
            $table->string('poscood');
            $table->string('city');
            $table->string('state');

            $table->string('openingtime');
            $table->string('closingtime');
            $table->string('holiday'); // can write public will close or call before
            
            //social
            $table->string('instagram');
            $table->string('facebook');
            $table->string('youtube');
            $table->string('website');
            $table->string('location');

            $table->double('defaultshippingprice');
            $table->integer('cod')->default('0'); // COD

            $table->string('activestatus');

            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();

            
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_store_masters');
    }
}
