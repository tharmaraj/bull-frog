<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_masters', function (Blueprint $table) {
            $table->id();

            //userid
            $table->integer('userid');
            //merchantid
            $table->integer('merchantid');

            //status
            $table->integer('activestatus');

            //files
            $table->string('profileimage')->nullable();
            $table->string('supportdoc1')->nullable();
            $table->string('supportdoc2')->nullable();
            $table->string('supportdoc3')->nullable();
            $table->string('supportdoc4')->nullable();
            $table->string('supportdoc5')->nullable();

            //profile
            $table->string('name');
            $table->string('email');
            $table->string('ic');
            $table->string('phone');
            $table->string('address1');
            $table->string('address2');
            $table->string('poscood');
            $table->string('city');
            $table->string('state');
            
            //thick
            $table->integer('deliveryavailable');
            
            //admin setting
            $table->integer('storeactivestatus');
            $table->integer('limitproduct');
            $table->integer('maxproductvalue');
            $table->integer('minproductvalue');


            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();
            


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_masters');
    }
}
