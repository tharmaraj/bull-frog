<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBargainModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bargain_models', function (Blueprint $table) {
            $table->id();
            
            $table->string('productid');
            $table->string('variantid');
            $table->string('variant_ID');
            $table->double('originalamount');
            $table->longText('customer_message')->nullable();
            $table->longText('merchant_message')->nullable();
            $table->double('bargainamount');

            $table->integer('customerbargainind');
            $table->integer('merchantbargainind');
            
            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bargain_models');
    }
}
