<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_masters', function (Blueprint $table) {
            $table->id();
            
            $table->string('image');
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->longtext('productid')->nullable();

            $table->string('startdate')->nullable();
            $table->string('enddate')->nullable();

            $table->integer('adds')->nullable();

            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider_masters');
    }
}
