<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewProductModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_product_models', function (Blueprint $table) {
            $table->id();
            $table->string('productid');
            $table->string('variantid')->nullable();
            $table->string('variantid_ID')->nullable();
            $table->string('orderid')->nullable();
            
            $table->string('star')->nullable();
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->longtext('review');

            $table->string('replyto_reviewid')->nullable();

            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_product_models');
    }
}
