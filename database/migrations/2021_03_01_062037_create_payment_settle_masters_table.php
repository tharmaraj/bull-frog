<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSettleMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_settle_masters', function (Blueprint $table) {
            $table->id();
            
            $table->integer('userid');
            $table->string('orderid');
            $table->string('voucherid')->nullable(); // optional

            $table->string('totalvalue');
            
            $table->string('paymentmethod');
            $table->string('phone');
            $table->string('shippingaddressid');
            $table->longText('data');
            
            $table->string('changesmoney')->nullable(); //data only

            $table->string('paymentgatewayid')->nullable(); //Payment Gateway

            $table->longText('receipt_copy')->nullable();
            $table->longText('invoice_copy')->nullable();

            $table->integer('payment_status')->default('2');
            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_settle_masters');
    }
}
