<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_masters', function (Blueprint $table) {
            $table->id();
            
            $table->string('userid');
            $table->string('address1');
            $table->string('address2');
            $table->string('postcode');
            $table->string('city');
            $table->string('state');

            $table->longtext('geoaddress')->nullable();
            $table->longtext('geolat')->nullable();
            $table->longtext('geolong')->nullable();

            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_masters');
    }
}
