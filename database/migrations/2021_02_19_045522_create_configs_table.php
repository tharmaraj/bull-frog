<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->id();
            //payment tick
            $table->string('cod')->nullable();
            $table->string('fpx')->nullable();

            $table->string('companyname')->nullable();

            $table->string('string1')->nullable();
            $table->string('string2')->nullable();
            $table->string('string3')->nullable();
            $table->string('string4')->nullable();
            $table->string('string5')->nullable();
            $table->string('string6')->nullable();
            $table->string('seeall')->nullable();

            //colors
            $table->string('primaryColor')->nullable();
            $table->string('secondaryColor')->nullable();
            $table->string('infoColor')->nullable();
            $table->string('warningColor')->nullable();
            $table->string('dangerColor')->nullable();
            $table->string('succcessColor')->nullable();
            
            //payment value
            $table->string('shippingCommission')->nullable();
            $table->string('companyCommission')->nullable();
            $table->string('ipayCommission')->nullable();
            
            $table->string('backgroundimage')->nullable();

            $table->string('limitproductformerchant')->default('10');
            $table->string('maxproductvaluemerchant')->default('200');
            $table->string('minproductvaluemerchant')->default('10');
            $table->string('companycommissionpercentage')->default('11');
            $table->string('ipay88commissionpercentage')->default('0');

            $table->string('defaultshippingprice')->default('2');

            $table->string('defaultProfileImage')->nullable();
            $table->string('fastsaleimage')->nullable();
            $table->string('hotsaleimage')->nullable();
            $table->string('offersaleimage')->nullable();
            $table->string('noDataImage')->nullable();

            $table->string('minimumpurchasevalue')->nullable();
            $table->string('maximumpurchasevalue')->nullable();

            $table->string('currentmobileversion')->nullable();
            
            $table->string('activestatus');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
