<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantItemListMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variant_item_list_masters', function (Blueprint $table) {
            $table->id();
            //media
            $table->integer('variantid');
            $table->integer('variant_ID');
            $table->string('photo1')->nullable();
            $table->string('name')->nullable();
            $table->string('count')->nullable();

            $table->string('activestatus');
            
            $table->string('createdby')->nullable();
            $table->string('updatedby')->nullable();
            $table->string('updateddate')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variant_item_list_masters');
    }
}
