<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEasyModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('easy_models', function (Blueprint $table) {
            $table->id();
            $table->string('channelid');
            $table->string('description')->nullable();
            $table->string('note')->nullable();
            $table->longtext('photobase')->nullable();
            $table->double('amount')->nullable();
            $table->string('paymenttype')->nullable();
            $table->string('storeind')->nullable();
            $table->string('activestatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('easy_models');
    }
}
