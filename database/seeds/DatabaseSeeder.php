<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        DB::table('users')->insert([
            'userid' => rand(),
            'name' => 'Tharmaraj',
            'phone' => '0133052350',
            'email' => 'tharmaraj123@yahoo.com',
            'password' => Hash::make('123123123'),
            'role' => '1',
            'activestatus' => '1',
            'type' => '1',
        ]);
        DB::table('users')->insert([
            'userid' => rand(),
            'name' => 'Tharmaraj',
            'phone' => '0133052350',
            'email' => 'softwareprojectplus@gmail.com',
            'password' => Hash::make('123123123'),
            'role' => '3',
            'activestatus' => '1',
            'type' => '1',
        ]);
        DB::table('users')->insert([
            'userid' => rand(),
            'name' => 'Tharmaraj',
            'phone' => '0133052350',
            'email' => 'rajinfomina@gmail.com',
            'password' => Hash::make('123123123'),
            'role' => '2',
            'activestatus' => '1',
            'type' => '1',
        ]);

        DB::table('configs')->insert([
            'companyname' => 'BullFrog',
            'string1' => 'Top Sell',
            'seeall' => 'See All',

            'backgroundimage' => Request::url().'/images/chickenmeat.jpg',
            'activestatus' => '1',
        ]);

        DB::table('category_masters')->insert([
        'index' => 2,
        'name' => 'Chicken',
        'image' => Request::url().'/images/chicken.png',
        'activestatus' => '1',
        ]);
        DB::table('category_masters')->insert([
        'index' => 3,
        'name' => 'Fish',
        'image' => Request::url().'/images/fish.png',
        'activestatus' => '1',
        ]);
        DB::table('category_masters')->insert([
        'index' => 1,
        'name' => 'Goat',
        'image' => Request::url().'/images/goat.png',
        'activestatus' => '1',
        ]);


        DB::table('sub_category_masters')->insert([
        'category' => 1,
        'index' => 1,
        'name' => 'Kampung',
        'image' => Request::url().'/images/ayamkampung.png',
        'activestatus' => '1',
        ]);
        DB::table('sub_category_masters')->insert([
        'category' => 1,
        'index' => 2,
        'name' => 'Daging',
        'image' => Request::url().'/images/ayamdaging.png',
        'activestatus' => '1',
        ]);
        DB::table('sub_category_masters')->insert([
        'category' => 1,
        'index' => 3,
        'name' => 'Telur',
        'image' => Request::url().'/images/ayamtelur.png',
        'activestatus' => '1',
        ]);

        DB::table('product_unit_measurement_masters')->insert([
            'name' => 'KG',
            'activestatus' => '1',
        ]);

        DB::table('order_payment_statuses')->insert([
            'name' => 'Pending',
            'image' => Request::url().'/images/ayamtelur.png',
            'index' => 1,
            'activestatus' => '1',
        ]);
        DB::table('order_payment_statuses')->insert([
            'name' => 'Payed',
            'image' => Request::url().'/images/ayamtelur.png',
            'index' => 2,
            'activestatus' => '1',
        ]);
        DB::table('order_payment_statuses')->insert([
            'name' => 'Failed',
            'image' => Request::url().'/images/ayamtelur.png',
            'index' => 3,
            'activestatus' => '1',
        ]);
        DB::table('order_shipping_statuses')->insert([
            'name' => 'Pending',
            'image' => Request::url().'/images/ayamtelur.png',
            'index' => 1,
            'activestatus' => '1',
        ]);
        DB::table('order_shipping_statuses')->insert([
            'name' => 'Packing',
            'image' => Request::url().'/images/ayamtelur.png',
            'index' => 2,
            'activestatus' => '1',
        ]);
        DB::table('order_shipping_statuses')->insert([
            'name' => 'Out Of Delivery',
            'image' => Request::url().'/images/ayamtelur.png',
            'index' => 3,
            'activestatus' => '1',
        ]);
        DB::table('order_shipping_statuses')->insert([
            'name' => 'Delivered',
            'image' => Request::url().'/images/ayamtelur.png',
            'index' => 4,
            'activestatus' => '1',
        ]);
        

    }
}
