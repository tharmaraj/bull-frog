<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//API
Route::post('/productsapi', 'ProductStoreController@publicproductview')->name('productsapiweb');
Route::post('/paymentproductcreateapi', 'PaymentProductController@createapi');


//API


Route::get('/', function () {
    // return view('auth/login');
    // return view('welcome');
    return view('welcome_new');
});

Route::get('/privacy-policy', function () {
    return view('privacy_policy');
});

Route::get('/delete-account', function () {
    return view('auth.deleteaccount');
});
Route::post('/deleteaccount', 'ConfigController@deleteaccount')->name('deleteaccount');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/config', 'ConfigController@api');
Route::post('/getTotalCartCount', 'ConfigController@getTotalCartCount');

Route::post('/masterapi', 'MasterApiController@api');
Route::post('/handleLogin', 'MasterApiController@handleLogin')->name('handleLogin');
Route::post('/handleRegister', 'MasterApiController@handleRegister')->name('handleRegister');

Route::get('/memberview', 'MemberController@index')->name('memberview');
Route::get('/memberapprove/{id}', 'MemberController@approve')->name('memberapprove');
Route::get('/memberreject/{id}', 'MemberController@reject')->name('memberreject');
Route::get('/memberlogin/{id}', 'MemberController@login')->name('memberlogin');

Route::get('/profileview', 'ProfileController@index')->name('profileview');
Route::get('/profileedit/{id}', 'ProfileController@edit')->name('profileedit');
Route::post('/profileupdate/{id}', 'ProfileController@update')->name('profileupdate');

Route::get('/merchantview', 'MerchantController@index')->name('merchantview');
Route::get('/merchantcreate', 'MerchantController@create')->name('merchantcreate');
Route::post('/merchantstore', 'MerchantController@store')->name('merchantstore');
Route::get('/merchantedit/{id}', 'MerchantController@edit')->name('merchantedit');
Route::post('/merchantupdate/{id}', 'MerchantController@update')->name('merchantupdate');
Route::get('/merchantapprove/{id}', 'MerchantController@approve')->name('merchantapprove');
Route::get('/merchantreject/{id}', 'MerchantController@reject')->name('merchantreject');
Route::get('/merchantdelete/{id}', 'MerchantController@destroy')->name('merchantdelete');

Route::get('/merchantstoreview', 'MerchantStoreController@index')->name('merchantstoreview');
Route::get('/merchantstorecreate', 'MerchantStoreController@create')->name('merchantstorecreate');
Route::post('/merchantstorestore', 'MerchantStoreController@store')->name('merchantstorestore');
Route::get('/merchantstoreedit/{id}', 'MerchantStoreController@edit')->name('merchantstoreedit');
Route::post('/merchantstoreupdate/{id}', 'MerchantStoreController@update')->name('merchantstoreupdate');
Route::get('/merchantstoreapprove/{id}', 'MerchantStoreController@approve')->name('merchantstoreapprove');
Route::get('/merchantstorereject/{id}', 'MerchantStoreController@reject')->name('merchantstorereject');
Route::get('/merchantstoredelete/{id}', 'MerchantStoreController@destroy')->name('merchantstoredelete');

Route::get('/productstoreview', 'ProductStoreController@index')->name('productstoreview');
Route::get('/productstorecreate', 'ProductStoreController@create')->name('productstorecreate');
Route::get('/morningmarketcreate/{morningmarketind}', 'ProductStoreController@morningmarketcreate')->name('morningmarketcreate');
Route::post('/productstorestore', 'ProductStoreController@store')->name('productstorestore');
Route::get('/productstoreedit/{id}', 'ProductStoreController@edit')->name('productstoreedit');
Route::post('/productstoreupdate/{id}', 'ProductStoreController@update')->name('productstoreupdate');
Route::get('/productstoreapprove/{id}', 'ProductStoreController@approve')->name('productstoreapprove');
Route::get('/productstorereject/{id}', 'ProductStoreController@reject')->name('productstorereject');
Route::get('/productstoredelete/{id}', 'ProductStoreController@destroy')->name('productstoredelete');
Route::get('/viewvariant/{variantid}', 'ProductStoreController@viewvariant')->name('viewvariant');
Route::get('/products', 'ProductStoreController@publicproductview')->name('publicproductview');
Route::get('/morningmarketview', 'ProductStoreController@morningmarketview')->name('morningmarketview');
Route::get('/deactiveMarketProduct', 'ProductStoreController@deactiveMarketProduct')->name('deactiveMarketProduct');

Route::get('/variantproductview', 'VariantProductController@index')->name('variantproductview');
Route::get('/variantproductcreate', 'VariantProductController@create')->name('variantproductcreate');
Route::post('/variantproductstore', 'VariantProductController@store')->name('variantproductstore');
Route::get('/variantproductedit/{id}', 'VariantProductController@edit')->name('variantproductedit');
Route::post('/variantproductupdate/{id}', 'VariantProductController@update')->name('variantproductupdate');
Route::get('/variantproductapprove/{id}', 'VariantProductController@approve')->name('variantproductapprove');
Route::get('/variantproductreject/{id}', 'VariantProductController@reject')->name('variantproductreject');
Route::get('/variantproductdelete/{id}', 'VariantProductController@destroy')->name('variantproductdelete');

Route::get('/cartproductview', 'CartProductMasterController@index')->name('cartproductview');
Route::get('/cartproductcreate', 'CartProductMasterController@create')->name('cartproductcreate');
Route::post('/cartproductstore', 'CartProductMasterController@store')->name('cartproductstore');
Route::get('/cartproductedit/{id}', 'CartProductMasterController@edit')->name('cartproductedit');
Route::post('/cartproductupdate/{id}', 'CartProductMasterController@update')->name('cartproductupdate');
Route::get('/cartproductdelete/{id}/{userid}', 'CartProductMasterController@destroy')->name('cartproductdelete');
Route::get('/cartproductminus/{id}/{userid}', 'CartProductMasterController@minus')->name('cartproductminus');

Route::get('/paymentproductview', 'PaymentProductController@index')->name('paymentproductview');
Route::get('/paymentproductcreate', 'PaymentProductController@create')->name('paymentproductcreate');
Route::post('/paymentproductstore', 'PaymentProductController@store')->name('paymentproductstore');
Route::get('/paymentproductedit/{id}', 'PaymentProductController@edit')->name('paymentproductedit');
Route::post('/paymentproductupdate/{id}', 'PaymentProductController@update')->name('paymentproductupdate');
Route::get('/paymentproductdelete/{id}', 'PaymentProductController@destroy')->name('paymentproductdelete');

Route::get('/shippingmasterview', 'ShippingMasterController@index')->name('shippingmasterview');
Route::get('/shippingmastercreate', 'ShippingMasterController@create')->name('shippingmastercreate');
Route::post('/shippingmasterstore', 'ShippingMasterController@store')->name('shippingmasterstore');
Route::get('/shippingmasteredit/{id}', 'ShippingMasterController@edit')->name('shippingmasteredit');
Route::post('/shippingmasterupdate/{id}', 'ShippingMasterController@update')->name('shippingmasterupdate');
Route::get('/shippingmasterapprove/{id}', 'ShippingMasterController@approve')->name('shippingmasterapprove');
Route::get('/shippingmasterreject/{id}', 'ShippingMasterController@reject')->name('shippingmasterreject');
Route::get('/shippingmasterdelete/{id}', 'ShippingMasterController@destroy')->name('shippingmasterdelete');

Route::post('/paymentsettlestore', 'PaymentSettleController@store')->name('paymentsettlestore');
Route::post('/paymentsettleupdatereceipt', 'PaymentSettleController@updateReceipt')->name('paymentsettleupdatereceipt');

Route::post('/paymentsettleupdate', 'PaymentSettleController@updateOrder');
Route::post('/paymentsettlegetpaymentstatus', 'PaymentSettleController@getpaymentstatus');
Route::get('/paymentgatewayredirect', 'PaymentSettleController@redirect');
Route::post('/paymentgatewaycallback', 'PaymentSettleController@callback');

Route::get('/historymasterview', 'HistoryMasterController@index')->name('historymasterview');
Route::get('/historymerchantmasterview', 'HistoryMasterController@indexmerchant')->name('historymerchantmasterview');
Route::get('/historymerchantmasterviewdelivered', 'HistoryMasterController@indexmerchantdelivered')->name('historymerchantmasterviewdelivered');
Route::get('/historymerchantshippingupdate/{id}/{userid}/{appind}', 'HistoryMasterController@editmerchantshippingstatus')->name('historymerchantshippingupdate');
Route::get('/historymerchantpaymentupdate/{id}/{userid}/{appind}', 'HistoryMasterController@editmerchantpaymentstatus')->name('historymerchantpaymentupdate');


Route::get('/voucherview', 'VoucherController@index')->name('voucherview');
Route::get('/vouchercreate', 'VoucherController@create')->name('vouchercreate');
Route::post('/voucherstore', 'VoucherController@store')->name('voucherstore');
Route::get('/voucheredit/{id}', 'VoucherController@edit')->name('voucheredit');
Route::post('/voucherupdate/{id}', 'VoucherController@update')->name('voucherupdate');
Route::get('/voucherapprove/{id}', 'VoucherController@approve')->name('voucherapprove');
Route::get('/voucherreject/{id}', 'VoucherController@reject')->name('voucherreject');
Route::get('/voucherdelete/{id}', 'VoucherController@destroy')->name('voucherdelete');

Route::get('/categoryview', 'CategoryController@index')->name('categoryview');
Route::get('/categoryedit/{id}', 'CategoryController@edit')->name('categoryedit');
Route::get('/categorycreate', 'CategoryController@create')->name('categorycreate');
Route::post('/categorystore', 'CategoryController@store')->name('categorystore');
Route::post('/categoryupdate/{id}', 'CategoryController@update')->name('categoryupdate');
Route::get('/categorydelete/{id}', 'CategoryController@destroy')->name('categorydelete');

Route::get('/subcategoryview', 'SubCategoryController@index')->name('subcategoryview');
Route::get('/subcategoryedit/{id}', 'SubCategoryController@edit')->name('subcategoryedit');
Route::get('/subcategorycreate', 'SubCategoryController@create')->name('subcategorycreate');
Route::post('/subcategorystore', 'SubCategoryController@store')->name('subcategorystore');
Route::post('/subcategoryupdate/{id}', 'SubCategoryController@update')->name('subcategoryupdate');
Route::get('/subcategorydelete/{id}', 'SubCategoryController@destroy')->name('subcategorydelete');

Route::get('/variantitemlistview', 'VariantItemListController@index')->name('variantitemlistview');
Route::get('/variantitemlistedit/{id}', 'VariantItemListController@edit')->name('variantitemlistedit');
Route::get('/variantitemlistcreate', 'VariantItemListController@create')->name('variantitemlistcreate');
Route::post('/variantitemliststore', 'VariantItemListController@store')->name('variantitemliststore');
Route::post('/variantitemlistupdate/{id}', 'VariantItemListController@update')->name('variantitemlistupdate');
Route::get('/variantitemlistdelete/{id}', 'VariantItemListController@destroy')->name('variantitemlistdelete');

Route::get('/sliderview', 'SliderController@index')->name('sliderview');
Route::get('/slideredit/{id}', 'SliderController@edit')->name('slideredit');
Route::get('/slidercreate', 'SliderController@create')->name('slidercreate');
Route::post('/sliderstore', 'SliderController@store')->name('sliderstore');
Route::post('/sliderupdate/{id}', 'SliderController@update')->name('sliderupdate');
Route::get('/sliderdelete/{id}', 'SliderController@destroy')->name('sliderdelete');
Route::get('/sliderapprove/{id}', 'SliderController@approve')->name('sliderapprove');
Route::get('/sliderreject/{id}', 'SliderController@reject')->name('sliderreject');

Route::get('/reviewproductview', 'ReviewProductController@index')->name('reviewproductview');
Route::get('/reviewproductedit/{id}', 'ReviewProductController@edit')->name('reviewproductedit');
Route::get('/reviewproductcreate', 'ReviewProductController@create')->name('reviewproductcreate');
Route::post('/reviewproductstore', 'ReviewProductController@store')->name('reviewproductstore');
Route::post('/reviewproductupdate/{id}', 'ReviewProductController@update')->name('reviewproductupdate');
Route::get('/reviewproductdelete/{id}', 'ReviewProductController@destroy')->name('reviewproductdelete');
Route::get('/reviewproductapprove/{id}', 'ReviewProductController@approve')->name('reviewproductapprove');
Route::get('/reviewproductreject/{id}', 'ReviewProductController@reject')->name('reviewproductreject');
Route::post('/reviewproductrejectapi', 'ReviewProductController@rejectapi')->name('reviewproductrejectapi');

Route::get('/voucherentryview', 'VoucherEntryController@index')->name('voucherentryview');
Route::post('/voucherentrystore', 'VoucherEntryController@store')->name('voucherentrystore');

Route::get('/mailtest', 'MailController@index')->name('mailtest');
Route::get('/telegramtest', 'MasterApiController@index')->name('telegramtest');

Route::get('/bargainview', 'BargainController@index')->name('bargainview');
Route::get('/bargaincustomerapprove/{id}', 'BargainController@customerapprove')->name('bargaincustomerapprove');
Route::get('/bargaincustomerreject/{id}', 'BargainController@customerreject')->name('bargaincustomerreject');
Route::get('/bargainapprove/{id}', 'BargainController@approve')->name('bargainapprove');
Route::get('/bargainreject/{id}', 'BargainController@reject')->name('bargainreject');
Route::post('/bargainstore', 'BargainController@store')->name('bargainstore');
Route::post('/bargainupdatemerchantmessage/{id}', 'BargainController@updatemerchantmessage')->name('bargainupdatemerchantmessage');

Route::get('/resetpasswordview', 'ResetPasswordManualController@index')->name('resetpasswordview');
Route::post('/resetpasswordmail', 'ResetPasswordManualController@create')->name('resetpasswordmail');
Route::get('/resetpasswordemail', 'ResetPasswordManualController@emailsend');
Route::post('/resetnewpassword', 'ResetPasswordManualController@store')->name('resetnewpassword');

Route::get('/dashboardview/{id}', 'DashboardController@index')->name('dashboardview');

//Esay App
Route::get('/channel', 'EasyAppController@getchannel');
Route::get('/records', 'EasyAppController@getrecords');
Route::get('/getrecordstoday', 'EasyAppController@getrecordstoday');
Route::get('/getrecordsbelumbayar', 'EasyAppController@getrecordsbelumbayar');
Route::post('/recordCreate', 'EasyAppController@recordCreate');
Route::post('/getrecorfilter', 'EasyAppController@getrecorfilter');
Route::get('/recordUpdate/{id}', 'EasyAppController@recordUpdate');
Route::get('/recordDelete/{id}', 'EasyAppController@recordDelete');
Route::post('/storein', 'EasyAppController@storein');
//Esay App